#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "gordian.h"


void init_partitions_list (){

	struct cell_t *cur_cell;

	partitions = (struct partition_t*) malloc (sizeof(struct partition_t));
	NULL_CHECK(partitions);

	partitions->contents = (struct cell_t**) malloc (movablesCtr * sizeof(struct cell_t*));
	NULL_CHECK(partitions->contents);

	//fill the partitions contents with pointers to the cells (only movable cells, not terminals)
	cur_cell = cells_list;
	while(cur_cell!=NULL){
		if(!cur_cell->terminal){
			partitions->contents[cur_cell->index] = cur_cell;
			}
		cur_cell = cur_cell->next;
		}

	partitions->contentsCtr = movablesCtr;
	partitions->centerX = coreAreaWidth / 2.0;
	partitions->centerY = coreAreaHeight / 2.0;
	partitions->minX = 0.0;
	partitions->minY = 0.0;
	partitions->maxX = coreAreaWidth;
	partitions->maxY = coreAreaHeight;
	#ifdef USE_AREA_CONSTRAINT
	partitions->area = partition_area(partitions);
	#endif
	partitions->next = NULL;
	partitionsCtr = 1;
	}


void free_partitions_list (){

	struct partition_t *p=partitions, *pnext;

	free(partitions->contents);

	while(p!=NULL){
		pnext = p->next;
		free(p);
		p = pnext;
		}
	}


/****************************************************************************************************************************************
 * Calculates and returns the total area that all the cells in partition occupy.							*
 ****************************************************************************************************************************************/
#ifdef USE_AREA_CONSTRAINT
double partition_area (struct partition_t *partition){

	double area=0.0;
	int i;

	for (i=0; i<partition->contentsCtr; i++) {
		area += partition->contents[i]->sizeX * partition->contents[i]->sizeY;
		}

	return (area);
	}
#endif


/****************************************************************************************************************************************
 * Splits a partition in two (left and right). The left partition keeps a percentage of cells according to (a), and the right partition	*
 * keeps a percentage of cells according to (1-a). Returns a pointer to the right partition. If the cut_direction is 'Y', then the left	*
 * and right partitions correspond to the lower and upper partition respectively.							*
 ****************************************************************************************************************************************/
struct partition_t* bisect_partition (struct partition_t *partition, double alpha, char cut_direction){

	struct partition_t *new;

	#ifdef USE_AREA_CONSTRAINT
	double total_area = partition->area;
	#endif

	//if contentsCtr is odd, it gives the extra cell to the right partition
	//int partition_contents_ctr = (int)(partition->contentsCtr * alpha);
	//int new_contents_ctr = partition->contentsCtr - partition_contents_ctr;

	//if contentsCtr is odd, it gives the extra cell to the left partition
	//int new_contents_ctr = (int)(partition->contentsCtr * alpha);
	//int partition_contents_ctr = partition->contentsCtr - new_contents_ctr;

/*int i;
double area=0.0;

for (i=0; i<partition->contentsCtr; i++){
	if (area >= partition->area * alpha) break;
	else area += partition->contents[i]->sizeX * partition->contents[i]->sizeY;
	}
if (i==partition->contentsCtr) i = (int)(partition->contentsCtr/2);
new_contents_ctr = i;
partition_contents_ctr = partition->contentsCtr - new_contents_ctr;*/


	//allocate memory for the right partition
	new = (struct partition_t*) malloc (sizeof(struct partition_t));
	NULL_CHECK(new);

	//let the left partition keep a portion of the cells
//	partition->contentsCtr = partition_contents_ctr;

	//give the rest cells to the right partition
//	new->contents = &(partition->contents[partition_contents_ctr]);
//	new->contentsCtr = new_contents_ctr;

	//calculate the new borders and centers of the left and right partition
	if(cut_direction =='X'){
		new->maxX = partition->maxX;
		partition->maxX = (partition->minX + partition->maxX) / 2.0;
		new->minX = partition->maxX;

		partition->centerX = (partition->minX + partition->maxX) / 2.0;
		new->centerX = (new->minX + new->maxX) / 2.0;

		new->minY = partition->minY;
		new->maxY = partition->maxY;
		new->centerY = partition->centerY;
		}
	else if(cut_direction =='Y'){
		new->maxY = partition->maxY;
		partition->maxY = (partition->minY + partition->maxY) / 2.0;
		new->minY = partition->maxY;

		partition->centerY = (partition->minY + partition->maxY) / 2.0;
		new->centerY = (new->minY + new->maxY) / 2.0;

		new->minX = partition->minX;
		new->maxX = partition->maxX;
		new->centerX = partition->centerX;
		}
	else {
		printf("\nERROR: invalid argument in bisect_partition\n");
		return NULL;
	}

	//make the list connections
	new->next = partition->next;
	partition->next = new;

	#ifdef USE_AREA_CONSTRAINT
	//calculate the total area of the cells in the two partitions
	new->area = partition_area(new);
	partition->area = total_area - new->area;
	#endif

	int i; 

	if(cut_direction =='X') {
		for(i=0; i< partition->contentsCtr; i++) {
			if (partition->contents[i]->centerX > partition->maxX)
			{
				new->contents = &partition->contents[i];
				new->contentsCtr = partition->contentsCtr -i;
				partition->contentsCtr = partition->contentsCtr - new->contentsCtr;
				break;
			}
		}
		if(new->contentsCtr==0)
			{
			printf("problhma1\n");
                        }
		if(partition->contentsCtr==0)
			{
			printf("problhma2\n");
                        }
	}
	else if(cut_direction =='Y'){
		for(i=0; i< partition->contentsCtr; i++) {
			if (partition->contents[i]->centerY > partition->maxY)
			{
				new->contents = &partition->contents[i];
				new->contentsCtr = partition->contentsCtr -i;
				partition->contentsCtr = partition->contentsCtr - new->contentsCtr;
				break;
			}
		}
		if(new->contentsCtr==0)
			{
			printf("problhma1\n");
                        }
		if(partition->contentsCtr==0)
			{
			printf("problhma2\n");
                        }
	}
	else{
		printf("\nERROR: Should not get here\n");
		
	}

	partitionsCtr++;

	return new;
	}


/****************************************************************************************************************************************
 * Calculates and returns the value of alpha, that corresponds to the cut-line that crosses the minimum number of nets.			*
 ****************************************************************************************************************************************/
double minimum_alpha (struct partition_t *partition, char cut_direction, double gamma){

	//double min_alpha = 0.5 - gamma;
	//double max_alpha = 0.5 + gamma;
	int i;
	double alpha=0.5;
	int min_net_cuts=netsCtr;

	if (cut_direction == 'X'){
		int mid;

		for (i=0; i<partition->contentsCtr && partition->contents[i]->centerX < partition->centerX; i++);
		mid=i;

		for (i=mid-(int)(((double)(partition->contentsCtr))*gamma/2.0); i<=mid+(int)(((double)(partition->contentsCtr))*gamma/2.0); i++){
			int cur_net_cuts=0;
			int j;

			for (j=0; j<netsCtr; j++){
				int k;
				double minX=coreAreaWidth, maxX=0.0;

				//find the borders of the net
				for(k=0; k<nets[j].connectionsCtr; k++){
					if(nets[j].connections[k]->centerX < minX) minX = nets[j].connections[k]->centerX;
					if(nets[j].connections[k]->centerX > maxX) maxX = nets[j].connections[k]->centerX;
					}
				if (partition->contents[i]->centerX < maxX && partition->contents[i]->centerX > minX) cur_net_cuts++;
				}

			if (cur_net_cuts < min_net_cuts){
				min_net_cuts = cur_net_cuts;
				alpha = ((double)i) / ((double)partition->contentsCtr);
//alpha = (partition->contents[i]->centerX - partition->minX) / (partition->maxX - partition->minX);
				}
			}
		}

	else if (cut_direction == 'Y'){

		int mid;

		for (i=0; i<partition->contentsCtr && partition->contents[i]->centerY < partition->centerY; i++);
		mid=i;

		for (i=mid-(int)(((double)(partition->contentsCtr))*gamma/2.0); i<=mid+(int)(((double)(partition->contentsCtr))*gamma/2.0); i++){
			int cur_net_cuts=0;
			int j;
/*			int found; */

			for (j=0; j<netsCtr; j++){
				int k;
				double minY=coreAreaHeight, maxY=0.0;
				
				//find the borders of the net
				for(k=0; k<nets[j].connectionsCtr; k++){
					if(nets[j].connections[k]->centerY < minY) minY = nets[j].connections[k]->centerY;
					if(nets[j].connections[k]->centerY > maxY) maxY = nets[j].connections[k]->centerY;
					}
				if (partition->contents[i]->centerY < maxY && partition->contents[i]->centerY > minY) cur_net_cuts++;
				}

			if (cur_net_cuts < min_net_cuts){
				min_net_cuts = cur_net_cuts;
				alpha = ((double)i) / ((double)partition->contentsCtr);
				}
			}
		}

//printf("\nstarting: %.0lf\n", partition->minX + (partition->maxX - partition->minX) * min_alpha);
//printf("\nstopping: %.0lf\n", partition->minX + (partition->maxX - partition->minX) * max_alpha);

		/*for (i=0; i<partition->contentsCtr && partition->contents[i]->centerX < partition->minX + (partition->maxX - partition->minX) * min_alpha; i++);

//printf("\nsel: %d from %d\n", i, partition->contentsCtr);

		if (i==partition->contentsCtr) printf ("\n\nAAAAAAAAAAAAAAAAAAAAAAAAAAA\n\n");

		for (; partition->contents[i]->centerX <= partition->minX + (partition->maxX - partition->minX) * max_alpha; i++){
			int cur_net_cuts=0;
			int j;
			int found;


			//if (partition->contents[i]->centerX < partition->minX + (partition->maxX - partition->minX) * min_alpha || partition->contents[i]->centerX > partition->minX + (partition->maxX - partition->minX) * max_alpha) continue;

			for (j=0; j<netsCtr; j++){
				int k;
				double minX=coreAreaWidth, maxX=0.0;

				found=0;

				//find the borders of the net
				for(k=0; k<nets[j].connectionsCtr; k++){
					if(nets[j].connections[k]->centerX < minX) minX = nets[j].connections[k]->centerX;
					if(nets[j].connections[k]->centerX > maxX) maxX = nets[j].connections[k]->centerX;
					}
				if (partition->contents[i]->centerX < maxX && partition->contents[i]->centerX > minX) cur_net_cuts++;
				}

			if (cur_net_cuts < min_net_cuts){
				min_net_cuts = cur_net_cuts;
				alpha = ((double)i) / ((double)partition->contentsCtr);
//alpha = (partition->contents[i]->centerX - partition->minX) / (partition->maxX - partition->minX);
				}
			}
		}*/

		/*for (i=(int)(partition->contentsCtr * min_alpha); i<=(int)(partition->contentsCtr * max_alpha); i++){
			int cur_net_cuts=0;
			int j;
			int found;

			for (j=0; j<netsCtr; j++){
				int k;
				double minX=coreAreaWidth, maxX=0.0;

				found=0;
 
				//first check is this net has any cells inside the partition
				for(k=0; k<nets[j].connectionsCtr; k++){
					if (nets[j].connections[k]->centerX < partition->maxX && nets[j].connections[k]->centerX > partition->minX && nets[j].connections[k]->centerY < partition->maxY && nets[j].connections[k]->centerY > partition->minY){
						found=1;
						break;
						}
					}

				if (found){				
					//find the borders of the net
					for(k=0; k<nets[j].connectionsCtr; k++){
						if(nets[j].connections[k]->centerX < minX) minX = nets[j].connections[k]->centerX;
						if(nets[j].connections[k]->centerX > maxX) maxX = nets[j].connections[k]->centerX;
						}
					if (partition->contents[i]->centerX < maxX && partition->contents[i]->centerX > minX) cur_net_cuts++;
					}
				}

			if (cur_net_cuts < min_net_cuts){
				min_net_cuts = cur_net_cuts;
				alpha = ((double)i) / ((double)partition->contentsCtr);
				}
			}
		}*/

//	else if (cut_direction == 'Y'){ alpha=0.5; }

		/*for (i=0; i<partition->contentsCtr; i++){
			int cur_net_cuts=0;
			int j;
			int found;

			if (partition->contents[i]->centerY < min_alpha || partition->contents[i]->centerY > max_alpha) continue;
			
			for (j=0; j<netsCtr; j++){
				int k;
				
				found=0;
 
				//first check is this net has any cells inside the partition
				for(k=0; k<nets[j].connectionsCtr; k++){
					if (nets[j].connections[k]->centerX < partition->maxX && nets[j].connections[k]->centerX > partition->minX && nets[j].connections[k]->centerY < partition->maxY && nets[j].connections[k]->centerY > partition->minY){
						found=1;
						break;
						}
					}

				if (found){
					double minY=coreAreaHeight, maxY=0.0;
				
					//find the borders of the net
					for(k=0; k<nets[j].connectionsCtr; k++){
						if(nets[j].connections[k]->centerY < minY) minY = nets[j].connections[k]->centerY;
						if(nets[j].connections[k]->centerY > maxY) maxY = nets[j].connections[k]->centerY;
						}
					if (partition->contents[i]->centerY < maxY && partition->contents[i]->centerY > minY) cur_net_cuts++;
					}
				}

			if (cur_net_cuts < min_net_cuts){
				min_net_cuts = cur_net_cuts;
				alpha = ((double)i) / ((double)partition->contentsCtr);
				}
			}
		}*/

		/*for (i=(int)(partition->contentsCtr * min_alpha); i<=(int)(partition->contentsCtr * max_alpha); i++){
			int cur_net_cuts=0;
			int j;
			int found;

			for (j=0; j<netsCtr; j++){
				int k;
				double minY=coreAreaHeight, maxY=0.0;

				found=0;
 
				//first check is this net has any cells inside the partition
				for(k=0; k<nets[j].connectionsCtr; k++){
					if (nets[j].connections[k]->centerX < partition->maxX && nets[j].connections[k]->centerX > partition->minX && nets[j].connections[k]->centerY < partition->maxY && nets[j].connections[k]->centerY > partition->minY){
						found=1;
						break;
						}
					}

				if (found){				
					//find the borders of the net
					for(k=0; k<nets[j].connectionsCtr; k++){
						if(nets[j].connections[k]->centerY < minY) minY = nets[j].connections[k]->centerY;
						if(nets[j].connections[k]->centerY > maxY) maxY = nets[j].connections[k]->centerY;
						}
					if (partition->contents[i]->centerY < maxY && partition->contents[i]->centerY > minY) cur_net_cuts++;
					}
				}
	
			if (cur_net_cuts < min_net_cuts){
				min_net_cuts = cur_net_cuts;
				alpha = ((double)i) / ((double)partition->contentsCtr);
				}
			}
		}*/

//printf ("%.2lf\t", alpha);
	return (alpha);
}



