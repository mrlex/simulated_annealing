#include <stdio.h>
#include <stdlib.h>
#include "gordian.h"


/* Legalizes the cells. First moves the cells so they all fit inside rows. Then moves the cells so they do not overlap. */
void legalize (struct cell_t **cells_array){

    int i, j, k;
    double pos_y, pos_x;        //used in final placement
    int numrows;            //the count of rows in the core area
    int *row_count;            //the count of cells inside every row
    double *row_len;        //the total length of cells inside every row
    int sweeps=0;            //number of sweeps needed in final placement
    int done;            //indicates the completion of the final placement
    int offset;            //distance in rows, where the extra cells are attempted to be moved to
    #if (PRINTING==COMPACT)
    int last_printed_percentage=0;    //used for printing
    #endif

    #if (PRINTING==FULL)
    printf("\tLegalizing..."); fflush(stdout);
    #elif (PRINTING==COMPACT)
    printf("Legalizing\t[ 0%%]"); fflush(stdout);
    #endif

    //sort the cells (ascending y-coordinate order)
    quicksort (cells_array, 0, movablesCtr-1, 'Y');

    numrows = (int)(coreAreaHeight/stdCellHeight);
    pos_y = stdCellHeight / 2;
    pos_x = 0;

    //allocate memory for the row_count and row_len vectors
    row_count = (int*) malloc (numrows * sizeof(int));
    NULL_CHECK(row_count);
    row_len = (double*) malloc (numrows * sizeof(double));
    NULL_CHECK(row_len);

    for (i=0; i<numrows; i++){
        row_count[i] = 0;
        row_len[i] = 0.0;
        }

    i=j=0;

    //calculate the cell count and row length of every row    
    do{    
        if (cells_array[j]->centerY <= pos_y + stdCellHeight/2 && ( cells_array[j]->centerY > pos_y - stdCellHeight/2 || cells_array[j]->centerY == 0)){
            row_count[i]++;
            row_len[i] += cells_array[j]->sizeX;
            j++;
            }
        else {
            pos_y += stdCellHeight;
            i++;
            }
    } while (j<movablesCtr);


    offset=1;

    //move cells so they fit inside the rows
    do {
        sweeps++;
        j=0;
        done=1;

        for (i=0; i<numrows; i++){

            if (row_len[i] > coreAreaWidth){
                int move_cell;
                int move_direction=0;                

                //see if the extra cell can be moved 'offset' rows above or below
                if ((i>=offset && i<numrows-offset && row_len[i-offset] >= row_len[i+offset] && row_len[i] > row_len[i+offset] + cells_array[j+row_count[i]-1]->sizeX) ){ //|| i<offset){
                    move_cell = j+row_count[i]-1;
                    move_direction = 1;
                    }
                else if ((i>=offset && i<numrows-offset && row_len[i-offset] < row_len[i+offset] && row_len[i] > row_len[i-offset] + cells_array[j]->sizeX) ){ //|| i>=numrows-offset){
                    move_cell = j;
                    move_direction = -1;
                    }

                //move the cell
                if (move_direction != 0){
                    int move_to=j;

                    if (offset==1) cells_array[move_cell]->centerY = cells_array[move_cell+move_direction]->centerY;
                    else {
                        if (move_direction > 0) for (k=0; k<offset; k++) move_to += row_count[i+k];
                        else {
                            move_to--;
                            for (k=1; k<offset; k++) move_to -= row_count[i-k];
                            }

                        cells_array[move_cell]->centerY = cells_array[move_to]->centerY;
                        }

                    row_count[i]--;
                    row_count[i+move_direction*offset]++;
                    row_len[i] -= cells_array[move_cell]->sizeX;
                    row_len[i+move_direction*offset] += cells_array[move_cell]->sizeX;
                    done=0;

                    //restore the cells' sorting
                    if (offset>1) {
                        if (move_direction > 0) quicksort (cells_array, move_cell, move_to, 'Y');
                        else quicksort (cells_array, move_to, move_cell, 'Y');
                        }
                    }

                if (move_direction < 0) j++;

                //if (move_direction != 0 && offset > 1) break;
                }
            j += row_count[i];
            }

        if (done) offset++;
        else offset=1;

        } while (!(done && offset==numrows));

    #if (PRINTING==COMPACT)
    printf("\rLegalizing\t[50%%]"); fflush(stdout);
    #endif

    j=0;
    pos_y = stdCellHeight / 2;

    //legalize cells inside every row
    for (i=0; i<numrows; i++){

        /*#if (PRINTING==COMPACT)
        if((int)(j*100/movablesCtr) > last_printed_percentage){
            last_printed_percentage = (int) (j*100/movablesCtr);
            printf("\rLegalizing\t[%2d%%]", last_printed_percentage); fflush(stdout);
            }
        #endif*/

        //sort the cells in the i-th row (ascending x-coordinate order)
        quicksort (cells_array, j, j+row_count[i]-1, 'X');

        pos_x = (coreAreaWidth - row_len[i]) / 2;

        for (k=j; k<j+row_count[i]; k++) {
            cells_array[k]->centerY = pos_y;
            cells_array[k]->centerX = pos_x + cells_array[k]->sizeX / 2;
            pos_x += cells_array[k]->sizeX;
            }
            
        pos_y += stdCellHeight;
        j += row_count[i];
        }

    #if (PRINTING==FULL)
    printf("\b\b\b in %d sweeps\n\n", sweeps);
    #elif (PRINTING==COMPACT)
    printf("\rLegalizing\t[OK] \n"); fflush(stdout);
    #endif
}




////////////////

#include <stdio.h>
#include <stdlib.h>
#include "gordian.h"
#include <stdbool.h>

//////////////////////////
typedef struct sorted
{
    struct cell_t *next_cell;
    struct sorted *next;
}sort;

typedef struct rows
{
    double row_max;
    double filled;
}row;

sort *curr_sort, *pre_sort, *temp_sort,*curr2_sort, *pre2_sort, *root_sort;
//////////////////////

void refresh_coordinates(struct cell_t **cells_array)
{
    int j;

    for( j = 0; j < movablesCtr; j++) {
        cells_array[j]->coordinates_x = cells_array[j]->centerX - (cells_array[j]->sizeX / 2.0);
        cells_array[j]->coordinates_y = cells_array[j]->centerY - (cells_array[j]->sizeY / 2.0);     
    }
}

void refresh_center_coordinates(struct cell_t **cells_array)
{
    int j;

    for( j = 0; j < movablesCtr; j++) {
        cells_array[j]->centerX = cells_array[j]->coordinates_x + (cells_array[j]->sizeX / 2.0);
        cells_array[j]->centerY = cells_array[j]->coordinates_y + (cells_array[j]->sizeY / 2.0);     
    }
}

void legalize_y(double height, struct cell_t **cells_array) 
{
    int j;
    for(j = 0; j<movablesCtr; j++)
    {
        double current_row = stdCellHeight;
        for (current_row = stdCellHeight; current_row < cells_array[j]->coordinates_y; current_row += stdCellHeight);
        if((current_row - (cells_array[j]->coordinates_y)) > (stdCellHeight / 2.0)) {
            cells_array[j]->coordinates_y = current_row - stdCellHeight;
        }
        else {
            cells_array[j]->coordinates_y = current_row;
        }
        if (cells_array[j]->coordinates_y == height) {
            cells_array[j]->coordinates_y = height - stdCellHeight; 
        }
        cells_array[j]->placed = 0; 
    }
}

void calculate_row_utilization(int rows_number, sort **sort_array, row *map_area)
{
    int i;

    for (i=0 ; i < rows_number ; i++) {
        curr_sort=sort_array[i]->next;            
        while(curr_sort !=NULL) {
            map_area[i].filled += curr_sort->next_cell->sizeX;
            curr_sort=curr_sort->next;
        }
    }
}

void create_sort_array(sort **sort_array, int rows) 
{
    int i;
    
    for (i = 0; i < rows; i++)
    {
        sort_array[i] = (sort *)malloc(sizeof(sort));
        sort_array[i]->next=NULL;
        sort_array[i]->next_cell=NULL;
    }
}

bool insert_by_coordinate_x (struct cell_t *curr_cell)
{
    if ((curr_sort->next_cell->coordinates_x) > (curr_cell->coordinates_x)) {
        temp_sort=(sort *)malloc(sizeof(sort));
        temp_sort->next=curr_sort;
        temp_sort->next_cell=curr_cell;
        pre_sort->next=temp_sort;
        pre_sort=temp_sort;
        return true;
    }    
    return false;
}

void insert_cell_in_row_in_sort_array(sort *sort_array_row, struct cell_t *curr_cell)
{
    for (pre_sort = sort_array_row, curr_sort = sort_array_row->next;
         curr_sort != NULL; 
         pre_sort = curr_sort, curr_sort = curr_sort->next) {
        
        if (curr_sort != NULL) {
            if (insert_by_coordinate_x(curr_cell)) {                 
                break;
            }
        }
    }
    if (curr_sort == NULL) {
        curr_sort=(sort *)malloc(sizeof(sort));
        curr_sort->next=NULL;
        pre_sort->next=curr_sort;
        curr_sort->next_cell=curr_cell;    
    }
}

void insert_cells_in_rows(sort **sort_array, int rows, struct cell_t **cells_array)
{
    int i, j;        
    create_sort_array(sort_array, rows);

    for (j = 0;j < movablesCtr; j++)
    {
        for (i=0 ; i < rows ; i++)
        {
            double row_coordinate_y = i * stdCellHeight;
            if(row_coordinate_y == cells_array[j]->coordinates_y || row_coordinate_y - cells_array[j]->coordinates_y > 0.0001)
            {
                insert_cell_in_row_in_sort_array(sort_array[i], cells_array[j]);
                break;    
            }    
        }
    }
}

bool cells_fit_in_row(int row_number, row *map_area)
{
    if (map_area[row_number].filled > map_area[row_number].row_max)
        return false;
    else
        return true;
}

bool row_is_first(int row_number)
{
    if (row_number == 0)
        return true;
    else 
        return false;
}

bool row_is_last(int i, int rows_number)
{
    if (i == (rows_number-1))
        return true;
    else
        return false;
}

void update_map_area(int row_under_examination, int new_row, row *map_area) 
{
    map_area[new_row].filled += curr_sort->next_cell->sizeX;
    map_area[row_under_examination].filled -= curr_sort->next_cell->sizeX;
}

void set_cell_to_next_row()
{
    pre_sort->next = curr_sort->next;                
    curr_sort->next = curr2_sort->next;                
    curr2_sort->next = curr_sort;                
    curr_sort = pre_sort->next;    
}

bool place_cell_to_new_row(int row_under_examination, int new_row, row *map_area)
{
    bool change_done = false;

    if(curr2_sort->next == NULL) {
        update_map_area(row_under_examination, new_row, map_area);
        set_cell_to_next_row();
    }
    else {
        while(curr2_sort->next != NULL) {
            if((curr2_sort->next->next_cell->coordinates_x) > (curr_sort->next_cell->coordinates_x)) {
                update_map_area(row_under_examination, new_row, map_area);
                set_cell_to_next_row();
                return true;
            }
            pre2_sort=pre2_sort->next;
            curr2_sort=curr2_sort->next;
        }
        if(!change_done) { 
            update_map_area(row_under_examination, new_row, map_area);
            set_cell_to_next_row();
            return true;
        }
    }
    return false;
}

void move_cell_to_row_up(int row_under_examination, row *map_area, sort **sort_array)
{
    double length = 0;
    pre_sort = sort_array[row_under_examination];
    curr_sort = sort_array[row_under_examination]->next;            

    while(curr_sort != NULL) {
        bool change_done  = false;
        //place cells and find cells that do not fit in row
        if(map_area[row_under_examination].row_max > (length + curr_sort->next_cell->sizeX)) {
            curr_sort->next_cell->coordinates_x = length;
            length += curr_sort->next_cell->sizeX;
        }
        else {
            //move to next row
            curr_sort->next_cell->coordinates_y += stdCellHeight;    
            pre2_sort=sort_array[row_under_examination + 1];
            curr2_sort=sort_array[row_under_examination + 1];
            
            change_done = place_cell_to_new_row(row_under_examination, row_under_examination + 1, map_area);                        
        }
        if(!change_done) {
            pre_sort=curr_sort;
            curr_sort=curr_sort->next;
        }
    }
}

void move_cell_to_rows_down(int row_under_examination, int rows_number, row *map_area, sort **sort_array)
{
    double length = 0;
    int j;
    pre_sort = sort_array[row_under_examination];
    curr_sort = sort_array[row_under_examination]->next;
            
    while(curr_sort !=NULL) {
        bool change_done  = false;
        //place cells and find cells that do not fit in row
        if(map_area[row_under_examination].row_max > (length + curr_sort->next_cell->sizeX)) {
            curr_sort->next_cell->coordinates_x = length;
            length += curr_sort->next_cell->sizeX;
        }
        else {
            //move to next row
            for(j = rows_number-2; j >=0 ; j = j-1) {
                curr_sort->next_cell->coordinates_y -= stdCellHeight;    
                pre2_sort=sort_array[j];
                curr2_sort=sort_array[j];

                if(map_area[j].row_max > (map_area[j].filled + curr_sort->next_cell->sizeX) ) {
                    change_done = place_cell_to_new_row(row_under_examination, j, map_area);
                    break;
                }
            }
        }
        if(!change_done) {
            pre_sort = pre_sort->next;
            curr_sort = curr_sort->next;
        }
    }
}


bool check_if_fits_down(int row_under_examination, int new_row, row *map_area, sort **sort_array)
{                                    
    pre2_sort = sort_array[row_under_examination-new_row];
    curr2_sort = sort_array[row_under_examination-new_row];

    if (map_area[row_under_examination-new_row].row_max > (map_area[row_under_examination-new_row].filled + curr_sort->next_cell->sizeX) ) {    
        curr_sort->next_cell->coordinates_y= stdCellHeight*(row_under_examination-new_row);    
        if (curr2_sort->next == NULL) {
            update_map_area(row_under_examination, row_under_examination - new_row, map_area);
            set_cell_to_next_row();
        }
        else {
            while(curr2_sort->next!=NULL) {
                if((curr2_sort->next->next_cell->coordinates_x) > (curr_sort->next_cell->coordinates_x)) {
                    update_map_area(row_under_examination, row_under_examination - new_row, map_area);
                    set_cell_to_next_row();
                    return true;
                }
                pre2_sort=pre2_sort->next;
                curr2_sort=curr2_sort->next;
            }
            update_map_area(row_under_examination, row_under_examination - new_row, map_area);
            set_cell_to_next_row();
            return true;
        }
        return true;
    }
    return false;
}                    

bool check_if_fits_up(int row_under_examination, int new_row, row *map_area, sort **sort_array)
{
    pre2_sort = sort_array[row_under_examination+new_row];
    curr2_sort = sort_array[row_under_examination+new_row];

    if (map_area[row_under_examination+new_row].row_max > (map_area[row_under_examination+new_row].filled + curr_sort->next_cell->sizeX)) {
        curr_sort->next_cell->coordinates_y = stdCellHeight*(row_under_examination-new_row);
        if (curr2_sort->next == NULL) {
            update_map_area(row_under_examination, new_row, map_area);
            set_cell_to_next_row();
        }
        else {
            while(curr2_sort->next!=NULL) {
                if((curr2_sort->next->next_cell->coordinates_x) > (curr_sort->next_cell->coordinates_x)) {
                    update_map_area(row_under_examination, row_under_examination + new_row, map_area);
                    set_cell_to_next_row();
                    return true;
                }
                pre2_sort = pre2_sort->next;
                curr2_sort = curr2_sort->next;
            }
            update_map_area(row_under_examination, row_under_examination + new_row, map_area);
            set_cell_to_next_row();
        }
        return true;                    
    }
    return false;
}        
    
void move_cell_where_it_fits(int row_under_examination, int rows_number, row *map_area, sort **sort_array)
{
    int new_row = 1;
    double length = 0;
    pre_sort = sort_array[row_under_examination];
    curr_sort = sort_array[row_under_examination]->next;
            
    while(curr_sort != NULL) {    
        bool replaced = false;
        if (map_area[row_under_examination].row_max > (length + curr_sort->next_cell->sizeX)) {
            curr_sort->next_cell->coordinates_x = length;
            length += curr_sort->next_cell->sizeX;
        }
        else {
            new_row = 1;
            while ((row_under_examination-new_row) >=0 || (row_under_examination+new_row) < rows_number) {
                if(row_under_examination-new_row >= 0) {
                    replaced = check_if_fits_down(row_under_examination, new_row, map_area, sort_array);                            
                }        
                if ((row_under_examination+new_row < rows_number) && !replaced) {
                    replaced = check_if_fits_up(row_under_examination, new_row, map_area, sort_array);                            
                }
                if(replaced) {
                    break;
                }
                new_row++;
            }
        }
        if(!replaced) {
            pre_sort=pre_sort->next;
            curr_sort=curr_sort->next;
        }
    }
}

void move_cells_that_do_not_fit_in_rows(int rows_number, sort **sort_array,row *map_area)
{
    int i;

    for (i=0 ; i < rows_number; i++) {
        
        pre_sort=sort_array[i];
        curr_sort=sort_array[i]->next;
        if (!cells_fit_in_row(i, map_area)) {    
            if (row_is_first(i)) {
                move_cell_to_row_up(i, map_area, sort_array);
            }
            else if(row_is_last(i, rows_number)) {
                move_cell_to_rows_down(i, rows_number, map_area, sort_array);
            }
            else {
                move_cell_where_it_fits(i, rows_number, map_area, sort_array);
            }
        }
    }
}

void legalize_every_row(int rows_number, sort **sort_array, double chip_width)
{
    int i;
    for(i=0; i<rows_number; i++) {
        printf("legalizing row: %d\n", i);
        double over=0;
        int overlap=0;
        curr_sort=sort_array[i]->next;
        do {
            overlap=0;
            if(curr_sort != NULL) {
                while(curr_sort->next != NULL) {
                    if(curr_sort->next_cell->placed == 1) {
                        if(curr_sort->next !=NULL) {
                            if((curr_sort->next->next_cell->placed !=1)&&((curr_sort->next_cell->coordinates_x + curr_sort->next_cell->sizeX)>curr_sort->next->next_cell->coordinates_x)) {
                                overlap=1;
                                if((curr_sort->next_cell->coordinates_x+curr_sort->next_cell->sizeX + 0.00001)>=chip_width) {
                                    curr_sort->next->next_cell->coordinates_x=chip_width- curr_sort->next->next_cell->sizeX;
                                    curr_sort->next->next_cell->placed=0;
                                    curr_sort->next_cell->placed=0;
                                    break;
                                }
                                else {
                                    curr_sort->next->next_cell->coordinates_x=curr_sort->next_cell->coordinates_x + curr_sort->next_cell->sizeX;
                                    curr_sort->next->next_cell->placed=1;
                                }                    
                            }
                            else if(((curr_sort->next->next_cell->placed ==1)&&((curr_sort->next_cell->coordinates_x + curr_sort->next_cell->sizeX)>curr_sort->next->next_cell->coordinates_x+1))) {
                                overlap=1;
                                curr_sort->next->next_cell->placed =0;
                            }
                        }
                    }
                    else if((curr_sort->next_cell->coordinates_x + curr_sort->next_cell->sizeX)>curr_sort->next->next_cell->coordinates_x + 0.00001) {    
                        over=fabs(((curr_sort->next_cell->coordinates_x + curr_sort->next_cell->sizeX)- curr_sort->next->next_cell->coordinates_x)/2);
                        if((curr_sort->next_cell->coordinates_x - over )<0) {
                            curr_sort->next_cell->coordinates_x=0;
                            curr_sort->next->next_cell->coordinates_x=curr_sort->next_cell->sizeX;
                            curr_sort->next_cell->placed=1;
                            curr_sort->next->next_cell->placed=1;
                        }
                        else if((curr_sort->next->next_cell->coordinates_x + curr_sort->next->next_cell->sizeX+ over )>chip_width) {
                            curr_sort->next->next_cell->coordinates_x=chip_width - curr_sort->next->next_cell->sizeX;
                            curr_sort->next_cell->coordinates_x=curr_sort->next->next_cell->coordinates_x - curr_sort->next_cell->sizeX;
                            curr_sort->next_cell->placed=0;
                            curr_sort->next->next_cell->placed=1;
                        }
                        else {
                            if(curr_sort->next->next_cell->placed==1) {
                                  curr_sort->next_cell->coordinates_x= curr_sort->next->next_cell->coordinates_x- curr_sort->next_cell->sizeX;
                                curr_sort->next_cell->placed=1;
                            }    
                            else {
                                curr_sort->next_cell->coordinates_x-=over;
                                curr_sort->next->next_cell->coordinates_x+=over;
                            }
                        }
                        overlap=1;
                    }
                    curr_sort=curr_sort->next;
                }
            }
            curr_sort=sort_array[i]->next;
        } while(overlap == 1);
    }
}

void init_map_area(int rows_number, row *map_area, double chip_width)
{
    int i;

    for(i = 0; i<rows_number; i++) {
        map_area[i].filled = 0;
        map_area[i].row_max = chip_width;
    }
}

void legalizer_abacus_alike(double chip_height, double chip_width, struct cell_t **cells_array)
{
    refresh_coordinates(cells_array);

    legalize_y(chip_height, cells_array);

    int rows_number =(int) round(chip_height / stdCellHeight);
    sort *sort_array[rows_number];    

    insert_cells_in_rows(sort_array, rows_number, cells_array);

    row map_area[rows_number];
    init_map_area(rows_number, map_area, chip_width);

    calculate_row_utilization(rows_number, sort_array, map_area);

    move_cells_that_do_not_fit_in_rows(rows_number, sort_array, map_area);

    int i;

    for (i=0 ; i < rows_number ; i++) {
        curr_sort=sort_array[i]->next;
        map_area[i].filled = 0;            
        while(curr_sort !=NULL) {
            map_area[i].filled += curr_sort->next_cell->sizeX;
            curr_sort=curr_sort->next;
        }
//        printf("row : %d --> filled: %lf max_length %lf\n", i, map_area[i].filled, map_area[i].row_max);
    }


    legalize_every_row(rows_number, sort_array, chip_width);
/*
    printf("\n\n\n");
    for(i=0; i< rows_number; i++) {
        printf ("row %d\n", i);
        curr_sort = sort_array[i]->next;
        while(curr_sort) {
            printf(" name: %s %lf %lf", curr_sort->next_cell->name, curr_sort->next_cell->coordinates_x, curr_sort->next_cell->coordinates_x+curr_sort->next_cell->sizeX);
            curr_sort = curr_sort->next;
        }
        printf("\n"); 
    }
    printf("\n\n\n");
*/
    refresh_center_coordinates(cells_array);
}

void sort_all_cells_by_coordinate_x(struct cell_t **cells_array)
{
    int i = 0;
    root_sort = NULL;

    for(i = 0; i < movablesCtr; i++) {
        if (root_sort == NULL) {
            temp_sort = (sort *)malloc(sizeof(sort));
            temp_sort->next_cell = cells_array[i];
            temp_sort->next = NULL;
            root_sort = temp_sort;        
        }
        else {
            curr_sort = root_sort;
            pre_sort = NULL;
            for (; curr_sort; pre_sort = curr_sort,curr_sort = curr_sort->next) {
                if (cells_array[i]->coordinates_x < curr_sort->next_cell->coordinates_x) {
                    temp_sort = (sort *)malloc(sizeof(sort));
                    temp_sort->next_cell = cells_array[i];
                    if (pre_sort == NULL) {
                        temp_sort->next = curr_sort;
                        root_sort = temp_sort;                    
                    }
                    else {
                        pre_sort->next = temp_sort;
                        temp_sort->next = curr_sort;                    
                    }
                    break;            
                }        
            }
            if (curr_sort == NULL){
                temp_sort = (sort *)malloc(sizeof(sort));
                temp_sort->next_cell = cells_array[i];
                pre_sort->next = temp_sort;
                temp_sort->next = curr_sort;
            }             
        }
    }    
}

double calculate_cell_movement_cost(double row_x, double row_y, sort *curr_sort) 
{
    return fabs(curr_sort->next_cell->coordinates_x - row_x) + fabs(curr_sort->next_cell->coordinates_y - row_y);
}

void tetris_assign_cells_to_rows(int rows_number, row *map_area)
{
    int i = 0;
    double new_cost, best_cost = INFINITY;
    int best_row = 0;

    for(curr_sort = root_sort; curr_sort; curr_sort = curr_sort->next) {
        best_cost = INFINITY;
        for (i = 0; i < rows_number; i++) {
            if ((curr_sort->next_cell->sizeX + map_area[i].filled) > map_area[i].row_max) {
                new_cost = INFINITY;
            } 
            else {
                new_cost = calculate_cell_movement_cost (map_area[i].filled, i*stdCellHeight, curr_sort);
            }
            if (best_cost > new_cost) {
                best_cost = new_cost;
                best_row = i;
            }  
        }
        curr_sort->next_cell->coordinates_x = map_area[best_row].filled;
        curr_sort->next_cell->coordinates_y = best_row * stdCellHeight;
        map_area[best_row].filled += curr_sort->next_cell->sizeX;
    }
}

void tetris(double chip_height, double chip_width, struct cell_t **cells_array)
{
    printf("starting sorting\n");
    sort_all_cells_by_coordinate_x(cells_array);
    printf("finished sorting\n");
    //print_all_sorted_cells_list();
    int rows_number =(int) round(chip_height / stdCellHeight);                
    row map_area[rows_number];

    init_map_area(rows_number, map_area, chip_width);
    tetris_assign_cells_to_rows(rows_number, map_area);
    refresh_center_coordinates(cells_array);
}

void insert_cell_to_new_sorted_position()
{
    pre_sort->next = curr_sort->next;
    curr_sort->next = curr2_sort;
    pre2_sort->next = curr_sort;
}

bool cell_has_larger_coordinate_x()
{
    if ((curr2_sort->next_cell->coordinates_x) > (curr_sort->next_cell->coordinates_x)) {
        insert_cell_to_new_sorted_position();        
        return true;     
    }
    return false;
}

bool cell_is_larger()
{
    if ((curr2_sort->next_cell->sizeX) > (curr_sort->next_cell->sizeX)) {
        insert_cell_to_new_sorted_position();        
        return true;     
    }
    return false;
}

bool cell_is_smaller()
{
    if ((curr2_sort->next_cell->sizeX) < (curr_sort->next_cell->sizeX)) {
        insert_cell_to_new_sorted_position();        
        return true;     
    }
    return false;
}


void original_row_is_first(sort *sort_array_row)
{
    /* move cell to next line, sort the cell there and 
    ** let the algorithm take over on next repeat
    */
    curr_sort->next_cell->coordinates_y += stdCellHeight;

    for (pre2_sort = sort_array_row, curr2_sort = sort_array_row->next;
         curr2_sort != NULL;
         pre2_sort = curr2_sort, curr2_sort = curr2_sort->next) {    

        if(curr2_sort != NULL) {
            if(cell_has_larger_coordinate_x()) {
                //curr_cell has smaller coordinate_x    
                break; 
            }
        }
    }
                        
    if (curr2_sort == NULL) {
        insert_cell_to_new_sorted_position();
    }
    curr_sort=pre_sort;
}

double calculate_new_width_of_row (sort *sort_array_row)
{
    if (sort_array_row->next == NULL) {
        pre2_sort = sort_array_row;    
        return curr_sort->next_cell->sizeX;    
    }
    else {
        for (pre2_sort = sort_array_row, curr2_sort = sort_array_row->next;
              curr2_sort != NULL;
              pre2_sort = curr2_sort, curr2_sort = curr2_sort->next);        
        return pre2_sort->next_cell->coordinates_x + pre2_sort->next_cell->sizeX + curr_sort->next_cell->sizeX;
    }
}

bool cell_fits_in_rows_below(sort **sort_array, int i, double chip_width)
{
    int j = 0;
    
    for(j=i-1; j>=0; j=j-1) {
        double new_width = calculate_new_width_of_row(sort_array[j]);
        
        if (new_width <= chip_width) {    
            insert_cell_to_new_sorted_position();
            if(pre2_sort->next_cell == NULL) { 
                //first element of the array
                curr_sort->next_cell->coordinates_x= 0;
            }
            else {
                curr_sort->next_cell->coordinates_x= (pre2_sort->next_cell->coordinates_x) + (pre2_sort->next_cell->sizeX);
            }
            curr_sort->next_cell->coordinates_y = j * stdCellHeight;
            curr_sort = pre_sort;
            return true;
        }
    }
    return false;
}

void original_row_is_not_first(sort **sort_array, int i, double chip_width, int rows) 
{
    if (cell_fits_in_rows_below(sort_array, i, chip_width) == false) {
        if( i < rows) {                
            original_row_is_first(sort_array[i]);
        }
        else {
            printf("Something wrong happened!\n");
        }
    }    
}

void cell_does_not_fit_in_row(sort **sort_array, int i, double chip_width, int rows) 
{    
    if (i == 0) {        
        original_row_is_first(sort_array[i + 1]);
    }
    else {
        original_row_is_not_first(sort_array, i, chip_width, rows);
    }
}

void legalize_x(double chip_width, double chip_height, struct cell_t **cells_array)
{
    int rows =(int) round(chip_height / stdCellHeight);
    sort *sort_array[rows];

    insert_cells_in_rows(sort_array, rows, cells_array);

    int i = 0;
    for (i=0 ; i < rows ; i++) {
        double  total_width = 0;
        for (pre_sort = sort_array[i], curr_sort = sort_array[i]->next;
             curr_sort != NULL; 
             pre_sort = curr_sort, curr_sort = curr_sort->next) {

            double new_total_width = total_width + curr_sort->next_cell->sizeX;
            if (new_total_width > chip_width) {    
                cell_does_not_fit_in_row(sort_array, i, chip_width, rows); 
            }
            else {
                curr_sort->next_cell->coordinates_x = total_width;
                total_width = new_total_width;
            }        
        }
    }
}

