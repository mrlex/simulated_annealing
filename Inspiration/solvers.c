#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "gordian.h"

#define OMEGA_TOL	(10e-16)
#define RHO_TOL		(10e-16)
#define ITER_LIMIT	(1000)

#define CS_NORM(VECTOR, N) cs_norm_infty_vector(VECTOR, N)
//#define CS_NORM(VECTOR, N) cs_norm_sec_vector(VECTOR, N)


/* returns the second norm of a vector of size n */
double cs_norm_sec_vector (double *vector, int n){
	int i;
	double sum=0.0;
	for(i=0; i<n; i++) sum += pow(vector[i], 2.0);
	return sqrt(sum);
	}


/* returns the infinite norm of a vector of size n */
double cs_norm_infty_vector (double *vector, int n){
	int i;
	double max=0.0;
	for(i=0; i<n; i++) if(fabs(vector[i]) > max) max = fabs(vector[i]);
	return (max);
	}


/* x=A\b where A is symmetric positive definite; solution is returned in x */
int cs_cgsol (cs *A, double *x, double *b, int A_n, double tol){

	double *r, *z, *p, *q, *res;
	int i, j, iter;
	double rho=0.0, rho1=0.0, alpha, beta, norm_b, sum; 

	double *preconditioner;
	preconditioner = (double*) malloc (A_n * sizeof(double));
	if(!preconditioner){
		perror(NULL);
		exit(1);
		}	

	//allocate workspace
	r = (double*) malloc (A_n * sizeof(double));
	z = (double*) malloc (A_n * sizeof(double));
	p = (double*) malloc (A_n * sizeof(double));
	q = (double*) malloc (A_n * sizeof(double));
	res = (double*) malloc (A_n * sizeof(double));
	if(!r || !z || !p || !q || !res){
		perror(NULL);
		exit(1);
		}

	//initialize workspace
	for(i=0; i<A_n; i++) r[i] = z[i] = p[i] = q[i] = res[i] = 0.0;

	//calculate preconditioner
	for(i=0; i<A_n; i++){
		int p, found;
		found = -1;
		for(p=A->p[i]; p<A->p[i+1]; p++) if(A->i[p] == i) found = p;
		if(found>=0) preconditioner[i] = 1.0 / A->x[found];
		else preconditioner[i] = 1.0;
		}

	//res=A*x
	for(i=0; i<A_n; i++)
		for(j=A->p[i]; j<A->p[i+1]; j++)
			res[A->i[j]] += A->x[j] * x[i];

	//r=b-A*x
	for(i=0; i<A_n; i++) r[i] = b[i]-res[i];

	iter = 0;
	norm_b = CS_NORM (b, A_n);
	if(norm_b == 0.0) norm_b = 1.0;

	while((CS_NORM (r, A_n) / norm_b > tol) && iter < ITER_LIMIT){

		iter++;

		//preconditioner solving
		for(i=0; i<A_n; i++) z[i] = preconditioner[i] * r[i];

		sum = 0.0;
		for(i=0; i<A_n; i++) sum += r[i] * z[i];
		rho = sum;

		if(iter == 1){
			for(i=0; i<A_n; i++) p[i] = z[i];
			}
		else{
			beta = rho / rho1;
			for(i=0; i<A_n; i++) p[i] = z[i] + beta * p[i];
			}

		rho1 = rho;

		//q=A*p
		for(i=0; i<A_n; i++) q[i] = 0.0;
		for(i=0; i<A_n; i++)
			for(j=A->p[i]; j<A->p[i+1]; j++)
				q[A->i[j]] += A->x[j] * p[i];

		sum = 0.0;
		for(i=0; i<A_n; i++) sum += p[i] * q[i];

		alpha = rho / sum;

		for(i=0; i<A_n; i++){
			x[i] = x[i] + alpha * p[i];
			r[i] = r[i] - alpha * q[i];
			}
      		}

	free(r);
	free(z);
	free(p);
	free(q);
	free(res);
	
	free(preconditioner);

	return iter;
	}


/* x=A\b where A is unsymmetric; solution is returned in x */
/*int cs_bicgsol (cs *A, double *x, double *b, int A_n, double tol){

	double *r, *r_tilde, *z, *z_tilde, *p, *p_tilde, *q, *q_tilde, *res, *preconditioner;
	int i, j, iter;
	double rho=0.0, rho1=0.0, alpha, beta, omega, norm_b, sum; 

	//allocate workspace
	r = (double*) malloc (A_n * sizeof(double));
	z = (double*) malloc (A_n * sizeof(double));
	p = (double*) malloc (A_n * sizeof(double));
	q = (double*) malloc (A_n * sizeof(double));
	r_tilde = (double*) malloc (A_n * sizeof(double));
	z_tilde = (double*) malloc (A_n * sizeof(double));
	p_tilde = (double*) malloc (A_n * sizeof(double));
	q_tilde = (double*) malloc (A_n * sizeof(double));
	res = (double*) malloc (A_n * sizeof(double));
	preconditioner = (double*) malloc (A_n * sizeof(double));
	if(!r || !z || !p || !q || !r_tilde || !z_tilde || !p_tilde || !q_tilde || !res || !preconditioner){
		perror(NULL);
		exit(1);
		}

	//initialize workspace
	for(i=0; i<A_n; i++) r[i] = z[i] = p[i] = q[i] = res[i] = 0.0;

	//calculate preconditioner
	for(i=0; i<A_n; i++){
		int p, found;
		found = -1;
		for(p=A->p[i]; p<A->p[i+1]; p++) if(A->i[p] == i) found = p;
		if(found>=0) preconditioner[i] = 1.0 / A->x[found];
		else preconditioner[i] = 1.0;
		}

	//res=A*x
	for(i=0; i<A_n; i++)
		for(j=A->p[i]; j<A->p[i+1]; j++)
			res[A->i[j]] += A->x[j] * x[i];

	//r=r_tilde=b-A*x
	for(i=0; i<A_n; i++) r[i] = r_tilde[i] = b[i]-res[i];

	iter = 0;
	norm_b = CS_NORM (b, A_n);
	if(norm_b == 0.0) norm_b = 1.0;

	while((CS_NORM (r, A_n) / norm_b > tol) && iter < ITER_LIMIT){

		iter++;

		//preconditioner solving
		for(i=0; i<A_n; i++){
			z[i] = preconditioner[i] * r[i];
			z_tilde[i] = preconditioner[i] * r_tilde[i];
			}

		sum = 0.0;
		for(i=0; i<A_n; i++) sum += r_tilde[i] * z[i];
		rho = sum;

		if(fabs(rho) < RHO_TOL){
			printf("cs_bicgsol: failed due to zero rho\n");
			exit(1);
			}

		if(iter == 1){
			for(i=0; i<A_n; i++){
				p[i] = z[i];
				p_tilde[i] = z_tilde[i];
				}
			}
		else{
			beta = rho / rho1;
			for(i=0; i<A_n; i++){
				p[i] = z[i] + beta * p[i];
				p_tilde[i] = z_tilde[i] + beta * p_tilde[i];
				}
			}

		rho1 = rho;

		//q=A*p
		for(i=0; i<A_n; i++) q[i] = q_tilde[i] = 0.0;
		for(i=0; i<A_n; i++)
			for(j=A->p[i]; j<A->p[i+1]; j++){
				q[A->i[j]] += A->x[j] * p[i];
				q_tilde[i] += A->x[j] * p_tilde[A->i[j]];
				}

		sum = 0.0;
		for(i=0; i<A_n; i++) sum += p_tilde[i] * q[i];
		omega = sum;

		if(fabs(omega) < OMEGA_TOL){
			printf("cs_bicgsol: failed due to zero omega\n");
			exit(1);
			}

		alpha = rho / omega;

		for(i=0; i<A_n; i++){
			x[i] = x[i] + alpha * p[i];
			r[i] = r[i] - alpha * q[i];
			r_tilde[i] = r_tilde[i] - alpha * q_tilde[i];
			}
      		}


	free(r);
	free(z);
	free(p);
	free(q);
	free(r_tilde);
	free(z_tilde);
	free(p_tilde);
	free(q_tilde);
	free(res);
	free(preconditioner);

	return iter;
	}*/


/* x=A\b where A is symmetric; solution is returned in x */
int cs_bicgsymsol (cs *A, double *x, double *b, int A_n, double tol){

	double *r, *z, *p, *q, *res, *preconditioner;
	int i, j, iter;
	double rho=0.0, rho1=0.0, alpha, beta, omega, norm_b, sum; 

	
	//allocate workspace
	r = (double*) malloc (A_n * sizeof(double));
	z = (double*) malloc (A_n * sizeof(double));
	p = (double*) malloc (A_n * sizeof(double));
	q = (double*) malloc (A_n * sizeof(double));
	res = (double*) malloc (A_n * sizeof(double));
	preconditioner = (double*) malloc (A_n * sizeof(double));
	if(!r || !z || !p || !q || !res || !preconditioner){
		perror(NULL);
		exit(1);
		}

	//initialize workspace
	for(i=0; i<A_n; i++) r[i] = z[i] = p[i] = q[i] = res[i] = 0.0;

	//calculate preconditioner
	for(i=0; i<A_n; i++){
		int p, found;
		found = -1;
		for(p=A->p[i]; p<A->p[i+1]; p++) if(A->i[p] == i) found = p;
		//if(found>=0) preconditioner[i] = 1.0 / A->x[found];
		if(found>=0) preconditioner[i] = 0.1 / A->x[found];	//just works better!!!
		else preconditioner[i] = 1.0;
		}

	//res=A*x
	for(i=0; i<A_n; i++)
		for(j=A->p[i]; j<A->p[i+1]; j++)
			res[A->i[j]] += A->x[j] * x[i];

	//r=b-A*x
	for(i=0; i<A_n; i++) r[i] = b[i]-res[i];

	iter = 0;
	norm_b = CS_NORM (b, A_n);
	if(norm_b == 0.0) norm_b = 1.0;

	while((CS_NORM (r, A_n) / norm_b > tol) && iter < ITER_LIMIT){

		iter++;

		//preconditioner solving
		sum = 0.0;
		for(i=0; i<A_n; i++){
			z[i] = preconditioner[i] * r[i];
			sum += r[i] * z[i];
			}
		rho = sum;	

		if(fabs(rho) < RHO_TOL){
			printf("cs_bicsymgsol: failed due to zero rho\n");
			exit(1);
			}

		if(iter == 1){
			for(i=0; i<A_n; i++) p[i] = z[i];
			}
		else{
			beta = rho / rho1;
			for(i=0; i<A_n; i++) p[i] = z[i] + beta * p[i];
			}

		rho1 = rho;

		//q=A*p (old code, not optimal)
		/*for(i=0; i<A_n; i++) q[i] = 0.0;
		for(i=0; i<A_n; i++)
			for(j=A->p[i]; j<A->p[i+1]; j++)
				q[A->i[j]] += A->x[j] * p[i];*/

		//#pragma omp parallel for private(j) schedule(dynamic) num_threads(6) //rejected due to parallelization overhead
		for(i=0; i<A_n; i++){
			q[i] = 0.0;
			for(j=A->p[i]; j<A->p[i+1]; j++){
				q[i] += A->x[j] * p[A->i[j]];
				}
			}

		sum = 0.0;
		for(i=0; i<A_n; i++){
			sum += p[i] * q[i];
			}		
		omega = sum;

		if(fabs(omega) < OMEGA_TOL){
			printf("cs_bicgsymsol: failed due to zero omega\n");
			exit(1);
			}

		alpha = rho / omega;

		for(i=0; i<A_n; i++){
			x[i] = x[i] + alpha * p[i];
			r[i] = r[i] - alpha * q[i];
			}
      		}

	free(r);

	free(z);
	free(p);
	free(q);
	free(res);
	free(preconditioner);

	return iter;
	}

