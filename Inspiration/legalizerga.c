#include <stdio.h>
#include <stdlib.h>
#include "gordian.h"
#include <stdbool.h>

//////////////////////////
typedef struct sorted
{
	struct cell_t *next_cell;
	struct sorted *next;
}sort;

typedef struct rows
{
	double row_max;
	double filled;
}row;

sort *curr_sort, *pre_sort, *temp_sort,*curr2_sort, *pre2_sort, *root_sort;
//////////////////////

void refresh_coordinates(struct cell_t **cells_array)
{
	int j;

	for( j = 0; j < movablesCtr; j++) {
		cells_array[j]->coordinates_x = cells_array[j]->centerX + (cells_array[j]->sizeX / 2.0);
		cells_array[j]->coordinates_y = cells_array[j]->centerY + (cells_array[j]->sizeY / 2.0); 	
	}
}

void legalize_y(double height, struct cell_t **cells_array) 
{
	int j;
	for(j = 0; j<movablesCtr; j++)
	{
		double current_row = stdCellHeight;
		for (current_row = stdCellHeight; current_row < cells_array[j]->coordinates_y; current_row += stdCellHeight);
		if((current_row - (cells_array[j]->coordinates_y)) > (stdCellHeight / 2.0)) {
			cells_array[j]->coordinates_y = current_row - stdCellHeight;
		}
		else {
			cells_array[j]->coordinates_y = current_row;
		}
		if (cells_array[j]->coordinates_y == height) {
			cells_array[j]->coordinates_y = height - stdCellHeight; 
		}
	}
}

void calculate_row_utilization(int rows_number, sort **sort_array, row *map_area)
{
	int i;

	for (i=0 ; i < rows_number ; i++) {
		curr_sort=sort_array[i]->next;			
		while(curr_sort !=NULL) {
			map_area[i].filled += curr_sort->next_cell->sizeX;
			curr_sort=curr_sort->next;
		}
	}
}

void create_sort_array(sort **sort_array, int rows) 
{
	int i;
	
	for (i = 0; i < rows; i++)
	{
		sort_array[i] = (sort *)malloc(sizeof(sort));
		sort_array[i]->next=NULL;
		sort_array[i]->next_cell=NULL;
	}
}

bool insert_by_coordinate_x (struct cell_t *curr_cell)
{
	if ((curr_sort->next_cell->coordinates_x) > (curr_cell->coordinates_x)) {
		temp_sort=(sort *)malloc(sizeof(sort));
		temp_sort->next=curr_sort;
		temp_sort->next_cell=curr_cell;
		pre_sort->next=temp_sort;
		pre_sort=temp_sort;
		return true;
	}	
	return false;
}

void insert_cell_in_row_in_sort_array(sort *sort_array_row, struct cell_t *curr_cell)
{
	for (pre_sort = sort_array_row, curr_sort = sort_array_row->next;
		 curr_sort != NULL; 
		 pre_sort = curr_sort, curr_sort = curr_sort->next) {
		
		if (curr_sort != NULL) {
			if (insert_by_coordinate_x(curr_cell)) { 				
				break;
			}
		}
	}
	if (curr_sort == NULL) {
		curr_sort=(sort *)malloc(sizeof(sort));
		curr_sort->next=NULL;
		pre_sort->next=curr_sort;
		curr_sort->next_cell=curr_cell;	
	}
}

void insert_cells_in_rows(sort **sort_array, int rows, struct cell_t **cells_array)
{
	int i, j;		
	create_sort_array(sort_array, rows);

	for (j = 0;j < movablesCtr; j++)
	{
		for (i=0 ; i < rows ; i++)
		{
			double row_coordinate_y = i * stdCellHeight;
			if(row_coordinate_y == cells_array[j]->coordinates_y || row_coordinate_y - cells_array[j]->coordinates_y > 0.0001)
			{
				insert_cell_in_row_in_sort_array(sort_array[i], cells_array[j]);
				break;	
			}	
		}
	}
}

bool cells_fit_in_row(int row_number, row *map_area)
{
	if (map_area[row_number].filled > map_area[row_number].row_max)
		return false;
	else
		return true;
}

bool row_is_first(int row_number)
{
	if (row_number == 0)
		return true;
	else 
		return false;
}

bool row_is_last(int i, int rows_number)
{
	if (i == (rows_number-1))
		return true;
	else
		return false;
}

void update_map_area(int row_under_examination, int new_row, row *map_area) 
{
	map_area[new_row].filled += curr_sort->next_cell->sizeX;
	map_area[row_under_examination].filled -= curr_sort->next_cell->sizeX;
}

void set_cell_to_next_row()
{
	pre_sort->next = curr_sort->next;				
	curr_sort->next = curr2_sort->next;				
	curr2_sort->next = curr_sort;				
	curr_sort = pre_sort->next;	
}

bool place_cell_to_new_row(int row_under_examination, int new_row, row *map_area)
{
	bool change_done = false;

	if(curr2_sort->next == NULL) {
		update_map_area(row_under_examination, new_row, map_area);
		set_cell_to_next_row();
	}
	else {
		while(curr2_sort->next != NULL) {
			if((curr2_sort->next->next_cell->coordinates_x) > (curr_sort->next_cell->coordinates_x)) {
				update_map_area(row_under_examination, new_row, map_area);
				set_cell_to_next_row();
				return true;
			}
			pre2_sort=pre2_sort->next;
			curr2_sort=curr2_sort->next;
		}
		if(!change_done) { 
			update_map_area(row_under_examination, new_row, map_area);
			set_cell_to_next_row();
			return true;
		}
	}
	return false;
}

void move_cell_to_row_up(int row_under_examination, row *map_area, sort **sort_array)
{
	double length = 0;
	pre_sort = sort_array[row_under_examination];
	curr_sort = sort_array[row_under_examination]->next;			

	while(curr_sort != NULL) {
		bool change_done  = false;
		//place cells and find cells that do not fit in row
		if(map_area[row_under_examination].row_max > (length + curr_sort->next_cell->sizeX)) {
			curr_sort->next_cell->coordinates_x = length;
			length += curr_sort->next_cell->sizeX;
		}
		else {
			//move to next row
			curr_sort->next_cell->coordinates_y += stdCellHeight;	
			pre2_sort=sort_array[row_under_examination + 1];
			curr2_sort=sort_array[row_under_examination + 1];
			
			change_done = place_cell_to_new_row(row_under_examination, row_under_examination + 1, map_area);						
		}
		if(!change_done) {
			pre_sort=curr_sort;
			curr_sort=curr_sort->next;
		}
	}
}

void move_cell_to_rows_down(int row_under_examination, int rows_number, row *map_area, sort **sort_array)
{
	double length = 0;
	int j;
	pre_sort = sort_array[row_under_examination];
	curr_sort = sort_array[row_under_examination]->next;
			
	while(curr_sort !=NULL) {
		bool change_done  = false;
		//place cells and find cells that do not fit in row
		if(map_area[row_under_examination].row_max > (length + curr_sort->next_cell->sizeX)) {
			curr_sort->next_cell->coordinates_x = length;
			length += curr_sort->next_cell->sizeX;
		}
		else {
			//move to next row
			for(j = rows_number-2; j >=0 ; j = j-1) {
				curr_sort->next_cell->coordinates_y -= stdCellHeight;	
				pre2_sort=sort_array[j];
				curr2_sort=sort_array[j];

				if(map_area[j].row_max > (map_area[j].filled + curr_sort->next_cell->sizeX) ) {
					change_done = place_cell_to_new_row(row_under_examination, j, map_area);
					break;
				}
			}
		}
		if(!change_done) {
			pre_sort = pre_sort->next;
			curr_sort = curr_sort->next;
		}
	}
}


bool check_if_fits_down(int row_under_examination, int new_row, row *map_area, sort **sort_array)
{									
	pre2_sort = sort_array[row_under_examination-new_row];
	curr2_sort = sort_array[row_under_examination-new_row];

    if (map_area[row_under_examination-new_row].row_max > (map_area[row_under_examination-new_row].filled + curr_sort->next_cell->sizeX) ) {	
		curr_sort->next_cell->coordinates_y= stdCellHeight*(row_under_examination-new_row);	
		if (curr2_sort->next == NULL) {
			update_map_area(row_under_examination, new_row, map_area);
			set_cell_to_next_row();
		}
		else {
			while(curr2_sort->next!=NULL) {
				if((curr2_sort->next->next_cell->coordinates_x) > (curr_sort->next_cell->coordinates_x)) {
					update_map_area(row_under_examination, new_row, map_area);
					set_cell_to_next_row();
					return true;
				}
				pre2_sort=pre2_sort->next;
				curr2_sort=curr2_sort->next;
			}
			update_map_area(row_under_examination, new_row, map_area);
			set_cell_to_next_row();
			return true;
		}
		return true;
	}
	return false;
}					

bool check_if_fits_up(int row_under_examination, int new_row, row *map_area, sort **sort_array)
{
	pre2_sort = sort_array[row_under_examination+new_row];
	curr2_sort = sort_array[row_under_examination+new_row];

	if (map_area[row_under_examination+new_row].row_max > (map_area[row_under_examination+new_row].filled + curr_sort->next_cell->sizeX)) {
		curr_sort->next_cell->coordinates_y = stdCellHeight*(row_under_examination-new_row);
		if (curr2_sort->next == NULL) {
			update_map_area(row_under_examination, new_row, map_area);
			set_cell_to_next_row();
		}
		else {
			while(curr2_sort->next!=NULL) {
				if((curr2_sort->next->next_cell->coordinates_x) > (curr_sort->next_cell->coordinates_x)) {
					update_map_area(row_under_examination, new_row, map_area);
					set_cell_to_next_row();
					return true;
				}
				pre2_sort = pre2_sort->next;
				curr2_sort = curr2_sort->next;
			}
			update_map_area(row_under_examination, new_row, map_area);
			set_cell_to_next_row();
		}
		return true;					
	}
	return false;
}		
	
void move_cell_where_it_fits(int row_under_examination, int rows_number, row *map_area, sort **sort_array)
{
	int new_row = 1;
	double length = 0;
	pre_sort = sort_array[row_under_examination];
	curr_sort = sort_array[row_under_examination]->next;
			
	while(curr_sort != NULL) {	
		bool replaced = false;
		if (map_area[row_under_examination].row_max > (length + curr_sort->next_cell->sizeX)) {
			curr_sort->next_cell->coordinates_x = length;
			length += curr_sort->next_cell->sizeX;
		}
		else {
			new_row = 1;
			while ((row_under_examination-new_row) >=0 || (row_under_examination+new_row) < rows_number) {
				if(row_under_examination-new_row >= 0) {
					replaced = check_if_fits_down(row_under_examination, new_row, map_area, sort_array);							
				}		
				if ((row_under_examination+new_row < rows_number) && !replaced) {
					replaced = check_if_fits_down(row_under_examination, new_row, map_area, sort_array);							
				}
				if(replaced) {
					break;
				}
				new_row++;
			}
		}
		if(!replaced) {
			pre_sort=pre_sort->next;
			curr_sort=curr_sort->next;
		}
	}
}

void move_cells_that_do_not_fit_in_rows(int rows_number, sort **sort_array,row *map_area)
{
	int i;

	for (i=0 ; i < rows_number; i++) {
		
		pre_sort=sort_array[i];
		curr_sort=sort_array[i]->next;
		if (!cells_fit_in_row(i, map_area)) {	
			if (row_is_first(i)) {
				move_cell_to_row_up(i, map_area, sort_array);
			}
			else if(row_is_last(i, rows_number)) {
				move_cell_to_rows_down(i, rows_number, map_area, sort_array);
			}
			else {
				move_cell_where_it_fits(i, rows_number, map_area, sort_array);
			}
		}
	}
}

void legalize_every_row(int rows_number, sort **sort_array, double chip_width)
{
	int i;
	for(i=0; i<rows_number; i++) {
		double over=0;
		int overlap=0;
		curr_sort=sort_array[i]->next;
		do {
			overlap=0;
			if(curr_sort != NULL) {
				while(curr_sort->next != NULL) {
					if(curr_sort->next_cell->fixed == 1) {
						if(curr_sort->next !=NULL) {
							if((curr_sort->next->next_cell->fixed !=1)&&((curr_sort->next_cell->coordinates_x + curr_sort->next_cell->sizeX)>curr_sort->next->next_cell->coordinates_x)) {
								overlap=1;
								if((curr_sort->next_cell->coordinates_x+curr_sort->next_cell->sizeX + 0.001)>=chip_width) {
									curr_sort->next->next_cell->coordinates_x=chip_width- curr_sort->next->next_cell->sizeX;
									curr_sort->next->next_cell->fixed=0;
									curr_sort->next_cell->fixed=0;
									break;
								}
								else {
									curr_sort->next->next_cell->coordinates_x=curr_sort->next_cell->coordinates_x + curr_sort->next_cell->sizeX;
									curr_sort->next->next_cell->fixed=1;
								}					
							}
							else if(((curr_sort->next->next_cell->fixed ==1)&&((curr_sort->next_cell->coordinates_x + curr_sort->next_cell->sizeX)>curr_sort->next->next_cell->coordinates_x+0.0001))) {
								overlap=1;
								curr_sort->next->next_cell->fixed =0;
							}
						}
					}
					else if((curr_sort->next_cell->coordinates_x + curr_sort->next_cell->sizeX)>curr_sort->next->next_cell->coordinates_x + 0.00001) {	
						over=fabs(((curr_sort->next_cell->coordinates_x + curr_sort->next_cell->sizeX)- curr_sort->next->next_cell->coordinates_x)/2);
						if((curr_sort->next_cell->coordinates_x - over )<0) {
							curr_sort->next_cell->coordinates_x=0;
							curr_sort->next->next_cell->coordinates_x=curr_sort->next_cell->sizeX;
							curr_sort->next_cell->fixed=1;
							curr_sort->next->next_cell->fixed=1;
						}
						else if((curr_sort->next->next_cell->coordinates_x + curr_sort->next->next_cell->sizeX+ over )>chip_width) {
							curr_sort->next->next_cell->coordinates_x=chip_width - curr_sort->next->next_cell->sizeX;
							curr_sort->next_cell->coordinates_x=curr_sort->next->next_cell->coordinates_x - curr_sort->next_cell->sizeX;
							curr_sort->next_cell->fixed=0;
							curr_sort->next->next_cell->fixed=1;
						}
						else {
							if(curr_sort->next->next_cell->fixed==1) {
					  			curr_sort->next_cell->coordinates_x= curr_sort->next->next_cell->coordinates_x- curr_sort->next_cell->sizeX;
								curr_sort->next_cell->fixed=1;
							}	
							else {
								curr_sort->next_cell->coordinates_x-=over;
								curr_sort->next->next_cell->coordinates_x+=over;
							}
						}
						overlap=1;
					}
					curr_sort=curr_sort->next;
				}
			}
			curr_sort=sort_array[i]->next;
		} while(overlap==1);
	}
}

void init_map_area(int rows_number, row *map_area, double chip_width)
{
	int i;

	for(i = 0; i<rows_number; i++) {
		map_area[i].filled = 0;
		map_area[i].row_max = chip_width;
	}
}

void legalizer_abacus_alike(double chip_height, double chip_width, struct cell_t **cells_array)
{
	void refresh_coordinates();

	legalize_y(chip_height, cells_array);

	int rows_number =(int) round(chip_height / stdCellHeight);
	sort *sort_array[rows_number];	

	insert_cells_in_rows(sort_array, rows_number, cells_array);

	row map_area[rows_number];
	init_map_area(rows_number, map_area, chip_width);

	calculate_row_utilization(rows_number, sort_array, map_area);

	move_cells_that_do_not_fit_in_rows(rows_number, sort_array, map_area);

	legalize_every_row(rows_number, sort_array, chip_width);
/*
int i;
	printf("\n\n\n");
	for(i=0; i< rows_number; i++) {
		printf ("row %d\n", i);
		curr_sort = sort_array[i]->next;
		while(curr_sort) {
			printf(" name: %s %lf %lf", curr_sort->next_cell->name, curr_sort->next_cell->coordinates_x, curr_sort->next_cell->coordinates_x+curr_sort->next_cell->sizeX);
			curr_sort = curr_sort->next;
		}
		printf("\n"); 
	}
	printf("\n\n\n");
*/
}
