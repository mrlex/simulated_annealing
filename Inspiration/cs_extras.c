#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "gordian.h"


/* resizes a sparse matrix to the specified values */
int cs_resize (cs *A, int m, int n, int nzmax){
	int ok;

	if (!A || !CS_TRIPLET (A) || m < 0 || n < 0 || nzmax < 0)
		return (0); /* check inputs */

	A->m = m; /* define dimensions and nzmax */
	A->n = n;
	if(A->nzmax < nzmax){
		A->nzmax = CS_MAX (nzmax, 1);

		A->p = cs_realloc(A->p, nzmax, sizeof(int), &ok);
		if(!ok) return (0);

		A->i = cs_realloc(A->i, nzmax, sizeof(int), &ok);
		if(!ok) return (0);

		A->x = cs_realloc(A->x, nzmax, sizeof(double), &ok);
		if(!ok) return (0);
		}

	return (1);
	}


/* prints a sparse CSC matrix to a triplet form */
void cs_print_triplet(const cs *A, const char *outputFilename){

	int p, j, n, *Ap, *Ai;
	double *Ax = NULL;

	FILE *outputFilePtr;

	outputFilePtr = fopen(outputFilename, "w+");
	if (outputFilePtr == NULL) {

		fprintf(stderr, "Could not open output file %s for writing\n", outputFilename);
		printf( "Could not open output file %s for writing\n", outputFilename);
		exit(EXIT_FAILURE);
	}

	if (!A) {
		fprintf(outputFilePtr, "(null)\n");
		printf( "(null)\n");
		return;
	}

	n = A->n;
	Ap = A->p;
	Ai = A->i;
	Ax = A->x;
	for (j = 0; j < n; j++)
	{
		for (p = Ap[j]; p < Ap[j + 1]; p++)
		{
			fprintf(outputFilePtr, "%d	%d	%.8lf\n", (int)Ai[p], (int)j, Ax ? Ax[p] : 1);

		}
	}

	fclose(outputFilePtr);
}



void cs_print_to_screen(cs *A){

	int n = A->n, m = A->m, i, j, k, l;
	int *Ap = A->p;
	int *Ai = A->i;
	double *Ax = A->x;
	char found;

	printf("\n");

	for (i = 0; i < m; i++){
		for (j = 0; j < n; j++){

			found = 0;
			for (k = 0; k < n; k++){
				for (l = Ap[k]; l < Ap[k + 1]; l++){
					if (Ai[l] == i && k == j){
						if(Ax[l]>=0.0) printf("+%.1lf \t", Ax[l]);
						else printf("%.1lf \t", Ax[l]);
						found = 1;
						break;
						}
					}
				}
			if(!found) printf(" 0.0 \t");
			}
		printf("\n");
		}
	}



int count_zeros (cs *A){

	int i, j, ctr=0;

	for(i=0; i<A->n; i++)
		for(j=A->p[i]; j<A->p[i+1]; j++)
			if(A->x[j] == 0.0) ctr++;

	return (ctr);
	}



