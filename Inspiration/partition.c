#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "gordian.h"

#define MORE_AREA 1.3

int new_lists_counters[9] = {0,};

void init_partitions_list (){

    struct cell_t *cur_cell;

    partitions = (struct partition_t*) malloc (sizeof(struct partition_t));
    NULL_CHECK(partitions);

    partitions->contents = (struct cell_t**) malloc (movablesCtr * sizeof(struct cell_t*));
    NULL_CHECK(partitions->contents);

    //fill the partitions contents with pointers to the cells (only movable cells, not terminals)
    cur_cell = cells_list;
    while (cur_cell!=NULL) { 
        if (!cur_cell->terminal) {
            partitions->contents[cur_cell->index] = cur_cell;
        }
        cur_cell = cur_cell->next;
    }

    partitions->contentsCtr = movablesCtr;
    partitions->centerX = coreAreaWidth / 2.0;
    partitions->centerY = coreAreaHeight / 2.0;
    partitions->minX = 0.0;
    partitions->minY = 0.0;
    partitions->maxX = coreAreaWidth;
    partitions->maxY = coreAreaHeight;
    partitions->area = coreAreaWidth * coreAreaHeight;
    partitions->next = NULL;
    partitions_tail = partitions;
    partitionsCtr = 1;

}

void free_partitions_list (){

    struct partition_t *p=partitions, *pnext;
    while(p!=NULL){
        pnext = p->next;
//        if (p->contents && p->contents[0])
//            free(p->contents);
        free(p);
        p = pnext;
    }
}


/****************************************************************************************************************************************
 * Calculates and returns the total area that all the cells in partition occupy.                            *
 ****************************************************************************************************************************************/
/* #ifdef USE_AREA_CONSTRAINT  */
double partition_area (struct partition_t *partition){

    double area=0.0;
    int i;

    for (i=0; i<partition->contentsCtr; i++) {
        area += partition->contents[i]->sizeX * partition->contents[i]->sizeY;
        }

    return (area);
    }
/* #endif  */


/****************************************************************************************************************************************
 * Splits a partition in two (left and right). The left partition keeps a percentage of cells according to (a), and the right partition    *
 * keeps a percentage of cells according to (1-a). Returns a pointer to the right partition. If the cut_direction is 'Y', then the left    *
 * and right partitions correspond to the lower and upper partition respectively.                            *
 ****************************************************************************************************************************************/
struct partition_t* bisect_partition (struct partition_t *partition, double alpha, char cut_direction){

    struct partition_t *new;
        double gs_x, gs_y, gs_w, gs_h, gs_area, gs_temp, gs_diff;

    #ifdef USE_AREA_CONSTRAINT
    double total_area = partition->area;
    #endif 
 
    gs_area=partition_area(partition);
    gs_x=partition->centerX;
    gs_y=partition->centerY;
    gs_w=partition->maxX-partition->minX;
    gs_h=partition->maxY-partition->minY;
    gs_temp=gs_w*gs_h;
    gs_diff=(gs_temp-gs_area)/gs_area*100.0;
        printf("\nProcessing partition X=%3.1lf Y=%3.1lf W=%3.1lf H=%3.1lf with area Area %3.1lf WxH %3.1lf %3.1lf%% with %d cells\n", gs_x, gs_y, gs_w, gs_h, gs_area, gs_temp, gs_diff, partition->contentsCtr);

    //if contentsCtr is odd, it gives the extra cell to the right partition
    //int partition_contents_ctr = (int)(partition->contentsCtr * alpha);
    //int new_contents_ctr = partition->contentsCtr - partition_contents_ctr;

    //if contentsCtr is odd, it gives the extra cell to the left partition
    int new_contents_ctr = (int)(partition->contentsCtr * alpha);
    int partition_contents_ctr = partition->contentsCtr - new_contents_ctr;

/*int i;
double area=0.0;

for (i=0; i<partition->contentsCtr; i++){
    if (area >= partition->area * alpha) break;
    else area += partition->contents[i]->sizeX * partition->contents[i]->sizeY;
    }
if (i==partition->contentsCtr) i = (int)(partition->contentsCtr/2);
new_contents_ctr = i;
partition_contents_ctr = partition->contentsCtr - new_contents_ctr;*/


    //allocate memory for the right partition
    new = (struct partition_t*) malloc (sizeof(struct partition_t));
    NULL_CHECK(new);

    //let the left partition keep a portion of the cells
    partition->contentsCtr = partition_contents_ctr;

    //give the rest cells to the right partition
    new->contents = &(partition->contents[partition_contents_ctr]);
    new->contentsCtr = new_contents_ctr;

    //calculate the new borders and centers of the left and right partition
    if(cut_direction =='X'){
        new->maxX = partition->maxX;
        partition->maxX = partition->minX + (partition->maxX - partition->minX) * (double)partition_contents_ctr / (double)(partition_contents_ctr + new_contents_ctr);
        new->minX = partition->maxX;

        partition->centerX = (partition->minX + partition->maxX) / 2.0;
        new->centerX = (new->minX + new->maxX) / 2.0;

        new->minY = partition->minY;
        new->maxY = partition->maxY;
        new->centerY = partition->centerY;
        }
    else if(cut_direction =='Y'){
        new->maxY = partition->maxY;
        partition->maxY = partition->minY + (partition->maxY - partition->minY) * (double)partition_contents_ctr / (double)(partition_contents_ctr + new_contents_ctr);
        new->minY = partition->maxY;

        partition->centerY = (partition->minY + partition->maxY) / 2.0;
        new->centerY = (new->minY + new->maxY) / 2.0;

        new->minX = partition->minX;
        new->maxX = partition->maxX;
        new->centerX = partition->centerX;
        }
    else{
        printf("\nERROR: invalid argument in bisect_partition\n");
        return NULL;
        }

    //make the list connections
    new->next = partition->next;
    partition->next = new;

    #ifdef USE_AREA_CONSTRAINT 
    //calculate the total area of the cells in the two partitions
    new->area = partition_area(new);
    partition->area = total_area - new->area;
    #endif 

    partitionsCtr++;
    /*xxx*/
     printf("Broken into:\n");
    gs_area=partition_area(partition);
    gs_x=partition->centerX;
    gs_y=partition->centerY;
    gs_w=partition->maxX-partition->minX;
    gs_h=partition->maxY-partition->minY;
    gs_temp=gs_w*gs_h;
    gs_diff=(gs_temp-gs_area)/gs_area*100.0;
        printf("     Left  partition X=%3.1lf Y=%3.1lf W=%3.1lf H=%3.1lf with area Area %3.1lf WxH %3.1lf %3.1lf%% with %d cells\n", gs_x, gs_y, gs_w, gs_h, gs_area, gs_temp, gs_diff, partition->contentsCtr);

 
    gs_area=partition_area(new);
    gs_x=new->centerX;
    gs_y=new->centerY;
    gs_w=new->maxX-new->minX;
    gs_h=new->maxY-new->minY;
    gs_temp=gs_w*gs_h;
    gs_diff=(gs_temp-gs_area)/gs_area*100.0;
        printf("     Right partition X=%3.1lf Y=%3.1lf W=%3.1lf H=%3.1lf with area Area %3.1lf WxH %3.1lf %3.1lf%% with %d cells\n", gs_x, gs_y, gs_w, gs_h, gs_area, gs_temp, gs_diff, new->contentsCtr);


    return new;
    }


/****************************************************************************************************************************************
 * Calculates and returns the value of alpha, that corresponds to the cut-line that crosses the minimum number of nets.            *
 ****************************************************************************************************************************************/
double minimum_alpha (struct partition_t *partition, char cut_direction, double gamma){

    //double min_alpha = 0.5 - gamma;
    //double max_alpha = 0.5 + gamma;
    int i;
    double alpha=0.5;
    int min_net_cuts=netsCtr;

    if (cut_direction == 'X'){
        int mid;

        for (i=0; i<partition->contentsCtr && partition->contents[i]->centerX < partition->centerX; i++);
        mid=i;

        for (i=mid-(int)(((double)(partition->contentsCtr))*gamma/2.0); i<=mid+(int)(((double)(partition->contentsCtr))*gamma/2.0); i++){
            int cur_net_cuts=0;
            int j;

            for (j=0; j<netsCtr; j++){
                int k;
                double minX=coreAreaWidth, maxX=0.0;

                //find the borders of the net
                for(k=0; k<nets[j].connectionsCtr; k++){
                    if(nets[j].connections[k]->centerX < minX) minX = nets[j].connections[k]->centerX;
                    if(nets[j].connections[k]->centerX > maxX) maxX = nets[j].connections[k]->centerX;
                    }
                if (partition->contents[i]->centerX < maxX && partition->contents[i]->centerX > minX) cur_net_cuts++;
                }

            if (cur_net_cuts < min_net_cuts){
                min_net_cuts = cur_net_cuts;
                alpha = ((double)i) / ((double)partition->contentsCtr);
//alpha = (partition->contents[i]->centerX - partition->minX) / (partition->maxX - partition->minX);
                }
            }
        }

    else if (cut_direction == 'Y'){

        int mid;

        for (i=0; i<partition->contentsCtr && partition->contents[i]->centerY < partition->centerY; i++);
        mid=i;

        for (i=mid-(int)(((double)(partition->contentsCtr))*gamma/2.0); i<=mid+(int)(((double)(partition->contentsCtr))*gamma/2.0); i++){
            int cur_net_cuts=0;
            int j;
            /* int found; */

            for (j=0; j<netsCtr; j++){
                int k;
                double minY=coreAreaHeight, maxY=0.0;
                
                //find the borders of the net
                for(k=0; k<nets[j].connectionsCtr; k++){
                    if(nets[j].connections[k]->centerY < minY) minY = nets[j].connections[k]->centerY;
                    if(nets[j].connections[k]->centerY > maxY) maxY = nets[j].connections[k]->centerY;
                    }
                if (partition->contents[i]->centerY < maxY && partition->contents[i]->centerY > minY) cur_net_cuts++;
                }

            if (cur_net_cuts < min_net_cuts){
                min_net_cuts = cur_net_cuts;
                alpha = ((double)i) / ((double)partition->contentsCtr);
                }
            }
        }

//printf("\nstarting: %.0lf\n", partition->minX + (partition->maxX - partition->minX) * min_alpha);
//printf("\nstopping: %.0lf\n", partition->minX + (partition->maxX - partition->minX) * max_alpha);

        /*for (i=0; i<partition->contentsCtr && partition->contents[i]->centerX < partition->minX + (partition->maxX - partition->minX) * min_alpha; i++);

//printf("\nsel: %d from %d\n", i, partition->contentsCtr);

        if (i==partition->contentsCtr) printf ("\n\nAAAAAAAAAAAAAAAAAAAAAAAAAAA\n\n");

        for (; partition->contents[i]->centerX <= partition->minX + (partition->maxX - partition->minX) * max_alpha; i++){
            int cur_net_cuts=0;
            int j;
            int found;


            //if (partition->contents[i]->centerX < partition->minX + (partition->maxX - partition->minX) * min_alpha || partition->contents[i]->centerX > partition->minX + (partition->maxX - partition->minX) * max_alpha) continue;

            for (j=0; j<netsCtr; j++){
                int k;
                double minX=coreAreaWidth, maxX=0.0;

                found=0;

                //find the borders of the net
                for(k=0; k<nets[j].connectionsCtr; k++){
                    if(nets[j].connections[k]->centerX < minX) minX = nets[j].connections[k]->centerX;
                    if(nets[j].connections[k]->centerX > maxX) maxX = nets[j].connections[k]->centerX;
                    }
                if (partition->contents[i]->centerX < maxX && partition->contents[i]->centerX > minX) cur_net_cuts++;
                }

            if (cur_net_cuts < min_net_cuts){
                min_net_cuts = cur_net_cuts;
                alpha = ((double)i) / ((double)partition->contentsCtr);
//alpha = (partition->contents[i]->centerX - partition->minX) / (partition->maxX - partition->minX);
                }
            }
        }*/

        /*for (i=(int)(partition->contentsCtr * min_alpha); i<=(int)(partition->contentsCtr * max_alpha); i++){
            int cur_net_cuts=0;
            int j;
            int found;

            for (j=0; j<netsCtr; j++){
                int k;
                double minX=coreAreaWidth, maxX=0.0;

                found=0;
 
                //first check is this net has any cells inside the partition
                for(k=0; k<nets[j].connectionsCtr; k++){
                    if (nets[j].connections[k]->centerX < partition->maxX && nets[j].connections[k]->centerX > partition->minX && nets[j].connections[k]->centerY < partition->maxY && nets[j].connections[k]->centerY > partition->minY){
                        found=1;
                        break;
                        }
                    }

                if (found){                
                    //find the borders of the net
                    for(k=0; k<nets[j].connectionsCtr; k++){
                        if(nets[j].connections[k]->centerX < minX) minX = nets[j].connections[k]->centerX;
                        if(nets[j].connections[k]->centerX > maxX) maxX = nets[j].connections[k]->centerX;
                        }
                    if (partition->contents[i]->centerX < maxX && partition->contents[i]->centerX > minX) cur_net_cuts++;
                    }
                }

            if (cur_net_cuts < min_net_cuts){
                min_net_cuts = cur_net_cuts;
                alpha = ((double)i) / ((double)partition->contentsCtr);
                }
            }
        }*/

//    else if (cut_direction == 'Y'){ alpha=0.5; }

        /*for (i=0; i<partition->contentsCtr; i++){
            int cur_net_cuts=0;
            int j;
            int found;

            if (partition->contents[i]->centerY < min_alpha || partition->contents[i]->centerY > max_alpha) continue;
            
            for (j=0; j<netsCtr; j++){
                int k;
                
                found=0;
 
                //first check is this net has any cells inside the partition
                for(k=0; k<nets[j].connectionsCtr; k++){
                    if (nets[j].connections[k]->centerX < partition->maxX && nets[j].connections[k]->centerX > partition->minX && nets[j].connections[k]->centerY < partition->maxY && nets[j].connections[k]->centerY > partition->minY){
                        found=1;
                        break;
                        }
                    }

                if (found){
                    double minY=coreAreaHeight, maxY=0.0;
                
                    //find the borders of the net
                    for(k=0; k<nets[j].connectionsCtr; k++){
                        if(nets[j].connections[k]->centerY < minY) minY = nets[j].connections[k]->centerY;
                        if(nets[j].connections[k]->centerY > maxY) maxY = nets[j].connections[k]->centerY;
                        }
                    if (partition->contents[i]->centerY < maxY && partition->contents[i]->centerY > minY) cur_net_cuts++;
                    }
                }

            if (cur_net_cuts < min_net_cuts){
                min_net_cuts = cur_net_cuts;
                alpha = ((double)i) / ((double)partition->contentsCtr);
                }
            }
        }*/

        /*for (i=(int)(partition->contentsCtr * min_alpha); i<=(int)(partition->contentsCtr * max_alpha); i++){
            int cur_net_cuts=0;
            int j;
            int found;

            for (j=0; j<netsCtr; j++){
                int k;
                double minY=coreAreaHeight, maxY=0.0;

                found=0;
 
                //first check is this net has any cells inside the partition
                for(k=0; k<nets[j].connectionsCtr; k++){
                    if (nets[j].connections[k]->centerX < partition->maxX && nets[j].connections[k]->centerX > partition->minX && nets[j].connections[k]->centerY < partition->maxY && nets[j].connections[k]->centerY > partition->minY){
                        found=1;
                        break;
                        }
                    }

                if (found){                
                    //find the borders of the net
                    for(k=0; k<nets[j].connectionsCtr; k++){
                        if(nets[j].connections[k]->centerY < minY) minY = nets[j].connections[k]->centerY;
                        if(nets[j].connections[k]->centerY > maxY) maxY = nets[j].connections[k]->centerY;
                        }
                    if (partition->contents[i]->centerY < maxY && partition->contents[i]->centerY > minY) cur_net_cuts++;
                    }
                }
    
            if (cur_net_cuts < min_net_cuts){
                min_net_cuts = cur_net_cuts;
                alpha = ((double)i) / ((double)partition->contentsCtr);
                }
            }
        }*/

//printf ("%.2lf\t", alpha);
    return (alpha);
}

void calculate_trisection_partitions_cuts(double x_points[], double y_points[], double min_width, double max_width, double min_height, double max_height)
{
    x_points[0] = min_width;
    x_points[1] = min_width + ((max_width - min_width) * 33.0/100.0);
    x_points[2] = min_width + ((max_width - min_width) * 67.0/100.0);
    x_points[3] = max_width;
    
    y_points[0] = min_height;
    y_points[1] = min_height + ((max_height - min_height) * 33.0/100.0);
    y_points[2] = min_height + ((max_height - min_height) * 67.0/100.0);
    y_points[3] = max_height;
}

void create_partition_item(double x_points[], double y_points[], int i, int j)
{
    curr_partition = (struct partition_t*) malloc (sizeof(struct partition_t));
    NULL_CHECK(curr_partition);
    
    curr_partition->minX = x_points[j];
    curr_partition->minY = y_points[i];
    curr_partition->maxX = x_points[j + 1];
    curr_partition->maxY = y_points[i + 1];
    curr_partition->centerX = (curr_partition->minX + curr_partition->maxX) / 2.0;
    curr_partition->centerY = (curr_partition->minY + curr_partition->maxY) / 2.0;
    curr_partition->area = (curr_partition->maxX - curr_partition->minX) * (curr_partition->maxY - curr_partition->minY);
    curr_partition->cell_area = 0;
    curr_partition->contentsCtr = 0;
    curr_partition->next = NULL;
}

void print_cells_in_partition(struct cell_t** point_cell, int counter)
{
    int i = 0;
    for (i = 0; i < counter; i++) {
        printf("name: %s -> x : %lf  y: %lf \n", point_cell[i]->name, point_cell[i]->centerX, point_cell[i]->centerY); 
    }
}

void create_partitions_list(double x_points[], double y_points[])
{
    int i, j =0;

    for (i = 0; i < 3; i++) {
        for (j = 0; j < 3; j++) {
            create_partition_item(x_points, y_points, i, j);
            if (root_partition == NULL) {
                root_partition = curr_partition;
                tail_partition = root_partition;
            }
            else {
               tail_partition->next = curr_partition;
               tail_partition = curr_partition;
            }
        }
    }
}

void init_new_cells_lists()
{
    memset(&list, 0, sizeof(list));
    memset(&new_lists_counters, 0, sizeof(new_lists_counters));
}

double calculate_distance (double cellX, double cellY, double centerX, double centerY) 
{
    return sqrt(pow((centerX - cellX), 2) + pow((centerY - cellY), 2));
}

void calculate_cells_distance_from_partitions(int position)
{
    int i = 0;

    for (i = position; i < partitions->contentsCtr; i++) {
        curr_partition = root_partition;
        int j = 0;
        double min_distance = INFINITY;

        for (j = 0; j < 9 ; j++) {
            if (position > 0 && j == 4) {
                partitions->contents[i]->distance_info[j].distance = INFINITY;
                partitions->contents[i]->distance_info[j].partition_number = j;
            }
            else {
                partitions->contents[i]->distance_info[j].distance = calculate_distance(partitions->contents[i]->centerX, partitions->contents[i]->centerY, curr_partition-> centerX, curr_partition->centerY);
                partitions->contents[i]->distance_info[j].partition_number = j;
            }

            if (min_distance > partitions->contents[i]->distance_info[j].distance) {
                min_distance = partitions->contents[i]->distance_info[j].distance;
            }
            curr_partition = curr_partition->next;    
        }
        partitions->contents[i]->distance = min_distance;
    }
}


void calculate_distance_from_center()
{
    int i = 0;

    for (i = 0; i < partitions->contentsCtr; i++) {
        partitions->contents[i]->distance = calculate_distance(partitions->contents[i]->centerX, partitions->contents[i]->centerY, partitions-> centerX, partitions->centerY);
    }
     
}

void sort_cells_by_distance() 
{
    calculate_distance_from_center();
    new_quicksort(partitions->contents, 0, partitions->contentsCtr-1);
}

void sort_cells_by_distances(int position) 
{
    calculate_cells_distance_from_partitions(position);
    new_quicksort(partitions->contents, 0, partitions->contentsCtr-1);
}

void split_cells_by_three()
{
    //printf("cells in start partition: \n");
    //print_cells_in_partition(partitions->contents, partitions->contentsCtr);
    
    quicksort (partitions->contents, 0, partitions->contentsCtr-1, 'Y');
    int counter1 = floor(partitions->contentsCtr *33.0/100.0);
    int counter2 = ceil(partitions->contentsCtr *34.0/100.0);
    
    struct cell_t** list_y0 = NULL;
    struct cell_t** list_y1 = NULL;
    struct cell_t** list_y2 = NULL;
    list_y0 = &partitions->contents[0];
    list_y1 = &partitions->contents[counter1];
    list_y2 = &partitions->contents[counter1+counter2];

    int list_y0_ctr = counter1;
    int list_y1_ctr = counter2;
    int list_y2_ctr = partitions->contentsCtr - (counter1 + counter2);

/*    printf("\nfirst list\n");
    print_cells_in_partition(list1, list1_ctr);
    printf("\nsecond list\n");

    print_cells_in_partition(list2, list2_ctr);
    printf("\nthird list\n");
    print_cells_in_partition(list3, list3_ctr);    
*/
    quicksort (list_y0, 0, list_y0_ctr-1, 'X');
    quicksort (list_y1, 0, list_y1_ctr-1, 'X');
    quicksort (list_y2, 0, list_y2_ctr-1, 'X');

    counter1 = floor(list_y0_ctr *33.0/100.0);
    counter2 = ceil(list_y0_ctr *34.0/100.0);
    list[0] = list_y0;
    list[1] = &list_y0[counter1];
    list[2] = &list_y0[counter2+counter1];

    new_lists_counters[0] = counter1;
    new_lists_counters[1] = counter2;
    new_lists_counters[2] = list_y0_ctr - (counter1 + counter2);
/*
 *  printf("\nfirst list\n");
    print_cells_in_partition(list11, list11_ctr);
    printf("\nsecond list\n");

    print_cells_in_partition(list12, list12_ctr);
    printf("\nthird list\n");
    print_cells_in_partition(list13, list13_ctr);    
*/

    counter1 = floor(list_y1_ctr *33.0/100.0);
    counter2 = ceil(list_y1_ctr *34.0/100.0);
    list[3] = list_y1;
    list[4] = &list_y1[counter1];
    list[5] = &list_y1[counter2+counter1];

    new_lists_counters[3] = counter1;
    new_lists_counters[4] = counter2;
    new_lists_counters[5] = list_y1_ctr - (counter1 + counter2);
/*
 * printf("\nfirst list\n");
    print_cells_in_partition(list21, list21_ctr);
    printf("\nsecond list\n");

    print_cells_in_partition(list22, list22_ctr);
    printf("\nthird list\n");
    print_cells_in_partition(list23, list23_ctr);    
*/    
    counter1 = floor(list_y2_ctr *33.0/100.0);
    counter2 = ceil(list_y2_ctr *34.0/100.0);
    list[6] = list_y2;
    list[7] = &list_y2[counter1];
    list[8] = &list_y2[counter2+counter1];

    new_lists_counters[6] = counter1;
    new_lists_counters[7] = counter2;
    new_lists_counters[8] = list_y2_ctr - (counter1 + counter2);
/*
    printf("\nfirst list\n");
    print_cells_in_partition(list31, list31_ctr);
    printf("\nsecond list\n");

    print_cells_in_partition(list32, list32_ctr);
    printf("\nthird list\n");
    print_cells_in_partition(list33, list33_ctr);    
*/
}

int find_minimum_distance(double all_distances[])
{
    int i = 0, position = 0;
    double turn_min_distance = INFINITY;

    for (i = 0; i < 9; i++) {
        if (i == 4 || turn_min_distance < all_distances[i]) {
            continue;
        }
        else {
            turn_min_distance = all_distances[i];
            position = i;
        }    
    }
    return position;
}

void assign_cells_by_distance() 
{
    int i , j = 0;
    int counter = 0;
    double total_area = 0;
    //curr partition = central partition
    curr_partition = root_partition->next->next->next->next;

    for (i = 0; i < partitions->contentsCtr; i++) {
        if (total_area < (curr_partition->area * 1.2)) {
            counter++;
            total_area += partitions->contents[i]->area;
        }
        else {
            break;
        }
    }
    curr_partition->contents = (struct cell_t**) malloc (counter * sizeof(struct cell_t*));
    curr_partition->contentsCtr = counter;
    
    for(i = 0; i < counter; i++) {
        curr_partition->contents[i] = partitions->contents[i];
    }
   
    /* now that the cental partition is full check other */

    curr_partition = root_partition;
    for(i = counter; i < partitions->contentsCtr; i++) {
        double all_distances[9] = {0.0};
        curr_partition = root_partition;

        for(j = 0; j < 9; j++) {
            all_distances[j]  = calculate_distance(partitions->contents[i]->centerX, partitions->contents[i]->centerY, curr_partition->centerX, curr_partition->centerY);
            curr_partition = curr_partition->next;
        }
        
        while (1) { 
            int check_position = find_minimum_distance(all_distances);
            curr_partition = root_partition;
            for(j = 0; j < check_position; j++) {
                curr_partition = curr_partition->next;
            }
            if (partitions->contents[i]->area + curr_partition->cell_area < curr_partition->area * MORE_AREA) {
                curr_partition->contentsCtr++;
                if (curr_partition->contentsCtr == 1) {
                     curr_partition->contents = (struct cell_t**) malloc (sizeof(struct cell_t*));
                }
                else {
                    curr_partition->contents = (struct cell_t**) realloc (curr_partition->contents, curr_partition->contentsCtr * sizeof(struct cell_t*));
                }
                curr_partition->contents[curr_partition->contentsCtr - 1] = partitions->contents[i];
                curr_partition->cell_area += partitions->contents[i]->area;
                break;
            }
            else {
                all_distances[check_position] = INFINITY; 
            }
        }
    }
}

int find_partition_with_minimum_distance(struct cell_t *cell) 
{
    int i;

    for (i = 0; i < 9; i++) {
        if (cell->distance == cell->distance_info[i].distance) {
            return cell->distance_info[i].partition_number;
        }
    }
    
    return -1;
}

void find_next_minimum_distance(struct cell_t *cell) 
{
    int i;

    for (i = 0; i < 9; i++) {
        if (cell->distance == cell->distance_info[i].distance) {
            cell->distance_info[i].distance = INFINITY;
            break;
        }
    }
    
    double min_distance = INFINITY;

    for (i = 0; i < 9; i++) {
        if (min_distance > cell->distance_info[i].distance) {
            min_distance = cell->distance_info[i].distance;
        }    
    }
    
    cell->distance = min_distance;
}

int first_place_cells_in_central_partition()
{
    int i, counter = 0;
    double total_area = 0;
    curr_partition = root_partition->next->next->next->next;

    for (i = 0; i < partitions->contentsCtr; i++) {
        if (total_area < (curr_partition->area * MORE_AREA)) {
            counter++;
            total_area += partitions->contents[i]->area;
        }
        else {
            break;
        }
    }
    curr_partition->contents = (struct cell_t**) malloc (counter * sizeof(struct cell_t*));
    curr_partition->contentsCtr = counter;
    
    for(i = 0; i < counter; i++) {
        partitions->contents[i]->distance = INFINITY;
        curr_partition->contents[i] = partitions->contents[i];
    }

    return i;
}

void assign_all_cells_by_distance() 
{
    int i = 0;
    int check_partition;
    int j = 0;

    while (1) {
        if ( partitions->contentsCtr == i || partitions->contents[i]->distance == INFINITY) {
            //finish loop when found a cell with distance at INFINITY or when you finished all cells
            break;
        }

        check_partition = find_partition_with_minimum_distance(partitions->contents[i]);
        if (check_partition == -1) {
            printf("ERROR happened");
            break;
        }
        printf("for cell %s partition found %d\n", partitions->contents[i]->name, check_partition);
        curr_partition = root_partition;
        for(j = 0; j < check_partition; j++) {
            curr_partition = curr_partition->next;
        }
        if ((partitions->contents[i]->area + curr_partition->cell_area < curr_partition->area * MORE_AREA) || (curr_partition->contentsCtr == 0)) {
            /* accept at least one cell if a partition area is smaller than cell area or if fits in area */
            curr_partition->contentsCtr++;
            if (curr_partition->contentsCtr == 1) {
                printf("here1\n");
                curr_partition->contents = (struct cell_t**) malloc (sizeof(struct cell_t*));
            }
            else {
                printf("here2\n");
                curr_partition->contents = (struct cell_t**) realloc (curr_partition->contents, curr_partition->contentsCtr * sizeof(struct cell_t*));
            }
            curr_partition->contents[curr_partition->contentsCtr - 1] = partitions->contents[i];
            curr_partition->cell_area += partitions->contents[i]->area;
            partitions->contents[i]->distance = INFINITY;
            i++;
        }
        else {
            printf("here3\n");
            find_next_minimum_distance(partitions->contents[i]);
            new_quicksort(partitions->contents, 0, partitions->contentsCtr-1);
            i = 0;
        }
    }
}

void assign_cells()
{
    int i , j = 0;

    curr_partition = root_partition;
    for (i = 0; i < 9; i++) {
        if (new_lists_counters[i] > 0) {
            curr_partition->contents = (struct cell_t**) malloc (new_lists_counters[i] * sizeof(struct cell_t*));
            for (j = 0; j < new_lists_counters[i]; j++) {
                curr_partition->contents[j] = list[i][j];
            }
            curr_partition->contentsCtr = new_lists_counters[i];
        }
        curr_partition = curr_partition->next;
    }
}

void split_cells_to_the_new_partitions_evenly()
{
    init_new_cells_lists();
    split_cells_by_three();
    assign_cells();
    //assign_cells_by_distance();
}

void split_cells_to_the_new_partitions()
{
    /* center first */
    //sort_cells_by_distance();
    //assign_cells_by_distance();

    /*where should go */
    printf("before sorting\n");
    sort_cells_by_distances(0);
    printf("before assigning\n");
    assign_all_cells_by_distance();
}

void enhanced_split_cells_to_the_new_partitions()
{
    int position_to_continue = 0;
    sort_cells_by_distance();
    position_to_continue = first_place_cells_in_central_partition();
    sort_cells_by_distances(position_to_continue);
    assign_all_cells_by_distance();
}

void remove_empty_partition()
{
    int i = 0;
    curr_partition = root_partition;
    pre_partition = root_partition;

    for (i = 0; i < 9; i++) {
       if (curr_partition->contentsCtr == 0) {
           //printf("removing partition : %d\n", i);
           if (curr_partition == root_partition) {
               root_partition = root_partition->next;
               free(curr_partition);
               pre_partition = root_partition;
               curr_partition = root_partition;
               continue;
           }
           else {
               pre_partition->next = curr_partition->next;
               if (curr_partition == tail_partition) {
                   tail_partition = pre_partition;
               }
               tmp_partition = curr_partition;
               curr_partition = curr_partition->next;
               free(tmp_partition);
               continue;
           }
       }
       else {
           partitionsCtr++;
       }
       pre_partition = curr_partition;
       curr_partition = curr_partition->next;
    }
}

void create_new_partition_list(struct partition_t* tmp_partition) 
{
    int i, j = 0;
    double x_points[4] = {0.0,};
    double y_points[4] = {0.0,};
    
    calculate_trisection_partitions_cuts(x_points, y_points, partitions->minX, partitions->maxX, partitions->minY, partitions->maxY);

    root_partition = NULL;
//    printf ("points: x-> %lf, %lf, %lf, %lf  y-> %lf, %lf, %lf, %lf \n", x_points[0], x_points[1], x_points[2], x_points[3], y_points[0], y_points[1], y_points[2], y_points[3]);

    /*create a new partition list by adding elements to the tail*/
    create_partitions_list(x_points, y_points);
    //split_cells_to_the_new_partitions_evenly();
    printf("before splitting cells to partition\n");
    //split_cells_to_the_new_partitions();
    enhanced_split_cells_to_the_new_partitions();
    printf("splitting finished\n");
    remove_empty_partition();
}

void tripartition_area ()
{
    /* while checking all partitions existing from the previous loop
     * create for every partition a list of new 9 partitions with root_partition
     * and tail partition and add them at the end of the previous list.
     * Check when the previous list finishes with previous_loop_tail
     */

    do {
        printf("create list of new partitions..\n");
        create_new_partition_list(partitions);
        
        //swap pointers
        printf("go to the next_partition");
        partitions_tail->next = root_partition;
        curr_partition = partitions;
        partitions = curr_partition->next;
        
        //remove partitioned area from list
        free(curr_partition);
        partitionsCtr--;
        partitions_tail = tail_partition;
    } while (partitions != previous_loop_tail->next);
    printf("finished partitioninf round");
    previous_loop_tail = partitions_tail;
}

void print_partitions_list()
{
    cur_p = partitions;
    while(cur_p) {
        printf("partition %lf %lf %lf %lf, %lf %lf \n",  cur_p-> minX, cur_p-> maxX, cur_p-> minY, cur_p-> maxY, cur_p-> centerX, cur_p-> centerY);
        //printf("cells: \n");
        if (cur_p->contents != NULL) {
            print_cells_in_partition(cur_p->contents, cur_p->contentsCtr);
        }        
        cur_p = cur_p->next;
    }
    printf("Total partitions counter: %d\n\n", partitionsCtr);
}

bool tripartition_finished()
{
    printf("is tripartition finished?\n");
    cur_p = partitions;
    for (; cur_p != NULL; cur_p = cur_p->next) {
        if (cur_p->contentsCtr > 4) {
            return false;
        }
    }
    return true;
}

