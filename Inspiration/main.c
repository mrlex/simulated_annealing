#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <sys/time.h>
#include <malloc.h>
#include <omp.h>
#include "gordian.h"
#include <stdbool.h>

#ifdef USE_STAR_NETS
    int max_net_degree = 4;
    int total_dummy_nodes = 0;    //the total dummy nodes count used in star nets
#endif
cs *C_triplet, *C;            //the Laplacian matrix in triplet form
double *Dx=NULL, *Dy=NULL;    //the Fixed pin vectors for x-coordinate and y-coordinate 
double *x=NULL, *y=NULL;    //the solution vectors
double *ja_w=NULL, *ja_x=NULL, *ja_y=NULL;

#ifndef USE_Z
    unsigned int CA_n;        //the dimention of the matrix containing the Laplacian and the Constraints matrices
    unsigned int CA_nz;        //the count of the non-zero elements in the matrix containing the Laplacian and the Constraints matrices
    unsigned int CA_n_prev;
#endif

extern double grid_min;
extern double avg_area;

/* base 2 integer logarithm */
int ilog2 (double number){ return ceil((log(number)/log(2.0))); }    

/* base 2 decimal logarithm */
int dlog2 (double number){ return ((double)(log(number)/log(2.0))); }

int option_parser (int argc, char *argv[], char **option_plot, char *option_pl, double *option_itol, int *cells_k, 
                   short *option_legalize, short *option_pwl, short *option_wlcalc, int *multiple_cuts, int *iterations) 
{
    int i = 0; 

    if (argc < 3) {
        printf("\nPlease specify: <benchmark_path> <auxiliary_file> [<option1> <option2> ...]?\n\n");
        printf("Options:\n\n");
        printf(" -pl    : Use option -pl to specify a filename for the output pl file. For\n");
        printf("          example if the option \"-pl=test.pl\" is used, then the output file\n");
        printf("          will be named \"test.pl\". If a filename is not specified then the\n");
        printf("          output pl file is named \"default.pl\". If the option is not used\n");
        printf("          then the output pl file is not created.\n\n");
        printf(" -plot  : Use option -plot to specify the results of which partitioning step will\n");
        printf("          be plotted to a png image. Acceptable values are: \"first\", \"last\",\n");
        printf("          \"final\" and \"all\". For example \"-plot=last\" plots the result of the\n");
        printf("          last partitioning step.\n\n");
        #ifdef USE_STAR_NETS
        printf(" -snets : Use option -snets to specify the maximum allowed net degree. Nets with\n");
        printf("          degree greated than the specified, will be converted to star-nets by\n");
        printf("          adding an extra dummy node. The default value is 4.\n\n");
        #endif
        printf(" -itol  : Use option -itol to specify the tolerance of the iterative solver. The\n");
        printf("          default value is 0.001.\n\n");
        printf(" -mcuts : Use option -mcuts to specify the number of consecutive cuts performed\n");
        printf("          before solving. For example \"-mcuts=2\" will cut twice before solving\n");
        printf("          so every partition will be divided into four partitions. The default\n");
        printf("          value is 1.\n\n");
        printf(" -l     : Use option -l to perform legalization on the final results. After this\n");
        printf("          step all cells are placed inside the core area without overlaps.\n\n");
        printf(" -k     : Use option -k to specify the maximun number of cells that every partition\n");
        printf("          can contain after the partitioning steps. The default value is 4.\n\n");
        printf(" -pwl   : Use option -pwl to calculate and print the half perimeter wire length\n");
        printf("          after every partitioning step.\n\n");
        printf(" -wlcalc: Use option -wlcalc to run the hpwl calculator without running the placer.\n\n");
        printf(" -i     : iterations");
        
        /*printf("\nExamples:\n\n");
        printf("          Fast, low-accuracy: ./gordian ibm01/ ibm01.aux -itol=0.0025 -mcuts=2\n\n");
        printf("          High memory needs:    ./gordian ibm01/ ibm01.aux -snets=100\n\n");*/
        return -1;
    }

    #if (PRINTING==FULL)
    printf("\n");
    #endif    

    //parse program input options
    for (i=3; i<argc; i++) {
        if (!strncasecmp(argv[i], "-pl=", 4)) {
            option_pl = strdup(&argv[i][4]);
            NULL_CHECK(option_pl);
        }
        else if (!strncasecmp(argv[i], "-pl", 3) && strlen(argv[i])==3) {
            option_pl = strdup("default.pl");
            NULL_CHECK(option_pl);
        }
        else if (!strncasecmp(argv[i], "-plot=", 6) && strlen(argv[i])>=7) {
            if(!strncasecmp(&argv[i][6], "first", 5)) *option_plot = strdup("first");
            else if(!strncasecmp(&argv[i][6], "last", 4)) *option_plot = strdup("last");
            else if(!strncasecmp(&argv[i][6], "final", 5)) *option_plot = strdup("final"); //after legalization plotting
            else if(!strncasecmp(&argv[i][6], "all", 3)) *option_plot = strdup("all");
            else {
                printf("Invalid value in option -plot\n");
                return -1;
            }    
            NULL_CHECK(option_plot);
        }
        #ifdef USE_STAR_NETS
        else if(!strncasecmp(argv[i], "-snets=", 7) && strlen(argv[i])>=8) {
            sscanf(&argv[i][7], "%d", &max_net_degree);
        }
        #endif
        else if(!strncasecmp(argv[i], "-itol=", 6) && strlen(argv[i])>=7) {
            sscanf(&argv[i][6], "%lf", option_itol);
        }
        else if(!strncasecmp(argv[i], "-mcuts=", 7) && strlen(argv[i])>=8) {
            sscanf(&argv[i][7], "%d", multiple_cuts);
        }
        else if(!strncasecmp(argv[i], "-l", 2)) {
            *option_legalize=1;
        }
        else if(!strncasecmp(argv[i], "-k=", 3) && strlen(argv[i])>=4) {
            sscanf(&argv[i][3], "%d", cells_k);
        }
        else if(!strcasecmp(argv[i], "-pwl")) {
            *option_pwl = 1;
        }
        else if(!strcasecmp(argv[i], "-wlcalc")) {
            *option_wlcalc = 1;
        }
        else if (!strncasecmp(argv[i],"-i=", 3) && strlen(argv[i])>=4) {
            sscanf(&argv[i][3], "%d", iterations);
        }
        else {
            printf("Invalid option %d\n", i);
            return -1;
        }
    }

    if(*option_plot==NULL) *option_plot = strdup("no");
    return 1;
}

unsigned int calculate_laplacian_non_zero_elements()
{
    int i = 0, j = 0 , k = 0;
    unsigned int C_nz = 0;
    for(i = 0; i < netsCtr; i++) {
        #ifdef USE_STAR_NETS
        if(nets[i].connectionsCtr > max_net_degree) {
            for (j = 0; j < nets[i].connectionsCtr; j++) {
                if(!nets[i].connections[j]->terminal) {
                    C_nz += 4;
                }
                else {
                    C_nz += 1;
                }
            }
            total_dummy_nodes++;
            continue;
        }
        #endif

        for (j = 0; j < nets[i].connectionsCtr-1; j++) {
            for (k = j+1; k < nets[i].connectionsCtr; k++) {
                if (!nets[i].connections[j]->terminal && !nets[i].connections[k]->terminal) {
                    C_nz += 4;
                }
                else if (nets[i].connections[j]->terminal || nets[i].connections[k]->terminal) {
                    C_nz += 1;
                }
            }
        }
    }
    return C_nz;
}

void fill_laplacian_matrix(int dummy_node_index) 
{
    int i = 0, j = 0, k = 0;
    for (i = 0; i < netsCtr; i++) {

        double weight = 2.0 / nets[i].connectionsCtr;    //the clique's edge weight

        for (j = 0; j < nets[i].connectionsCtr; j++) {
            if(!nets[i].connections[j]->terminal) {
                nets[i].connections[j]->clique_weight += weight;
            }
        }

        #ifdef USE_STAR_NETS
        if (nets[i].connectionsCtr > max_net_degree) {
            for (j = 0; j < nets[i].connectionsCtr; j++) {

                if (!nets[i].connections[j]->terminal) {
                    //for the Adjacency matrix
                    cs_entry (C_triplet, nets[i].connections[j]->index, dummy_node_index, -weight);
                    cs_entry (C_triplet, dummy_node_index, nets[i].connections[j]->index, -weight);

                    //for the Degree matrix
                    cs_entry (C_triplet, nets[i].connections[j]->index, nets[i].connections[j]->index, weight);
                    cs_entry (C_triplet, dummy_node_index, dummy_node_index, weight);
                }

                else {
                    //for the Degree matrix
                    cs_entry (C_triplet, dummy_node_index, dummy_node_index, weight);

                    //for the Fixed pin vectors
                    Dx[dummy_node_index] += weight * nets[i].connections[j]->centerX;
                    Dy[dummy_node_index] += weight * nets[i].connections[j]->centerY;
                }
            }
            dummy_node_index++;
            continue;
        }
        #endif

        //j and k run through the i-th nets connections
        for(j = 0; j < nets[i].connectionsCtr-1; j++){

            for (k = j+1; k < nets[i].connectionsCtr; k++) {

                if (!nets[i].connections[j]->terminal && !nets[i].connections[k]->terminal) {

                    //for the Adjacency matrix
                    cs_entry (C_triplet, nets[i].connections[j]->index, nets[i].connections[k]->index, -weight);
                    cs_entry (C_triplet, nets[i].connections[k]->index, nets[i].connections[j]->index, -weight);

                    //for the Degree matrix
                    cs_entry (C_triplet, nets[i].connections[j]->index, nets[i].connections[j]->index, weight);
                    cs_entry (C_triplet, nets[i].connections[k]->index, nets[i].connections[k]->index, weight);
                }

                else if (!nets[i].connections[j]->terminal && nets[i].connections[k]->terminal) {

                    //for the Degree matrix
                    cs_entry (C_triplet, nets[i].connections[j]->index, nets[i].connections[j]->index, weight);

                    //for the Fixed pin vectors
                    Dx[nets[i].connections[j]->index] += weight * nets[i].connections[k]->centerX;
                    Dy[nets[i].connections[j]->index] += weight * nets[i].connections[k]->centerY;
                }

                else if (nets[i].connections[j]->terminal && !nets[i].connections[k]->terminal) {

                    //for the Degree matrix
                    cs_entry (C_triplet, nets[i].connections[k]->index, nets[i].connections[k]->index, weight);

                    //for the Fixed pin vectors
                    Dx[nets[i].connections[k]->index] += weight * nets[i].connections[j]->centerX;
                    Dy[nets[i].connections[k]->index] += weight * nets[i].connections[j]->centerY;
                }
            }
        }
    }
    
}

void init_tables_for_1st_iteration(unsigned int C_n, unsigned int C_nz)
{
    int i = 0;

    //allocate memory for the fixed pin vectors and the solution vectors
    Dx = (double*) malloc (C_n * sizeof(double));
    NULL_CHECK(Dx);
    Dy = (double*) malloc (C_n * sizeof(double));
    NULL_CHECK(Dy);
    x = (double*) malloc (C_n * sizeof(double));
    NULL_CHECK(x);
    y = (double*) malloc (C_n * sizeof(double));
    NULL_CHECK(y);




    //initialize the fixed pin vectors
    for (i = 0; i < C_n; i++) {
        Dx[i] = Dy[i] = 0.0;
        x[i] = coreAreaWidth / 2;
        y[i] = coreAreaHeight / 2; 
    }

    //allocate memory for the Laplacian matrix
    C_triplet = cs_spalloc (C_n, C_n, C_nz, 1, 1);
    NULL_CHECK(C_triplet);

    //calculate the Laplacian matrix
    #if (PRINTING==FULL)
    printf("\n"COLOR_BLUE"Step 0: "COLOR_RESET"Creating Laplacian matrix..."); fflush(stdout);
    #elif (PRINTING==COMPACT)
    printf("\rSolving \t[ 0%%]"); fflush(stdout);
    #endif
    
    fill_laplacian_matrix(movablesCtr);
    #if (PRINTING==FULL)
    printf("\b\b\b of dimention %d\n", C_n);
    #endif
}

int solve_without_using_z(unsigned int C_n, unsigned int C_nz, int *total_solver_iters, double option_itol)
{
    int i , j = 0;
    int iters = 0;
    //update the size of matrix A
    unsigned int A_n = partitionsCtr; //pow(2, partitioning_step);

    //update the size of matrix CA
    CA_n = C_n + A_n;

    //remove all the elements of matrix A from the matrix CA
    C_triplet->nz = C_nz;

    //resize the CA matrix
    if (!cs_resize (C_triplet, CA_n, CA_n, CA_nz)) {
        perror(NULL);
        return -1;
    }

    //reallocate memory for the fixed pin vectors
    Dx = (double*) realloc (Dx, CA_n * sizeof(double));
    NULL_CHECK(Dx);
    Dy = (double*) realloc (Dy, CA_n * sizeof(double));
    NULL_CHECK(Dy);

    //reallocate memory for the solution vectors
    x = (double*) realloc (x, CA_n * sizeof(double));
    NULL_CHECK(x);
    y = (double*) realloc (y, CA_n * sizeof(double));
    NULL_CHECK(y);

    //initialize the solution vectors
    for(i=CA_n_prev; i<CA_n; i++) x[i] = y[i] = 0.0;    

    //create the rest of the CAx=Dx linear system
    cur_p = partitions;
    for (i=0; i < partitionsCtr && cur_p!=NULL; i++, cur_p = cur_p->next) {

        #ifdef USE_AREA_CONSTRAINT
        double partition_area = cur_p->area;
        #else
        double weight = 0.0;
        if (cur_p->contentsCtr == 0) {
            weight = 0.0;
        }
        else {
            weight = 1.0 / cur_p->contentsCtr;
        }
        #endif

        //fill the A matrix with weights corresponding to the new partitions
        for (j = 0; j < cur_p->contentsCtr; j++) {
            #ifdef USE_AREA_CONSTRAINT
            cs_entry (C_triplet, cur_p->contents[j]->index, i+C_n, (cur_p->contents[j]->sizeX * cur_p->contents[j]->sizeY)/partition_area);
            cs_entry (C_triplet, i+C_n, cur_p->contents[j]->index, (cur_p->contents[j]->sizeX * cur_p->contents[j]->sizeY)/partition_area);
            #else
            cs_entry (C_triplet, cur_p->contents[j]->index, i+C_n, weight);
            cs_entry (C_triplet, i+C_n, cur_p->contents[j]->index, weight);
            #endif
        }

        //fill the fixed pin vectors with the center-locations
        Dx[i+C_n] = cur_p->centerX;
        Dy[i+C_n] = cur_p->centerY;
    }

    //convert the CA matrix from triplet to compressed column format
    C = cs_compress (C_triplet);                            
    cs_dupl (C);

    #if (PRINTING==FULL)
    printf("\b\b\b   \n");
    #endif

    //solve
    #pragma omp parallel num_threads(2)
    {
        int tid = omp_get_thread_num ();
        if (tid==0) {
            iters = cs_bicgsymsol (C, x, Dx, CA_n, option_itol);
            #if (PRINTING==FULL)
            printf("\tSolving for x in %d iterations\n", iters);
            #endif
            total_solver_iters += iters;
        }
        else {
            iters = cs_bicgsymsol (C, y, Dy, CA_n, option_itol);
            #if (PRINTING==FULL)
            printf("\tSolving for y in %d iterations\n", iters);
            #endif
            total_solver_iters += iters;
        }
    }
    return 0;
}

void print_cells_list()
{
    struct cell_t** curr_cell = movable_cells_list;
    int i = 0;
    for(; i < movablesCtr; i++ ){
        printf("cell %s %lf %lf\n", curr_cell[i]->name, curr_cell[i]->centerX, curr_cell[i]->centerY);
    }
}

void print_locations()
{
    struct cell_t *cur_cell = cells_list;
    while (cur_cell!=NULL) { 
        printf("cell %s %lf %lf\n", cur_cell->name, cur_cell->centerX, cur_cell->centerY);
        cur_cell = cur_cell->next;
    }

}

void create_movables_cells_lists()
{
    movable_cells_list = (struct cell_t**) malloc (movablesCtr * sizeof(struct cell_t*));
    struct cell_t *cur_cell = cells_list;
    while (cur_cell!=NULL) { 
        if (cur_cell->tpf==0) {
            movable_cells_list[cur_cell->index] = cur_cell;
        }
        cur_cell = cur_cell->next;
    }
}

void update_density_map(int m, int n, float ***density_map)
{
	
    int i=0, j=0;
    for (i=0;i<m;i++) {
        for (j=0;j<n;j++) {
            (*density_map)[i][j]=0;
        }
    }
    struct cell_t *cur_cell = cells_list;
    while (cur_cell != NULL) { 
        if (!cur_cell->terminal) {
            int x_pos_min = (cur_cell->centerX - cur_cell->sizeX/2)/grid_min;
            int x_pos_max = (cur_cell->centerX + cur_cell->sizeX/2)/grid_min;
            int y_pos_min = (cur_cell->centerY - cur_cell->sizeY/2)/grid_min;
            int y_pos_max = (cur_cell->centerY + cur_cell->sizeY/2)/grid_min;
            if (y_pos_min <0 ) y_pos_min =0;
            if (y_pos_max >n ) y_pos_max =n-1;
            if (x_pos_min <0) x_pos_min =0;
            if (x_pos_max >m ) x_pos_max =m-1;

            int step_y;
            int step_x;
            for (step_y = y_pos_min; step_y < y_pos_max; step_y += 1) {
                for (step_x = x_pos_min; step_x < x_pos_max; step_x += 1) {
                    (*density_map)[step_y][step_x] += 1;
                }
            }
        }
        cur_cell = cur_cell->next;
    }
    
}

int upper_lower_sum(int i, int w, float *density_map_row)
{
    if (i-1 < 0) {
        return density_map_row[i] + density_map_row[i+1];
    }
    else if (i+1 >= w) {
        return density_map_row[i-1] + density_map_row[i];
    }
    else {
        return density_map_row[i-1] + density_map_row[i] + density_map_row[i+1];
    }
}

int row_sum(int i, int w, float *density_map_row)
{
    if (i-1 < 0) {
        return density_map_row[i+1] + density_map_row[i];
    }
    else if (i+1 >= w) {
        return density_map_row[i-1] + density_map_row[i];
    }
    else {
        return density_map_row[i-1] + density_map_row[i+1] +density_map_row[i];
    }
}


void box_blur(int m, int n, float ***blur_map, float **density_map)
{
    int i=0, j=0;

    for (i = 0; i < m; i++) {
        for (j = 0; j < n; j++) {
            float total = 0;
            if (i-1 < 0) {
                total = row_sum(j, m, density_map[i]) + upper_lower_sum(j, m, density_map[i+1]);
            }
            else if(i+1 >= m) {
                total = row_sum(j, m, density_map[i]) + upper_lower_sum(j, m, density_map[i-1]);
            }
            else {
                total = row_sum(j, m, density_map[i]) + upper_lower_sum(j, m, density_map[i+1]) + upper_lower_sum(j, m, density_map[i-1]);
            }
            (*blur_map)[i][j] = total/9.0;
        }
    }

}
/*
void calculate_overlap(int m, int n, float (*density_map)[m][n])
{
    int overlap_area=0;

    for (i=0;i<m;i++) {
        for (j=0;j<n;j++) {
            if (density_map[i][j]>1) {
                overlap_area=(int)(*density_map[i][j])*grid_min*grid_min; 
            }
        }
    }    
}
*/
/* gordian's main routine */

void find_boundary_cells(struct net_t net, struct cell_t **left, struct cell_t **right, struct cell_t **upper, struct cell_t **lower)
{
    int i=0;
    *left=net.connections[0];
    *right=net.connections[0];
    *upper=net.connections[0];
    *lower=net.connections[0];

    for (i=0;i<net.connectionsCtr;i++) {
        if ((*left)->centerX > net.connections[i]->centerX) {
            *left=net.connections[i];
        }
        if ((*right)->centerX < net.connections[i]->centerX) {
            *right=net.connections[i];
        }
        if((*lower)->centerY > net.connections[i]->centerY) {
            *lower=net.connections[i];
        }
        if((*upper)->centerY < net.connections[i]->centerY) {
            *upper=net.connections[i];
        }
    }
}

void b2b(cs **cx_matrix, cs **cy_matrix) 
{
    int i=0;
    struct cell_t *left, *right, *upper, *lower;

    for (i = 0; i < netsCtr; i++) {
        int k=0, j=0;
        //printf("net name:\n");
        //for (k=0;k<nets[i].connectionsCtr;k++) {
        //    printf("cell name %s, centerX, Y: %lf, %lf\n",nets[i].connections[k]->name, nets[i].connections[k]->centerX, nets[i].connections[k]->centerY);
        //}
        //printf("\n");
        double x_distance = 0;
        double y_distance = 0;
        double weight = 2.0 / (nets[i].connectionsCtr -1);    //the clique's edge weight

        find_boundary_cells(nets[i], &left, &right, &upper, &lower);
        //printf("boundary cells: %s %d, %s %d, %s %d, %s %d\n", left->name, left->index, right->name, right->index, upper->name, upper->index, lower->name, lower->index);
        
                    //x_distance= fabs(left->centerX - right->centerX);
                    //y_distance= fabs(upper->centerY - lower->centerY);
        for(j = 0; j < nets[i].connectionsCtr-1; j++){
            for (k = j+1; k < nets[i].connectionsCtr; k++) {
                if (!strcmp(nets[i].connections[j]->name, left->name) ||!strcmp(nets[i].connections[j]->name, right->name) ||
                    !strcmp(nets[i].connections[k]->name, left->name) ||!strcmp(nets[i].connections[k]->name, right->name)) { 
                    x_distance= fabs(nets[i].connections[j]->centerX - nets[i].connections[k]->centerX);
                    if (x_distance <0.00001)
                        x_distance = 0.00001;
                    if (!nets[i].connections[j]->terminal && !nets[i].connections[k]->terminal) {
                        cs_entry (*cx_matrix, nets[i].connections[j]->index, nets[i].connections[k]->index, -weight/x_distance);
                        cs_entry (*cx_matrix, nets[i].connections[k]->index, nets[i].connections[j]->index, -weight/x_distance);
                        cs_entry (*cx_matrix, nets[i].connections[k]->index, nets[i].connections[k]->index, weight/x_distance);
                        cs_entry (*cx_matrix, nets[i].connections[j]->index, nets[i].connections[j]->index, weight/x_distance);
                    }
                    else if (nets[i].connections[j]->terminal && !nets[i].connections[k]->terminal) {
                        //printf("non terminal %s terminal %s\n", nets[i].connections[k]->name, nets[i].connections[j]->name);
                        cs_entry (*cx_matrix, nets[i].connections[k]->index, nets[i].connections[k]->index, weight/x_distance);
                    }
                    else if (!nets[i].connections[j]->terminal && nets[i].connections[k]->terminal) {
                        //printf("non terminal %s terminal %s\n", nets[i].connections[j]->name, nets[i].connections[k]->name);
                        cs_entry (*cx_matrix, nets[i].connections[k]->index, nets[i].connections[k]->index, weight/x_distance);
                    }
                }

                if (!strcmp(nets[i].connections[j]->name, upper->name) ||!strcmp(nets[i].connections[j]->name, nets[i].connections[k]->name) ||
                    !strcmp(nets[i].connections[k]->name, upper->name) ||!strcmp(nets[i].connections[k]->name, nets[i].connections[k]->name)) { 
                    y_distance= fabs(nets[i].connections[j]->centerY - nets[i].connections[k]->centerY);
                    if (y_distance <0.00001)
                        y_distance =0.00001;
                    if (!nets[i].connections[j]->terminal && !nets[i].connections[k]->terminal) {
                        cs_entry (*cy_matrix, nets[i].connections[j]->index, nets[i].connections[k]->index, -weight/y_distance);
                        cs_entry (*cy_matrix, nets[i].connections[k]->index, nets[i].connections[j]->index, -weight/y_distance);
                        cs_entry (*cy_matrix, nets[i].connections[k]->index, nets[i].connections[k]->index, weight/y_distance);
                        cs_entry (*cy_matrix, nets[i].connections[j]->index, nets[i].connections[j]->index, weight/y_distance);
                    }
                    else if (!nets[i].connections[j]->terminal && nets[i].connections[k]->terminal) {
                        cs_entry (*cy_matrix, nets[i].connections[j]->index, nets[i].connections[j]->index, weight/y_distance);
                    }

                    else if (nets[i].connections[j]->terminal && !nets[i].connections[k]->terminal) {
                        cs_entry (*cy_matrix, nets[i].connections[k]->index, nets[i].connections[k]->index, weight/y_distance);
                    }
                }
            }
        }
    }
}

int main (int argc, char *argv[]){

    //gordian variables
    int i = 0;
    int iters;            //keeps track of the number of iterations of the iterative solvers
    int steps_estimate;        //an estimation of the algorithms partitioning steps
    int partitioning_step = 0;        //the number of the current partitioning step
    int multiple_cuts = 1;        //the number of consecutive cuts performed during a partitioning step before solving
    unsigned int C_n;        //the dimention of Laplacian matrix
    unsigned int C_nz;        //the count of the non-zero elements in Laplacian matrix
    struct cell_t *cur_cell;    //used to scan the cells_list
    int iterations = 0;

    struct timeval t1, t2;

    //for statistics printing
    int total_solver_iters=0;
    double total_wire_length=0.0;

    //option variables
    char *option_pl=NULL;        //the name of the specified output .pl file
    char *option_plot=NULL;        //is either no, first, last, final or all 
    double option_itol=0.001;    //the tollerance of the iterative solver
    int cells_k = 4;        //the iterative process stops when every partition has k number of cells at most
    short option_legalize=0;
    short option_pwl=0;
    short option_wlcalc=0;

    gettimeofday(&t1, NULL);

    //check program inputs
    if (option_parser (argc, argv, &option_plot, option_pl, &option_itol, &cells_k, 
                       &option_legalize, &option_pwl, &option_wlcalc, &multiple_cuts, &iterations) == -1 ) {
        return -1;
    }
 
    //parse the necessary input files
    parse_bookshelf (argv[1], argv[2]);

    //if wlcalc option is used, calculate the hpwl without running the placer
    if(option_wlcalc) {
        #if (PRINTING==FULL)
        printf("\r%e", hpwl());
        //printf("\rHalf perimeter wire length:"COLOR_GREEN" %.3lfe6"COLOR_RESET"\n", hpwl()/1000000);
        #endif

        return 0;
    }

    #if (PRINTING==FULL)
    printf(COLOR_GREEN" %d "COLOR_RESET"movable cells and"COLOR_GREEN" %d "COLOR_RESET"fixed terminals\n", movablesCtr, cellsCtr-movablesCtr);
    printf("Core area dimentions: %.0lf x %.0lf\n\n", coreAreaWidth, coreAreaHeight);
    #endif

    //discard the .aux from the auxiliary filename
    for(i=0; i<strlen(argv[2]); i++) if(argv[2][i]=='.') {
        argv[2][i]='\0';        
    }
    
    create_movables_cells_lists();
    //calculate the number of non-zeros in the Laplacian matrix
    C_nz = calculate_laplacian_non_zero_elements(); 
    C_n = movablesCtr;
    printf("Non zero elements count: %u\n", C_nz);
    #ifdef USE_STAR_NETS
    C_n = movablesCtr + total_dummy_nodes;
    #endif

    #if (PRINTING==FULL)
    printf("Iterative solvers will use tolerance: %.2e\n\n", option_itol);
    printf("Will cut %d time(s) for every partitioning step\n\n", multiple_cuts);
    printf("Nets with degree greater than %d will be converted to star-nets\n", max_net_degree);
    #ifdef USE_STAR_NETS
    printf("Total real  nodes: %7d\n", movablesCtr);
    printf("Total dummy nodes: %7d\n\n", total_dummy_nodes); fflush(stdout);
    #endif
    #endif

    steps_estimate = ilog2(C_n/cells_k);

    #if (PRINTING==FULL)
    printf("Estimation of required memory: %d MB\n", (((C_nz*(4+4+8+4+8)) + (C_n*(4+8+8+8+8))) / 1048576));
    printf("Estimation of required partitioning steps: %d \n", steps_estimate);
    #endif

    init_tables_for_1st_iteration(C_n, C_nz);

    //convert the Laplacian matrix from triplet to compressed column format
    C = cs_compress (C_triplet);

    #if (PRINTING==FULL)
    printf("\tTotal algorithm memory: %d MB\n", (int)(mallinfo().hblkhd / 1048576));
    printf("\tTotal allocated memory: %d MB\n", (int)((mallinfo().arena + mallinfo().hblkhd) / 1048576));
    #endif

    cs_dupl (C);
    
    //cs_print (C,0);
/*    printf ("Dx= \n");
    for (i =0; i <C_n; i++) {
        printf("%lf \n", Dx[i]);
    }
    printf ("\nDy= \n");
    for (i =0; i <C_n; i++) {
       printf("%lf \n", Dy[i]);
    }
    printf ("\n");
*/   //solve
    #pragma omp parallel num_threads(2)
    {
        int tid = omp_get_thread_num ();
        if (tid==0) {
            iters = cs_cgsol (C, x, Dx, C_n, option_itol);
            #if (PRINTING==FULL)
            printf("\tSolving for x in %d iterations\n", iters);
            #endif
            total_solver_iters += iters;
        }
        else {
            iters = cs_cgsol (C, y, Dy, C_n, option_itol);
            #if (PRINTING==FULL)
            printf("\tSolving for y in %d iterations\n", iters);
            #endif
            total_solver_iters += iters;
        }
    }

    //store the solution coordinates to the cells in the cells_list
    cur_cell = cells_list;
    while (cur_cell!=NULL) {
        if (!cur_cell->terminal) {
            cur_cell->centerX = x[cur_cell->index];
            cur_cell->centerY = y[cur_cell->index];
        }
        cur_cell = cur_cell->next;
    }

    if (!strcmp(option_plot, "first") || !strcmp(option_plot, "all")) {
        char tmp[32];        
        sprintf(tmp, "%s_step00", argv[2]);
        plot_area (tmp, 0);
    }

    int m = (int)(coreAreaHeight/grid_min);
    int n = (int)(coreAreaWidth/grid_min);

    printf("grid_min= %lf, m=%d, n=%d\n", grid_min, m,n);

    //print_cells_list();
    //


    float **density_map = malloc (m*sizeof(float*));
    for (i=0;i<m; i++){
        density_map[i]=malloc(n*sizeof(float));
    }

    float **blur_map= malloc(m*sizeof(float*));
    for (i=0;i<m; i++){
        blur_map[i]=malloc(n*sizeof(float));
    }

    int k=0;
    struct cell_t **movable_cell = movable_cells_list;
/*    double previous_movement = INFINITY;
    double *Dx_init = (double*) malloc (C_n * sizeof(double));
    NULL_CHECK(Dx_init);
    double *Dy_init = (double*) malloc (C_n * sizeof(double));
    NULL_CHECK(Dy_init);
*/
/*    printf("initial values\n");
    printf("x=[");
    for(i=0; i<movablesCtr-1; i++) {
        printf("%lf; ",movable_cell[i]->centerX);
    }
    printf("%lf ]", movable_cell[i]->centerX);
    printf("\ny=[");
    for(i=0; i<movablesCtr-1; i++) {
        printf("%lf; ",movable_cell[i]->centerY);
    }
    printf("%lf ]\n", movable_cell[i]->centerY);
*/

    cs *Cxb2b_triplet, *Cyb2b_triplet;
        cs *Cxb2b, *Cyb2b;
        Cxb2b_triplet=cs_spalloc(C_n, C_n, C_nz, 1, 1);
        Cyb2b_triplet=cs_spalloc(C_n, C_n, C_nz, 1, 1);

        memset(Dx, 0, sizeof(Dx));
        memset(Dy, 0, sizeof(Dy));


        b2b(&Cxb2b_triplet, &Cyb2b_triplet);
	
    while (k<iterations) {
		
/*        cs *Cxb2b_triplet, *Cyb2b_triplet;
        cs *Cxb2b, *Cyb2b;
        Cxb2b_triplet=cs_spalloc(C_n, C_n, C_nz, 1, 1);
        Cyb2b_triplet=cs_spalloc(C_n, C_n, C_nz, 1, 1);

        memset(Dx, 0, sizeof(Dx));
        memset(Dy, 0, sizeof(Dy));


        b2b(&Cxb2b_triplet, &Cyb2b_triplet);
        printf("Cx_triplet: \n");
        cs_print(Cxb2b_triplet, 0); 
        printf("\nCy_triplet: \n");
        cs_print(Cyb2b_triplet, 0); 
*/        Cxb2b=cs_compress(Cxb2b_triplet);
        Cyb2b=cs_compress(Cyb2b_triplet);
        cs_dupl(Cxb2b);
        cs_dupl(Cyb2b);
        
/*        printf("Cx: \n");
        cs_print(Cxb2b, 0); 
        printf("\nCy: \n");
        cs_print(Cyb2b, 0); 
*/        movable_cell = movable_cells_list;
        
        printf ("%d loop\n", k+1);
        
        update_density_map(m, n, &density_map); 
        
        //printf("skata1\n");
        //blurring
        int j=0;
        for (i=0; i<m; i++) {
            for (j=0; j<n; j++) {
                blur_map[i][j]=0;
            }
        }
        //printf("skata2\n");
        
        box_blur(m, n, &blur_map, density_map);
        //printf("skata3\n");
        box_blur(m, n, &blur_map, blur_map);
        //printf("skata4\n");
        box_blur(m, n, &blur_map, blur_map);
        //printf("skata5\n");
/*        printf("density_map=[");
        for (i=0; i<m; i++) {
            for (j=0; j<n-1; j++) {
                printf("%lf, ",density_map[i][j]);
            }
            printf("%lf ;\n",density_map[i][j+1]);
        }
 
        printf("blur_map=[");
        for (i=0; i<m; i++) {
            for (j=0; j<n-1; j++) {
                printf("%lf, ",blur_map[i][j]);
            }
            printf("%lf ;\n",blur_map[i][j+1]);
        }
*/
        cs *T;
        T=cs_spalloc(C_n, C_n, C_n, 1, 1);
        
        for(i=0; i<movablesCtr; i++) {
        
            int bin_position_min_x = (int) floor(movable_cell[i]->centerX/grid_min);
            int bin_position_max_x = (int) ceil(movable_cell[i]->centerX/grid_min);
            int bin_position_min_y = (int) floor(movable_cell[i]->centerX/grid_min);
            int bin_position_max_y = (int) ceil(movable_cell[i]->centerX/grid_min);
            if (bin_position_min_x <0 ) bin_position_min_x =0;
            if (bin_position_min_x >m ) bin_position_min_x =m-1;
            if (bin_position_max_x <0 ) bin_position_max_x =0;
            if (bin_position_max_x >m ) bin_position_max_x =m-1;
            if (bin_position_min_y <0) bin_position_min_y =0;
            if (bin_position_min_y >n) bin_position_min_y= n-1;
            if (bin_position_max_y >n ) bin_position_max_y =n-1;
            if (bin_position_max_y <0 ) bin_position_max_y =0;

            //printf("%d %d %d %d\n", bin_position_min_x, bin_position_max_x, bin_position_min_y, bin_position_max_y);
        
            Dx[movable_cell[i]->index]= -(movable_cell[i]->area/avg_area)*((blur_map[bin_position_max_x][bin_position_min_y] - blur_map[bin_position_min_x][bin_position_min_y])/grid_min)*(1.0/movablesCtr); 
            Dy[movable_cell[i]->index]= -(movable_cell[i]->area/avg_area)*((blur_map[bin_position_min_x][bin_position_max_y] - blur_map[bin_position_min_x][bin_position_min_y])/grid_min)*(1.0/movablesCtr); 
            cs_entry (T, movable_cell[i]->index, movable_cell[i]->index, (movable_cell[i]->area/avg_area)* (1.0/movablesCtr));
            //printf("skata6\n");
        }

        cs *Eye, *Cx_new, *Cy_new;
        Eye=cs_compress(T);
        cs_spfree(T);
          
        Cx_new=cs_add(Cxb2b, Eye, 1, 1);
        Cy_new=cs_add(Cyb2b, Eye, 1, 1);   
/*
        printf("Cx: \n");
        cs_print(Cx_new, 0); 
        printf("\nCy: \n");
        cs_print(Cy_new, 0); 
 
        printf("\nDx matrix \n");
        for (i=0; i<movablesCtr; i++) { 
            printf(" %lf \n",Dx[i]);
        }
        printf("\nDy matrix \n");
        for (i=0; i<movablesCtr; i++) { 
            printf(" %lf \n",Dy[i]);
        }
*/
        memset(x,0, sizeof(x));
        memset(y,0, sizeof(y));
#pragma omp parallel num_threads(2)
        {
            int tid = omp_get_thread_num ();
            if (tid==0) {
                iters = cs_cgsol (Cx_new, x, Dx, C_n, option_itol);
                #if (PRINTING==FULL)
                printf("\tSolving for x in %d iterations\n", iters);
                #endif
                total_solver_iters += iters;
            }
            else {
                iters = cs_cgsol (Cy_new, y, Dy, C_n, option_itol);
                #if (PRINTING==FULL)
                printf("\tSolving for y in %d iterations\n", iters);
                #endif
                total_solver_iters += iters;
            }
        }
/*         printf("\nx matrix \n");
    for (i=0; i<movablesCtr; i++) { 
        printf(" %lf \n",x[i]);
    }
    printf("\ny matrix \n");
    for (i=0; i<movablesCtr; i++) { 
        printf(" %lf \n",y[i]);
    }
*/           
        cur_cell = cells_list;
        while (cur_cell!=NULL) {
            if (!cur_cell->terminal) {
                cur_cell->centerX = cur_cell->centerX + x[cur_cell->index];
                cur_cell->centerY = cur_cell->centerY + y[cur_cell->index];
            }
            cur_cell = cur_cell->next;
        }

        printf("\n after blurring \n");
       // print_cells_list();

       cs_spfree(Eye);
        cs_spfree(Cx_new);
        cs_spfree(Cy_new);

        if (!strcmp(option_plot, "all")) {
            char tmp[32];        
            sprintf(tmp, "%s_step0%d", argv[2],k+1);
            plot_area (tmp, 0);
        }

        k++;
    }            
 
    #if (PRINTING==FULL)
    if (option_pwl) {
        total_wire_length = hpwl()/1000000;
        printf("\tHalf perimeter wire length:"COLOR_GREEN" %.3lfe6"COLOR_RESET"\n", total_wire_length);
    }
    #endif
    
    #if (PRINTING==COMPACT)
    printf("\rSolving \t[%2d%%]", (int)(100/(steps_estimate+1))); fflush(stdout);
    #endif
//**** Here is the iteration******//
//    
    //tables for poisson equation
/*    double *b = (double*) malloc ((m-2)*(n-2)*sizeof(double));
    double *u = (double*) malloc ((m-2)*(n-2)*sizeof(double));
    
    memset(b, 0, (m-2)*(n-2)*sizeof(b));
    memset(u, 0,(m-2)*(n-2)*sizeof(u));
    int j = 0;

    for (i = 0; i < n-3; i++){
        for(j = 0; j < m-3; j++){
            b[(i*(n-2))+j] = (-((float)(m-2)*(n-2))/(float)(m*n)) * density_map[i+1][j+1];
        }
    }

    printf("b vector: (max size=%d)\n", (n-2)*(m-2));
    printf("Dx = %lf", (-((float)(m-2)*(n-2))/(float)(m*n)));
    for (i = 0; i < n-2; i++) {
        for(j = 0; j < m-2; j++) {
            printf("at pos:%d %lf\n", (i*(n-2))+j, b[(i*(n-2))+j]);
        }
    }
*/
    #if (PRINTING==COMPACT)
    printf("\rSolving \t[OK] \n"); fflush(stdout);
    #endif
    

    if (hpwl()>1000000) {
        total_wire_length = hpwl()/1000000;
    }
    else
        total_wire_length = hpwl();

    //plot the solution of the last partitioning step
    if(!strcmp(option_plot, "last")){
        char tmp[32];
        sprintf(tmp, "%s_step%02d", argv[2], partitioning_step-1);
        plot_area (tmp, 0);
        printf("\n");
    }

    //perform legalization
    if (option_legalize) {

        printf("legalizing...\n");
        legalize (movable_cells_list);
        //legalizer_abacus_alike(coreAreaHeight, coreAreaWidth, partitions->contents);
        //tetris(coreAreaHeight, coreAreaWidth, movable_cells_list);
        
        if (hpwl()>1000000) {
            total_wire_length = hpwl()/1000000;
        }
        else
            total_wire_length = hpwl();

        #if (PRINTING==FULL)
        printf("\tHalf perimeter wire length:"COLOR_GREEN" %.3lfe6"COLOR_RESET"\n\n", total_wire_length);
        #endif    

        //plot the final solution
        if(!strcmp(option_plot, "final") || !strcmp(option_plot, "all")){
            char tmp[32];
            sprintf(tmp, "%s_final", argv[2]);
            plot_area (tmp, 0);
            printf("\n");
        }
    }
    #if (PRINTING==FULL)
    else {
        if(!strcmp(option_plot, "final")) printf("\tLegalization is disabled so no plotting will occur\n\n");
    }
    #endif

    //write the result file
    if(option_pl!=NULL) write_pl_file (option_pl);

    //free all the allocated memory
    cs_spfree (C_triplet);

    free (x);
    free (y);
    free (Dx);
    free (Dy);
        //cs_spfree(Cxb2b);
        //cs_spfree(Cyb2b);
 
    #if (PRINTING==FULL)
    gettimeofday(&t2, NULL);
    printf("\tTotal execution time in ms:"COLOR_GREEN" %ld"COLOR_RESET"\n", 1000*(t2.tv_sec-t1.tv_sec) + (t2.tv_usec-t1.tv_usec)/1000 );
    #endif
    return 0;
}
