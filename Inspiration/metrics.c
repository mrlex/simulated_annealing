#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "gordian.h"

#define BINS_DIM	(40)


/* calculates the total half-perimeter wire-length */
double hpwl (){

	int i, j;
	double sum = 0.0;

	for(i=0; i<netsCtr; i++){
		double minX=coreAreaWidth, minY=coreAreaHeight, maxX=0.0, maxY=0.0;
 
		for(j=0; j<nets[i].connectionsCtr; j++){
			if(nets[i].connections[j]->centerX < minX) minX = nets[i].connections[j]->centerX;
			if(nets[i].connections[j]->centerY < minY) minY = nets[i].connections[j]->centerY;
			if(nets[i].connections[j]->centerX > maxX) maxX = nets[i].connections[j]->centerX;
			if(nets[i].connections[j]->centerY > maxY) maxY = nets[i].connections[j]->centerY;
			}

			sum += ((maxX-minX) + (maxY-minY));
		}
	return sum;
	}


/* splits the core area in BINS_DIM * BINS_DIM bins, counts the cells inside every bin and returns the variance */
double distribution_variance (){

	int i, j;
	int bins[BINS_DIM+1][BINS_DIM+1];
	double mean;
	double variance;
	double bin_width;
	double bin_height;
	struct cell_t *cur_cell;

	mean = 0.0;
	variance = 0.0;
	bin_width = coreAreaWidth / BINS_DIM;
	bin_height = coreAreaHeight / BINS_DIM;
	
	for(i=0; i<BINS_DIM; i++)
		for(j=0; j<BINS_DIM; j++)
			bins[i][j] = 0;


	cur_cell = cells_list;
	while(cur_cell!=NULL){
		if(!cur_cell->terminal){
			//bins[(int) MY_MIN(MY_MAX((int)(cur_cell->centerX / bin_width), 0), coreAreaWidth)][(int) MY_MIN(MY_MAX((int)(cur_cell->centerY / bin_height), 0), coreAreaHeight)]++;
			bins[(int)(cur_cell->centerX/bin_width)][(int)(cur_cell->centerY/bin_height)]++;
			}
		cur_cell = cur_cell->next;
		}

	for(i=0; i<BINS_DIM; i++)
		for(j=0; j<BINS_DIM; j++)
			mean += bins[i][j];

	mean /= (BINS_DIM * BINS_DIM);

	for(i=0; i<BINS_DIM; i++)
		for(j=0; j<BINS_DIM; j++)
			variance += pow (bins[i][j] - mean, 2.0);

	variance /= (BINS_DIM * BINS_DIM);

	printf("\n\tVariance: %.1lf\n", variance);
	printf("\t    Mean: %.1lf\n", mean);

	return (variance);
	}

