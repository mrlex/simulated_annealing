#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "gordian.h"

#define PADDING_PERCENTAGE	(0.05)


//1: red
//2: green
//3: blue
//4: purple
//5: light blue
//6: yellow
//7: black
//8: blue
//9: orange
//10: dark green
//12: brown


/* plots the whole core area to a .png image file */
void plot_area (char *filename, short plot_partitions){

	char cmd[1024];
	double pin_dimX;
	double pin_dimY;
	struct cell_t *cur_cell;
	struct partition_t *p = partitions;
	gnuplot_ctrl *h;	//a plotting handler used by gnuplot

	pin_dimX = 2 * coreAreaWidth / (cellsCtr-movablesCtr); 
	pin_dimY = 2 * coreAreaHeight / (cellsCtr-movablesCtr);
	#if (PRINTING==COMPACT || PRINTING==FULL)
	int last_print=-1;
	int print_ctr=0;
	#endif

	h = gnuplot_init();
	gnuplot_resetplot(h);

	#if (PRINTING==FULL)
	printf("\rPlotting %s  0%%", filename); fflush(stdout);
	//printf("\tPlotting..."); fflush(stdout);
	#elif (PRINTING==COMPACT)
	printf("\rPlotting %s\t[ 0%%]", filename); fflush(stdout);
	#endif
	sprintf(cmd, "set style arrow 7 nohead ls 1");
	//plot the cells
	cur_cell = cells_list;
	while(cur_cell!=NULL){
			
		//print filled rectangle for a cell
		//if(!cur_cell->terminal) sprintf(cmd, "set object rect from %lf,%lf to %lf,%lf fc rgb 'green' fs solid 1 border -1;", cur_cell->centerX - cur_cell->sizeX / 2, cur_cell->centerY - cur_cell->sizeY / 2, cur_cell->centerX + cur_cell->sizeX / 2, cur_cell->centerY + cur_cell->sizeY / 2);

		//print empty rectangle for a cell
		if(cur_cell->tpf==0) sprintf(cmd, "set object rect from %lf,%lf to %lf,%lf fc rgb 'blue' fs solid;", cur_cell->centerX - cur_cell->sizeX / 2, cur_cell->centerY - cur_cell->sizeY / 2, cur_cell->centerX + cur_cell->sizeX / 2, cur_cell->centerY + cur_cell->sizeY / 2);
		//print filled rectangle for a pin
		else if(cur_cell->tpf==0x4)sprintf(cmd, "set object rect from %lf,%lf to %lf,%lf fc rgb 'grey' fs solid;", cur_cell->centerX - pin_dimX / 2, cur_cell->centerY - pin_dimY / 2, cur_cell->centerX + pin_dimX / 2, cur_cell->centerY + pin_dimY / 2);
		//else if(cur_cell->tpf==0x1)sprintf(cmd, "set object rect from %lf,%lf to %lf,%lf fc rgb 'green' fs solid;", cur_cell->centerX - pin_dimX / 2, cur_cell->centerY - pin_dimY / 2, cur_cell->centerX + pin_dimX / 2, cur_cell->centerY + pin_dimY / 2);

		gnuplot_cmd(h, cmd);
		cur_cell = cur_cell->next;

		//just for printing the percentage of the completed plotting proccess
		#if (PRINTING==FULL)
		if((int)(print_ctr*100/cellsCtr) > last_print){
			last_print = (int)(print_ctr*100/cellsCtr);
			printf("\b\b\b%2d%%", last_print); fflush(stdout);
			}
		print_ctr++;
		#elif (PRINTING==COMPACT)
		if((int)(print_ctr*100/cellsCtr) > last_print){
			last_print = (int)(print_ctr*100/cellsCtr);
			printf("\rPlotting\t[%2d%%]", last_print); fflush(stdout);
			}
		print_ctr++;	
		#endif
		}
    for(int i = 0; i < tupCtr; i++) {
        sprintf(cmd, "set arrow from %lf,%lf, 0 to %lf,%lf, nohead lc rgb 'red'",id_table[nets_table_row[i]]->centerX, id_table[nets_table_row[i]]->centerY, id_table[nets_table_col[i]]->centerX, id_table[nets_table_col[i]]->centerY); 
    }
	//plot the partitioning lines
	if (plot_partitions){
		while(p != NULL){
			sprintf(cmd, "set object rect from %lf,%lf to %lf,%lf lw 0.5 fillstyle empty border 3;", p->minX, p->minY, p->maxX, p->maxY);
			gnuplot_cmd(h, cmd);
			p = p->next;
			}
		}

	//plot the core area borders
	sprintf(cmd, "set object rect from 0,0 to %lf,%lf lw 2.0 fillstyle empty border 1;", coreAreaWidth, coreAreaHeight);
	gnuplot_cmd(h, cmd);

	//make the plot to a pop-up window
	//gnuplot_cmd(h, "set term wxt persist");
	//gnuplot_cmd(h, "set terminal wxt title 'GORDIAN Placement';");

	//make the plot to a png image file
	sprintf(cmd, "set output '%s';", filename);
	gnuplot_cmd(h, cmd);
	gnuplot_cmd(h,"set terminal png size 1600,1200;" );


	//make the plot visible
	gnuplot_cmd(h, "set key off;");		//disable the legend
	sprintf(cmd, "set xrange [%lf:%lf]; set yrange [%lf:%lf];", coreAreaWidth * (PADDING_PERCENTAGE) * (-1.0), coreAreaWidth * (PADDING_PERCENTAGE + 1), coreAreaHeight * (PADDING_PERCENTAGE) * (-1.0), coreAreaHeight * (PADDING_PERCENTAGE + 1));
	gnuplot_cmd(h, cmd);
	gnuplot_cmd(h, "plot -100000;");

	#if (PRINTING==FULL)
	printf("\b\b\b\b... \t[OK]\n");
	#elif (PRINTING==COMPACT)
	printf("\rPlotting\t[OK] "); fflush(stdout);
	#endif

	gnuplot_close(h);
	}


