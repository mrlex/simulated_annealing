double total_cost();
double total_cost_table();
double node_cost(int node, double n_centerX, double n_centerY);
void annealing (struct cell_t** cells_array, double hot, double range, int iter);
void output_check(struct cell_t** cells_array);
