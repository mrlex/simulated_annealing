#ifndef _GORDIAN_H_
#define _GORDIAN_H_

#include "parsers.h"

#include "../lib/cs.h"
#include "../lib/gnuplot_i.h"

#ifndef PRINTING
#error "Please define macro PRINTING and recompile"
#endif

#define EPS		(10e-6)

#define MY_MIN(A,B)	(((A) < (B)) ? (A) : (B))
#define MY_MAX(A,B)	(((A) > (B)) ? (A) : (B))

#define USE_STAR_NETS
//#define USE_AREA_CONSTRAINT


/* Function definitions for solvers.c */
int cs_cgsol (cs *A, double *x, double *b, int A_n, double tol);
int cs_bicgsol (cs *A, double *x, double *b, int A_n, double tol);
int cs_bicgsymsol (cs *A, double *x, double *b, int A_n, double tol);


/* Function definitions for sorting.c */
void quicksort (struct cell_t **a, int l, int r, char coordinate);
void quicksort_simple (int *table, int lo, int hi);
void selection_sort (struct cell_t **a, int n, char coordinate);
void quicksort_double_array (int* table1, int* table2, int l, int r);


/* Function definitions for metrics.c */
double hpwl ();
double distribution_variance ();


/* Function definitions for partition.c */
struct partition_t{
	struct cell_t **contents;
	int contentsCtr;

	double centerX;
	double centerY;

	double minX;
	double minY;
	double maxX;
	double maxY;

/*	#ifdef USE_AREA_CONSTRAINT */ /*GS addition*/
	double area;
/*	#endif */
    double cell_area;
	struct partition_t *next;
	} *partitions;

struct partition_t *cur_p, *root_partition, *curr_partition, *tmp_partition, *tail_partition, *previous_loop_tail, *partitions_tail, *pre_partition;
int partitionsCtr;

void init_partitions_list ();
void free_partitions_list ();
#ifdef USE_AREA_CONSTRAINT
double partition_area (struct partition_t *partition);
#endif
struct partition_t* bisect_partition (struct partition_t *partition, double alpha, char cut_direction);
double minimum_alpha (struct partition_t *partition, char cut_direction, double gamma);
void tripartition_area();
void print_partitions_list();
double calculate_distance (double cellX, double cellY, double centerX, double centerY);

/* Function definitions for legalizer.c */
void legalize (struct cell_t **cells_array);
//void legalizer_abacus_alike(double chip_height, double chip_width, struct cell_t **cells_array);
//void tetris(double chip_height, double chip_width, struct cell_t **cells_array);


/* Function definitions for plotter.c */
void plot_area (char *filename, short plot_partitions);	//plots the core area borders, cells, pins and partitioning lines


/* Function definitions for cs_extras.c */
int cs_resize (cs *A, int m, int n, int nzmax);
void cs_print_triplet(const cs *A, const char *outputFilename);
void cs_print_to_screen(cs *A);
int count_zeros (cs *A);

struct visited_cell_t {
    char name[50];
    struct visited_cell_t *next;
};

struct visited_cell_t *root_visited_cell, *tail_visited_cell, *cur_visited_cell, *tmp_visited_cell;

struct visited_terminal_t {
    char name[50];
    struct visited_terminal_t *next;
};

struct visited_terminal_t *root_visited_terminal, *tail_visited_terminal, *cur_visited_terminal, *tmp_visited_terminal;

#endif

