#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <math.h>

int main(int argc, char *argv[]) {
	int reps = 200000;
	int tmp, j, table[reps];
	time_t t1, t2, t3;
	srand((unsigned) clock());
	for (int i = 0; i < reps; i++) {
		table[i]= i;
	}
	t1 = clock();
	for (int i = reps-1; i > 0; i--){
		j = rand()/(RAND_MAX/i+1);
		tmp = table[j];
		table[j] = table[i];
		table[i] = tmp;
	}

	t2 = t3 = clock();
	for (int i = 0; i < reps; i++){
		printf("%d\n", table[i]);
	}
	printf("time: %d\n", t2-t1);
}
