#ifndef _PARSERS_H_
#define _PARSERS_H_
#include  <stdbool.h>

#define MAX_BOOKSHELF_FILES	10
#define MAX_FILENAME_SIZE	1024

#define MAX_STRING_LENGTH	32

#define COLOR_RED     "\x1b[31m"
#define COLOR_GREEN   "\x1b[32m"
#define COLOR_YELLOW  "\x1b[33m"
#define COLOR_BLUE    "\x1b[34m"
#define COLOR_MAGENTA "\x1b[35m"
#define COLOR_CYAN    "\x1b[36m"
#define COLOR_RESET   "\x1b[0m"

#define NULL_CHECK(X)\
	if(X==NULL){\
		perror(NULL);\
		exit(1);\
		}

#define CELLS_HASH_MODULO	50821
//some good hash table sizes
//101
//4099
//50821
//199933
//919393


/****************************************************************************************************************************************
 * A hash table that holds all the cells and terminals and their possitions. Stores the possition coordinates of the center. The total	*
 * number of cells is hold in "cellsCtr".												*
 ***************************************************************************************************************************************/
struct distance_info_t {
    double distance;
    int partition_number;
};

struct cell_t {
	char name[MAX_STRING_LENGTH];
	double centerX;		//position of the center
	double centerY;		//position of the center
	double sizeX;
	double sizeY;
	char tpf; //tlpf: flags for terminal, placed, fixed 
	int index;
    float distance;
	struct cell_t *next;
	//double coordinates_x;
	//double coordinates_y;
    //double area;
	//bool fixed;
	//bool placed;
	//char orientation[3];
	//int level;
	//double clique_weight;
    //bool visited;
    //bool already_checked;
    //struct distance_info_t distance_info[9];
    //a and b for line equation
    //double a;
    //double b;
    //double pinx_coord;
    //double piny_coord;
    //double centerX_orig;
    //double centerY_orig;
};

struct cell_t *cells[CELLS_HASH_MODULO];
struct cell_t *cells_list;
struct cell_t **list[9];
struct cell_t **movable_cells_list;
struct cell_t **id_table;
int *row_count;
//Cuda set up
char *name[MAX_STRING_LENGTH];
int *movables_cells_location;
char *tpf;
double *centerY, *centerX, *sizeX, *sizeY;
int *nets_table_row, *nets_table_col, *nets_table_value, *nets_table_start;

/****************************************************************************************************************************************
 * An array of structs, that holds all the nets. The "connections" array contains the indices to the "cells" array, indicating the	*
 * cells that the net is connected to. The total number of nets is hold in "netsCtr".							*
 ***************************************************************************************************************************************/
struct net_t {
	int connectionsCtr;
	struct cell_t **connections;
} *nets;

int cellsCtr;
int movablesCtr;
int movablesPadCtr;
int netsCtr;
int tupCtr;

double coreAreaWidth;	//the core area width
double coreAreaHeight;	//the core area height
double stdCellHeight;	//the standard cell height

int parse_bookshelf (char *path, char *aux);
void write_pl_file (char *file);
void print_parsed_info ();
void padding(struct cell_t** cells);
#endif

