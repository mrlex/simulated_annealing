#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <sys/utsname.h>
#include "parsers.h"
#include "gordian.h"

#ifndef PRINTING
#error "Please define macro PRINTING and recompile"
#endif

double grid_min;
double avg_area;


int cell_hash_function (char *name)
{
    int i, sum;

    sum = 0;
    for(i=0; i<strlen(name) && i<MAX_STRING_LENGTH; i++) {
        sum = (sum*7 + name[i]) % CELLS_HASH_MODULO;
    }
    return sum;
}


/* adds a cell to the hash table */
struct cell_t* cell_add (char *name)
{
    int pos;
    struct cell_t *new;

    pos = cell_hash_function (name);

    new = (struct cell_t*) malloc (sizeof(struct cell_t));
    if (new == NULL) {
        printf("Cannot allocate memory: cell_add %ld\n", sizeof(struct cell_t));
        exit(1);
    }

    strcpy(new->name, name);
    new->next = cells[pos];
    cells[pos] = new;
    cellsCtr++;

    return new;
}


/* find a cell inside the hash table and returns pointer to cell */
struct cell_t* cell_find (char *name)
{
    struct cell_t *i;

    i = cells[cell_hash_function(name)];

    while (i!=NULL) {
        if(!strcmp(i->name, name)) {
            return i;
        }
        i = i->next;
    }

    return NULL;
}


/* converts the hash table containing the cells, to a linked list */
void hash_to_list ()
{
    int i;
    struct cell_t *cur_cell, *last_cell=NULL; 

    //find the first cell in the hash table
    cells_list = cells[0];
    i=0;
    
    while(cells_list==NULL && i<CELLS_HASH_MODULO) {
        i++;
        cells_list = cells[i];
    }

    //link the last cell of every hash table row with the first cell of the next row
    cur_cell = cells[i];
    while(cur_cell!=NULL) {
        last_cell = cur_cell;
        cur_cell = cur_cell->next;
    }

    for(i=i+1; i<CELLS_HASH_MODULO; i++){
        cur_cell = cells[i];
        //make the link
        if(cur_cell==NULL) 
            continue;
        else 
            last_cell->next = cur_cell;

        //find the last cell of the hash table row
        while(cur_cell!=NULL) {
            last_cell = cur_cell;
            cur_cell = cur_cell->next;
        }
    }
}

char* random_name(char *name) {
	int key;
    static char charset[] = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";      
	name[0] = 'e';
	for(int i = 1; i<9; i++){
		key = rand()%(int)(sizeof(charset)-1);
		name[i] = charset[key];
	}
	name[9] = '\0';
	return name;
}
void padding(struct cell_t** cells_array) {
	
	int i;
	char *name;
	  
	time_t t;
	name = (char *)malloc(10*sizeof(char));
    if (name == NULL) {
        printf("Cannot allocate memory: padding %ld\n", 10*sizeof(char));
        exit(1);
    }
	struct cell_t *empty;
	int empty_rem = 2*coreAreaHeight/stdCellHeight-1;
	movablesPadCtr = movablesCtr+empty_rem+1;
	empty = cell_add(random_name(name));
	cellsCtr--;
	NULL_CHECK(empty);
	empty->centerX = coreAreaWidth;
	empty->centerY = coreAreaHeight - stdCellHeight/2;
	empty->sizeX = 0;
	empty->sizeY = stdCellHeight;
	empty->tpf = 1;
	empty->index = -1;
	
	//cells_tmp = (struct cell_t**) malloc ((movablesCtr + empty_rem + 1)*sizeof(struct cell_t*));
	//cells_array = (struct cell_t**) realloc(cells_array, (movablesCtr + empty_rem + 1)*sizeof(struct cell_t*));
	cells_array[movablesCtr+empty_rem] = empty;
	cells_array[movablesCtr+empty_rem-1] = cells_array[movablesCtr-1];
	for(i = movablesCtr-2; i >= 0; i--) {
		if(cells_array[i+empty_rem+1]->centerY>cells_array[i]->centerY) {
			empty = cell_add(random_name(name));
			NULL_CHECK(empty);
			empty->centerX = 0;
			empty->centerY = cells_array[i+empty_rem+1]->centerY;
			empty->sizeX = 0;
			empty->sizeY = stdCellHeight;
			empty->tpf = 1;
			empty->index = -1;
			cells_array[i+empty_rem] = empty;
			empty = cell_add(random_name(name));
			NULL_CHECK(empty);
			empty->centerX = coreAreaWidth;
			empty->centerY = cells_array[i]->centerY;
			empty->sizeX = 0;
			empty->sizeY = stdCellHeight;
			empty->tpf = 1;
			empty->index = -1;
			cells_array[i+empty_rem-1] = empty;
			empty_rem -=2;
			cellsCtr-=2;
		}
		cells_array[i+empty_rem] = cells_array[i];		
		//printf("%d:(%d) %s\n", i, empty_rem, cells_array[i+empty_rem]->name);
	}
	//printf("empty_rem = %d cells_array->name = %s\n", empty_rem, cells_array[0]->name);
	empty = cell_add(random_name(name));
	cellsCtr--;
	NULL_CHECK(empty);
	empty->centerX = 0;
	empty->centerY = 0;
	empty->sizeX = 0;
	empty->sizeY = stdCellHeight;
	empty->tpf = 1;
	empty->index = -1;
	cells_array[0] = empty;
//	free(cells_array);
//	cells_array = cells_tmp;
}


/* parses the necessary files in bookshelf format */
int parse_bookshelf (char *path, char *aux){

    int i, nodes_count, terminals_count, nets_count, netpins_count, rows_count, res;
    char filename[MAX_FILENAME_SIZE], tmp[16], line[1024];
    char file_aux[MAX_FILENAME_SIZE];
    char file_nets[MAX_FILENAME_SIZE];
    char file_nodes[MAX_FILENAME_SIZE];
    char file_pl[MAX_FILENAME_SIZE];
    char file_scl[MAX_FILENAME_SIZE];
    FILE *f;
    struct cell_t *cur_cell;

    #if (PRINTING==COMPACT)
    int last_print=-1;
    #endif

    #if (PRINTING==FULL)
    printf("Parsing...     "); fflush(stdout);
    #elif (PRINTING==COMPACT)
    printf("Parsing \t[ 0%%]"); fflush(stdout);
    #endif
    
    
    //initialize the hash table
    for(i=0; i<CELLS_HASH_MODULO; i++) 
        cells[i] = NULL;
	
    strcpy(file_aux, path);
    strcat(file_aux, "/");
    strcat(file_aux, aux);

    //open the auxiliary file
    f = fopen(file_aux, "r");
    NULL_CHECK(f);

    fscanf(f, "%*[^:]%*[:]");

    //read the filenames from the auxiliary file
    while (fscanf(f, "%s", filename) == 1) {

        if (sscanf(filename, "%*[^.]%*[.]%s", tmp)==1) {

            if(!strcmp(tmp, "nodes")) {
                strcpy(file_nodes, path);
                                strcat(file_nodes, "/");
                strcat(file_nodes, filename);
            }

            else if(!strcmp(tmp, "nets")) {
                strcpy(file_nets, path);
                                strcat(file_nets, "/");
                strcat(file_nets, filename);
            }

            else if(!strcmp(tmp, "pl")) {
                strcpy(file_pl, path);
                                strcat(file_pl, "/");
                strcat(file_pl, filename);
            }
            else if(!strcmp(tmp, "scl")) {
                strcpy(file_scl, path);
                                strcat(file_scl, "/");
                strcat(file_scl, filename);
            }
        }
    }

    fclose(f);


    //open the nodes file
    f = fopen(file_nodes, "r");
    NULL_CHECK(f);

    //ignore the first line containing the format name and revision
    fgets(line, sizeof(line), f);

    //ignore the commented lines with info about the origin of the file
    do {
        fgets(line, sizeof(line), f);
    } while(line[0]=='#');

    //parse the number of nodes and number of terminals
    if(!fscanf(f, "%*[^:]%*[:]%d", &nodes_count)){
        printf("\nERROR: nodes file does not contain number of nodes\n");
        exit(1);
        }

    if(!fscanf(f, "%*[^:]%*[:]%d", &terminals_count)){
        printf("\nERROR: nodes file does not contain number of terminals\n");
        exit(1);
        }
    
	id_table = (struct cell_t**) malloc ((nodes_count) * sizeof(struct cell_t*));
    if (id_table == NULL) {
        printf("Cannot allocate memory: id_table %ld\n", (nodes_count) * sizeof(struct cell_t*));
        exit(1);
    }
    movablesCtr = cellsCtr = i = 0;
    int terminalStart = nodes_count-terminals_count;
    double min_x = INFINITY;
    double total_cell_area =0;
    while (cellsCtr < nodes_count) {
        double sizeX, sizeY;
        char name[MAX_STRING_LENGTH];
        tmp[0]='\0';
        res = fscanf(f, "%s%lf%lf%*[ \t]%[^ \t\n]%*[ \t\n]", name, &sizeX, &sizeY, tmp);

        if (res < 3 || res > 4 || (res == 4 && strncasecmp(tmp, "terminal", 8))) {
            printf("\nERROR: in nodes file while parsing cell number: %d\n", cellsCtr);
            exit(1);
        }

        cur_cell = cell_add (name);
        NULL_CHECK(cur_cell);

        cur_cell->sizeX = sizeX;
        cur_cell->sizeY = sizeY;
        //cur_cell->area = sizeX * sizeY;
        //cur_cell->clique_weight = 0.0;

        if (res == 3) {
            cur_cell->tpf = 0;
            //cur_cell->level = -1;
            cur_cell->index = movablesCtr;
            if (cur_cell->sizeX < min_x) {
                min_x = cur_cell->sizeX;
            }    
            id_table[movablesCtr] = cur_cell;
            movablesCtr++;
        }
        else if (res == 4) {
            cur_cell->tpf = 0x4;
            //cur_cell->level = 0;
            cur_cell->index = i;
            id_table[terminalStart+i] = cur_cell;
            i++;    //counts the terminals
        }

        #if (PRINTING==COMPACT)
        if ((int)(cellsCtr*33/nodes_count) > last_print) {
            last_print = (int)(cellsCtr*33/nodes_count);
            printf("\b\b\b\b\b[%2d%%]", last_print); fflush(stdout);
        }
        #endif
        total_cell_area += cur_cell->sizeX * sizeY;
    }
    avg_area = total_cell_area/cellsCtr;
    grid_min = min_x / 10.0;
    fclose(f);

    if (i != terminals_count) {
        printf("\nERROR: Terminal count mismatch\n");
        exit(1);
    }

    if (terminals_count<=0) {
        printf("\nERROR: No fixed terminals found\n");
        exit(1);
    }

    //open the pl file
    f = fopen(file_pl, "r");
    NULL_CHECK(f);

    //ignore the first line containing the format name and revision
    fgets(line, sizeof(line), f);

    //ignore the commented lines with info about the origin of the file
    do {
        fgets(line, sizeof(line), f);
    } while(line[0]=='#');

    i = 0;
    while (i < cellsCtr) {
        double centerX, centerY;
        char name[MAX_STRING_LENGTH];

        tmp[0] = line[0] = '\0';
        res = fscanf(f, "%s%lf%lf%*[ \t]%*[:]%*[ \t]%[^ \t\n]%*[ \t]%[^ \t\n]%*[ \t\n]", name, &centerX, &centerY, tmp, line);

        cur_cell = cell_find (name);
        if (cur_cell == NULL) {
            printf("\nERROR: Cell %s in pl file was not found in nodes file\n", name);
            exit(1);
        }

        cur_cell->centerX = centerX;
        cur_cell->centerY = centerY;
        if (res>=4) {
            //strcpy(cur_cell->orientation, tmp);
        }
        else { 
            //strcpy(cur_cell->orientation, "-");
        }
        if(!strncasecmp(line, "/fixed", 6)){
            cur_cell->tpf = cur_cell->tpf|0x1;
            if(!(cur_cell->tpf&0x4)){
                printf("\nERROR: Cell %s in pl file is fixed. Currently supporting fixed terminal only\n", name);
                exit(1);
                }
            }/*
        else{
            cur_cell->tpf = cur_cell->tpf;
            if(cur_cell->terminal){
                printf("\nERROR: Cell %s in pl file is a terminal but is not declared fixed\n", name);
                exit(1);
                }
            }*/

        #if (PRINTING==COMPACT)
        if((int)(i*33/cellsCtr)+33 > last_print){
            last_print = (int)(i*33/cellsCtr)+33;
            printf("\b\b\b\b\b[%2d%%]", last_print); fflush(stdout);
            }
        #endif

        i++;
        }

    fclose(f);
    //int non_zero, *nets_table_z;

    //open the nets file
    f = fopen(file_nets, "r");
    NULL_CHECK(f);

    //ignore the first line containing the format name and revision
    fgets(line, sizeof(line), f);

    //ignore the commented lines with info about the origin of the file
    do{
        fgets(line, sizeof(line), f);
    }while(line[0]=='#');

    //parse the number of nets and number of pins
    if(!fscanf(f, "%*[^:]%*[:]%d", &nets_count)){
        printf("\nERROR: nets file does not contain number of nets\n");
        exit(1);
        }

    if(!fscanf(f, "%*[^:]%*[:]%d", &netpins_count)){
        printf("\nERROR: nets file does not contain number of netpins\n");
        exit(1);
        }

    nets = (struct net_t*) malloc (nets_count * sizeof(struct net_t));
    if (nets == NULL) {
        printf("Cannot allocate memory: nets %ld\n", nets_count * sizeof(struct net_t));
        exit(1);
    }

    int* nets_table_temp0 = (int *)calloc(2*netpins_count,sizeof(int));
    int* nets_table_temp1 = (int *)calloc(2*netpins_count,sizeof(int));
    int nets_tableCtr = 0;
    netsCtr = i = 0;
    while(netsCtr<nets_count){
        int net_degree, net_deg0, out_id, in_id;
        char name[MAX_STRING_LENGTH];

        res = fscanf(f, "%*[^:]%*[:]%*[ \t\n]%d%*[^\n]%*[\n]", &net_degree);
        if(res<1){
            printf("\nERROR: net degree could not be parsed from nets file\n");
            exit(1);
            }

        i+=net_degree;

        nets[netsCtr].connectionsCtr = net_degree;
		net_deg0 = net_degree;
        nets[netsCtr].connections = (struct cell_t**) malloc (net_degree * sizeof(struct cell_t*));
        if (nets[netsCtr].connections == NULL) {
            printf("Cannot allocate memory: connections %ld\n", net_degree * sizeof(struct cell_t*));
            exit(1);
        }
		//non_zero = 0;
        while(net_degree>0){
            fscanf(f, "%s%*[ \t]%*[^\n]%*[\n]", name);
            cur_cell = cell_find (name);
            if(cur_cell == NULL){
                printf("\nERROR: Cell %s in nets file was not found in nodes file\n", name);
                exit(1);
                }
            if (net_deg0 == net_degree) {
				if(cur_cell->tpf == 0x4)
					out_id = cur_cell->index + movablesCtr;
				else
					out_id = cur_cell->index;
				}
			else {
				if(cur_cell->tpf == 0x4){
					in_id = cur_cell->index + movablesCtr;
                    nets_table_temp0[nets_tableCtr] = out_id;
                    nets_table_temp1[nets_tableCtr] = in_id;
                    nets_table_temp0[nets_tableCtr+1] = in_id;
                    nets_table_temp1[nets_tableCtr+1] = out_id;
                    nets_tableCtr+=2;
					//non_zero += 2;
				}
				else{
					in_id = cur_cell->index;
                    nets_table_temp0[nets_tableCtr] = out_id;
                    nets_table_temp1[nets_tableCtr] = in_id;
                    nets_table_temp0[nets_tableCtr+1] = in_id;
                    nets_table_temp1[nets_tableCtr+1] = out_id;
                    nets_tableCtr+=2;
					//non_zero += 2;
				}
			}

            nets[netsCtr].connections[net_degree-1]    = cur_cell;
            net_degree--;
        }

        #if (PRINTING==COMPACT)
        if((int)(netsCtr*34/nets_count)+66 > last_print){
            last_print = (int)(netsCtr*34/nets_count)+66;
            printf("\b\b\b\b\b[%2d%%]", last_print); fflush(stdout);
        }
        #endif    
        netsCtr++;
    }
    fclose(f);
    if(i!=netpins_count){
        printf("\nERROR: Netpins count mismatch\n");
        exit(1);
        }

	//Sorting + creating COO nets table
    quicksort_double_array (nets_table_temp0, nets_table_temp1, 0, nets_tableCtr-1);

    int start = 0;
    int length = 0;
    for (i = 0; i < cellsCtr; i++){
        while (nets_table_temp0[start+length] == i) {
            length++;
        }
        if (length>1){
            quicksort_simple(nets_table_temp1, start, start+length-1);
        }
        if (start+length>=nets_tableCtr)
            break;
        start += length;
        length = 0;
    }
    printf("netpins_count: %d, nets_tableCtr: %d, start: %d\n", netpins_count, nets_tableCtr, start);
    nets_table_row = (int *)malloc(nets_tableCtr*sizeof(int));
    if (nets_table_row == NULL) {
        printf("Cannot allocate memory: nets_table_row %ld\n", nets_tableCtr * sizeof(int));
        exit(1);
    }

    nets_table_col = (int *)malloc(nets_tableCtr*sizeof(int));
    if (nets_table_col == NULL) {
        printf("Cannot allocate memory: nets_table_col %ld\n", nets_tableCtr * sizeof(int));
        exit(1);
    }

    nets_table_value = (int *)malloc(nets_tableCtr*sizeof(int));
    if (nets_table_value == NULL) {
        printf("Cannot allocate memory: nets_table_value %ld\n", nets_tableCtr * sizeof(int));
        exit(1);
    }
    nets_table_row[0] = nets_table_temp0[0];
    nets_table_col[0] = nets_table_temp1[0];
    nets_table_value[0] = 1;
    tupCtr = 0;
    for (i = 1; i < nets_tableCtr; i++){
        if (nets_table_temp0[i] == nets_table_temp0[i-1]){
            if (nets_table_temp1[i] == nets_table_temp1[i-1]){
                nets_table_value[tupCtr] ++;
            }
        }
        tupCtr++;
        nets_table_row[tupCtr] = nets_table_temp0[i];
        nets_table_col[tupCtr] = nets_table_temp1[i];
        nets_table_value[tupCtr] = 1;
    }


    int* tmp_array;
    tmp_array = (int *)realloc(nets_table_row, tupCtr*sizeof(int));
    if (tmp_array == NULL) {
        printf("Cannot allocate memory: realloc nets_table_row %ld\n", tupCtr*sizeof(int));
        exit(1);
    }
    nets_table_row = tmp_array;

    tmp_array = (int *)realloc(nets_table_col, tupCtr*sizeof(int));
    if (tmp_array == NULL) {
        printf("Cannot allocate memory: realloc  nets_table_col %ld\n", tupCtr*sizeof(int));
        exit(1);
    }
    nets_table_col = tmp_array;

    tmp_array = (int *)realloc(nets_table_value, tupCtr*sizeof(int));
    if (tmp_array == NULL) {
        printf("Cannot allocate memory: realloc nets_table_value %ld\n", tupCtr*sizeof(int));
        exit(1);
    }
    nets_table_value = tmp_array;


    //open the scl file
    f = fopen(file_scl, "r");
    NULL_CHECK(f);

    //ignore the first line containing the format name and revision
    fgets(line, sizeof(line), f);

    //ignore the commented lines with info about the origin of the file
    do{
        fgets(line, sizeof(line), f);
    }while(line[0]=='#');

    //parse the number of rows
    if(!fscanf(f, "%*[^:]%*[:]%d", &rows_count)){
        printf("\nERROR: scl file does not contain number of rows\n");
        exit(1);
        }

    //ignore all the rows till the last one
    for(i=0; i<rows_count-1; i++){

        do{
            fgets(line, sizeof(line), f);
            sscanf(line, "%s", tmp);
            }while(strncasecmp(tmp, "end", 3));
        }

    //parse the coordinate of the last row
    do{
        fscanf(f, "%s", line);
        }while(strncasecmp(line, "coordinate", 10));

    fscanf(f, "%*[^:]%*[:]%lf", &coreAreaHeight);
    

    //parse the height of the last row
    do{
        fscanf(f, "%s", line);
        }while(strncasecmp(line, "height", 6));

    fscanf(f, "%*[^:]%*[:]%lf", &stdCellHeight);
    coreAreaHeight += stdCellHeight;

    //parse the numsites of the last row
    do{
        fscanf(f, "%s", line);
        }while(strncasecmp(line, "numsites", 8));

    fscanf(f, "%*[^:]%*[:]%lf", &coreAreaWidth);

    fclose(f);

    hash_to_list ();

    #if (PRINTING==FULL)
    printf("\b\b\b\b\b\b\b\b...     \t[OK]");
    #elif (PRINTING==COMPACT)
    printf("\b\b\b\b\b[OK] \n"); fflush(stdout);
    #endif
    return 1;
    }


/* prints information about the parsed elements */
void print_parsed_info (){
    printf("\n------------------------------------------ PARSED INFO --------------------------------------------------------\n\n");
    printf("\t      cellsCtr: %7d\n", cellsCtr);
    printf("\t   movablesCtr: %7d\n", movablesCtr);
    printf("\t       netsCtr: %7d\n", netsCtr);
    printf("\t coreAreaWidth: %7.0lf\n", coreAreaWidth);
    printf("\tcoreAreaHeight: %7.0lf\n", coreAreaHeight);
    printf("\t stdCellHeight: %7.0lf\n", stdCellHeight);
    printf("\n----------------------------------------------------------------------------------------------------------------\n\n");
    }


/* creates the output .pl file that contains the final possitions of the cells */
void write_pl_file (char *file){

    FILE *f;
    time_t current_time = time(NULL);
    char *time_string;
    struct utsname unameData;

    if(uname(&unameData) < 0) perror("uname failed");

    time_string = ctime(&current_time);
    if(time_string[strlen(time_string)-1]=='\n') time_string[strlen(time_string)-1]='\0';

    f = fopen(file, "w");
    NULL_CHECK(f);

    fprintf(f, "UCLA pl 1.0\n");
    fprintf(f, "# Created     : %s\n", time_string);
    fprintf(f, "# User        : iostiv@inf.uth.gr (Stavros K. Ioannidis)\n");
    fprintf(f, "# Platform    : %s %s\n\n", unameData.sysname, unameData.release);

    struct cell_t *cur_cell = cells_list;
    while(cur_cell!=NULL){
        //fprintf(f, "\n%s\t%e\t%e\t:\t%s", cur_cell->name, cur_cell->centerX, cur_cell->centerY, cur_cell->orientation);
        cur_cell = cur_cell->next;
    }

    fclose(f);
    }


