#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
//#include <sys/time.h>
#include <malloc.h>
#include "parsers.h"
//#include <stdbool.h>
#include "gordian.h"
#include <time.h>
#include "simulated_annealing.h"

#define MIN(x, y) (((x) < (y)) ? (x) : (y))

double hpwl (){

	int i, j;
	double sum = 0.0;

	for(i=0; i<netsCtr; i++){
		double minX=coreAreaWidth, minY=coreAreaHeight, maxX=0.0, maxY=0.0;
 
		for(j=0; j<nets[i].connectionsCtr; j++){
			if(nets[i].connections[j]->centerX < minX) minX = nets[i].connections[j]->centerX;
			if(nets[i].connections[j]->centerY < minY) minY = nets[i].connections[j]->centerY;
			if(nets[i].connections[j]->centerX > maxX) maxX = nets[i].connections[j]->centerX;
			if(nets[i].connections[j]->centerY > maxY) maxY = nets[i].connections[j]->centerY;
			}

			sum += ((maxX-minX) + (maxY-minY));
		}
	return sum;
	}

void create_movables_cells_lists()
{
    movable_cells_list = (struct cell_t**) malloc (movablesCtr * sizeof(struct cell_t*));
    if (movable_cells_list == NULL) {
		printf("Cannot allocate memory: create_movables_cells_lists %ld\n", movablesCtr * sizeof(struct cell_t*));
		exit(1);
	}
    struct cell_t *cur_cell = cells_list;
    while (cur_cell!=NULL) { 
        if (cur_cell->tpf==0) {
            movable_cells_list[cur_cell->index] = cur_cell;
        }
        cur_cell = cur_cell->next;
    }
}

	
//initial placement strategy
void init_placement (struct cell_t** cells) {	
	int i;
		
	//random placement

	time_t t;
	int rows = coreAreaHeight / stdCellHeight;
	srand((unsigned) time(&t));
	for (i = 0; i<movablesCtr; i++){
		cells[i]->centerY = (rand()/(RAND_MAX/rows+1)) * stdCellHeight+ stdCellHeight/2;
		cells[i]->centerX = rand()/(RAND_MAX/(coreAreaWidth+1)+1);
		if ((cells[i]->centerX - cells[i]->sizeX/2) < 0)
			cells[i]->centerX = cells[i]->sizeX/2;
		else if ((cells[i]->centerX + cells[i]->sizeX/2) > coreAreaWidth)
			cells[i]->centerX = coreAreaWidth - cells[i]->sizeX/2;
	}
}
		
void free_everything() {
	free(cells_list);
	free(movable_cells_list);
	free(id_table);
	free(nets_table_col);
	free(nets_table_row);
	free(nets_table_value);
	free(row_count);
}
int main (int argc, char *argv[])
{
	//int i, j;//, z, h, count;
	time_t current_time;//, total_divide, tmp;
	clock_t start, end;
	double starting_cost, starting_cost_hpwl, final_cost, final_cost_hpwl;
	current_time = time(NULL);
	parse_bookshelf (argv[1], argv[2]);	// 1. parsing
	
	printf("\n");
	print_parsed_info();
	create_movables_cells_lists();
	
	init_placement(movable_cells_list);
	legalize(movable_cells_list);
	
	int empty_rem = 2*coreAreaHeight/stdCellHeight-1;
	movablesPadCtr = movablesCtr+empty_rem+1;
	struct cell_t** tmp_array;
	tmp_array = (struct cell_t**) realloc(movable_cells_list, (movablesCtr + empty_rem + 1)*sizeof(struct cell_t*));
	if (tmp_array == NULL) {
		printf("Cannot allocate memory: movable_cells_list %ld\n", (movablesCtr + empty_rem + 1)*sizeof(struct cell_t*));
		exit(1);
	}
	movable_cells_list = tmp_array;
	padding(movable_cells_list);
	char s1[40], s2[40];
	sprintf(s1, "%ld\n", time(&current_time));
	//plot_area(s1,0);
	starting_cost = total_cost_table()/2;
	starting_cost_hpwl = hpwl();
	int iter = 100000*(argv[3][0]-'0');
	int T = 10000000*(argv[4][0]-'0');
	printf("\n%d %d \n", iter, T);
	printf("\n");
	start = clock();
	annealing(movable_cells_list, T, MIN(coreAreaHeight, coreAreaWidth), movablesCtr);
	end = clock();
	output_check(movable_cells_list);
	sprintf(s2, "%ld\n", time(&current_time)+1);

	
	final_cost = total_cost_table()/2;
	final_cost_hpwl = hpwl();
	//printf("Output1: %s Output2: %s\nStarting total wire length: \t\t%f\nFinal total wire length: \t\t%f\n", s1, s2, starting_cost, final_cost);
	//printf("Starting total wire length(hpwl): \t%f\nFinal total wire length(hpwl): \t\t%f\n", starting_cost_hpwl, final_cost_hpwl);
	FILE *f;
	char costs[10];
	sprintf(costs, "../%c %c", argv[3][0], argv[4][0]);
	f = fopen(costs, "a");
	fprintf(f, "%f \t %f \t cells/time: \t%d\t%ld\n", starting_cost_hpwl, final_cost_hpwl, cellsCtr, end - start);
	fclose(f);
	//plot_area(s2,0);
	free_everything();
	return 0;
}
/*	
	for(int i = 0; i<cellsCtr; i++){
		printf("%s: ", id_table[i]->name);
		for(int j = 0; j<cellsCtr; j++) {
			if(nets_table[j+i*cellsCtr]!=0){
				printf("%d: %s ", nets_table[j+i*cellsCtr], id_table[j]->name);
			}
		}
		printf("\n");
	}
	int idA;
	for(int i = 0; i<100; i++) {
		do {
			idA = (int)rand()/(RAND_MAX/(movablesPadCtr)+1);							//1
		}while(movable_cells_list[idA]->sizeX==0);
		printf("%s: centerX = %f, centerY = %f\n", id_table[idA]->name, id_table[idA]->centerX, id_table[idA]->centerY);
		node_cost(idA, id_table[idA]->centerX, id_table[idA]->centerY);
	}
	
	for(int i = 0; i<cellsCtr; i++) {
		printf("%s:", id_table[i]->name);
		for(int j = 0; j<cellsCtr; j++) {
			printf("\t%d", nets_table[j+i*cellsCtr]);
		}
		printf("\n");
	}*/
	//printf("mcl0: center = %f, size = %f, mcl1: center = %f, size = %f\n", movable_cells_list[0]->centerX, movable_cells_list[0]->sizeX, movable_cells_list[1]->centerX, movable_cells_list[1]->sizeX);
	//output_check(movable_cells_list);
	//printf("Total wire length(table): %f\n", total_cost_table());
	//printf("coreAreaWidth= %f, coreAreaHight = %f\n" ,coreAreaWidth, coreAreaHeight);
/*	i = j = 0;
	int k;
	current_time = clock();
	int accept = 0;
	for(k = 0; k<100000;k++) {
		accept += cell_select(movable_cells_list, MIN(coreAreaHeight, coreAreaWidth), i,j);
	}
	printf("\ncell_select median = %ld\n, reps =%d, accept = %d\n", (clock()-current_time), k, accept);

	*/

