#include <stdio.h>
#include <stdlib.h>
#include "gordian.h"


/* Legalizes the cells. First moves the cells so they all fit inside rows. Then moves the cells so they do not overlap. */
void legalize (struct cell_t **cells_array){

    int i, j, k;
    double pos_y, pos_x;        //used in final placement
    int numrows;            //the count of rows in the core area
//    int *row_count;            //the count of cells inside every row
    double *row_len;        //the total length of cells inside every row
    int sweeps=0;            //number of sweeps needed in final placement
    int done;            //indicates the completion of the final placement
    int offset;            //distance in rows, where the extra cells are attempted to be moved to
    #if (PRINTING==COMPACT)
    //int last_printed_percentage=0;    //used for printing
    #endif

    #if (PRINTING==FULL)
    printf("\nLegalizing..."); fflush(stdout);
    #elif (PRINTING==COMPACT)
    printf("\rLegalizing\t[ 0%%]"); fflush(stdout);
    #endif

    //sort the cells (ascending y-coordinate order)
    quicksort (cells_array, 0, movablesCtr-1, 'Y');

    numrows = (int)(coreAreaHeight/stdCellHeight);
    pos_y = stdCellHeight / 2;
    pos_x = 0;

    //allocate memory for the row_count and row_len vectors
    row_count = (int*) malloc (numrows * sizeof(int));
    if (row_count == NULL) {
        printf("Cannot allocate memory: row_count %ld\n", numrows * sizeof(int));
        exit(1);
    }
    row_len = (double*) malloc (numrows * sizeof(double));
    if (row_len == NULL) {
        printf("Cannot allocate memory: row_len %ld\n", numrows * sizeof(double));
        exit(1);
    }

    for (i=0; i<numrows; i++){
        row_count[i] = 0;
        row_len[i] = 0.0;
        }

    i=j=0;
	//int *empty_order;
	//empty_order = (int *)malloc(numrows*sizeof(int));
    //calculate the cell count and row length of every row    
    do{    
        if (cells_array[j]->centerY <= pos_y + stdCellHeight/2 && ( cells_array[j]->centerY > pos_y - stdCellHeight/2 || cells_array[j]->centerY == 0)){
            row_count[i]++;
            /*if (row_len[i] += cells_array[j]->sizeX > coreAreaWidth){
				cells_array[j]->centerY += stdCellHeight;
			}*/
            row_len[i] += cells_array[j]->sizeX;
            j++;
            }
        else {
            pos_y += stdCellHeight;
            i++;
            }
    } while (j<movablesCtr);
    done = 1;

	    
    do {
    offset=1;
    do {
        sweeps++;
        j=0;
        done=1;

        for (i=0; i<numrows; i++){

            if (row_len[i] > coreAreaWidth){
                int move_cell;
                int move_direction=0;                

                //see if the extra cell can be moved 'offset' rows above or below
                if ((i>=offset && i<numrows-offset && row_len[i-offset] >= row_len[i+offset] && row_len[i] > row_len[i+offset] + cells_array[j+row_count[i]-1]->sizeX) ){ //|| i<offset){
                    move_cell = j+row_count[i]-1;
                    move_direction = 1;
                    }
                else if ((i>=offset && i<numrows-offset && row_len[i-offset] < row_len[i+offset] && row_len[i] > row_len[i-offset] + cells_array[j]->sizeX) ){ //|| i>=numrows-offset){
                    move_cell = j;
                    move_direction = -1;
                    }
                else if ((i < offset && row_len[i] > row_len[i+offset] + cells_array[j+row_count[i]-1]->sizeX)){
					move_cell = j+row_count[i]-1;
					move_direction = 1;
				}
				else if (i>=numrows-offset && row_len[i] > row_len[i-offset] + cells_array[j]->sizeX) {
                    move_cell = j;
                    move_direction = -1;
                    }
                    
                //move the cell
                if (move_direction != 0){
                    int move_to=j;

                    if (offset==1) cells_array[move_cell]->centerY = cells_array[move_cell+move_direction]->centerY;
                    else {
                        if (move_direction > 0) for (k=0; k<offset; k++) move_to += row_count[i+k];
                        else {
                            move_to--;
                            for (k=1; k<offset; k++) move_to -= row_count[i-k];
                            }

                        cells_array[move_cell]->centerY = cells_array[move_to]->centerY;
                        }

                    row_count[i]--;
                    row_count[i+move_direction*offset]++;
                    row_len[i] -= cells_array[move_cell]->sizeX;
                    row_len[i+move_direction*offset] += cells_array[move_cell]->sizeX;
                    done=0;

                    //restore the cells' sorting
                    if (offset>1) {
                        if (move_direction > 0) quicksort (cells_array, move_cell, move_to, 'Y');
                        else quicksort (cells_array, move_to, move_cell, 'Y');
                        }
                    }

                if (move_direction < 0) j++;

                //if (move_direction != 0 && offset > 1) break;
                }
            j += row_count[i];
            }

        if (done) offset++;
        else offset=1;

        } while (!(done && offset==numrows));
		done = 0;
		for(i =0; i< numrows; i++) {
			if (row_len[i] > coreAreaWidth) {
				done = 1;
				printf("\n *********BOOM********* \n");
			}
		}
	} while (done == 1);
		
    #if (PRINTING==COMPACT)
    printf("\rLegalizing\t[50%%]"); fflush(stdout);
    #endif

    j=0;
    pos_y = stdCellHeight / 2;

    //legalize and spread cells inside every row
    for (i=0; i<numrows; i++){

        /*#if (PRINTING==COMPACT)
        if((int)(j*100/movablesCtr) > last_printed_percentage){
            last_printed_percentage = (int) (j*100/movablesCtr);
            printf("\rLegalizing\t[%2d%%]", last_printed_percentage); fflush(stdout);
            }
        #endif*/

        //sort the cells in the i-th row (ascending x-coordinate order)
        quicksort (cells_array, j, j+row_count[i]-1, 'X');

        //pos_x = (coreAreaWidth - row_len[i]) / 2;
		pos_x = 0;
		double spacing;
        for (k=j; k<j+row_count[i]; k++) {
			spacing = (coreAreaWidth - row_len[i])/(row_count[i]-1);
            cells_array[k]->centerY = pos_y;
            cells_array[k]->centerX = pos_x + cells_array[k]->sizeX / 2;
            pos_x += cells_array[k]->sizeX + spacing;
            }
            
        pos_y += stdCellHeight;
        j += row_count[i];
        }

    #if (PRINTING==FULL)
    printf("\b\b\b\b\bin %d sweeps\n", sweeps);
    #elif (PRINTING==COMPACT)
    printf("\rLegalizing\t[OK] \n"); fflush(stdout);
    #endif
}




////////////////

#include <stdio.h>
#include <stdlib.h>
#include "gordian.h"
#include <stdbool.h>

//////////////////////////
typedef struct sorted
{
    struct cell_t *next_cell;
    struct sorted *next;
}sort;

typedef struct rows
{
    double row_max;
    double filled;
}row;

sort *curr_sort, *pre_sort, *temp_sort,*curr2_sort, *pre2_sort, *root_sort;
