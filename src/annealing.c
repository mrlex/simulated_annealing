#include "gordian.h"
#include "parsers.h"
#include "simulated_annealing.h"
#include <time.h>
#define MIN(x, y) (((x) < (y)) ? (x) : (y))
#define MAX(x, y) (((x) > (y)) ? (x) : (y))

int divide(struct cell_t** cells_array, double x, double y) {
	int j, range, i = movablesPadCtr/2;
	range = movablesPadCtr/4;
	j = 0;
	y = y - fmod(y,stdCellHeight) + stdCellHeight/2;
	while (j == 0) {
		/*if (i == 0){
			if ((x > cells_array[i]->centerX - cells_array[i]->sizeX/2) && (x< cells_array[i]->centerX + cells_array[i]->sizeX/2)) {
				j = 1;
			}
			else {
				i = -1;
				j = 1;
			}
		}
		else if (i == movablesPadCtr-1){
			if (x > cells_array[i]->centerX - cells_array[i]->sizeX/2 && x< cells_array[i]->centerX + cells_array[i]->sizeX/2)
				j = 1;
			else {
				i = -1;
				j = 1;
			}
		}*/
		if(i == 0||i == movablesPadCtr-1){
			i =-1;
			break;
		}
		if (fabs(cells_array[i]->centerY - y)<0.001) {
			if(x < cells_array[i]->centerX - cells_array[i]->sizeX/2) {
				if((y == cells_array[i-1]->centerY) && (x < cells_array[i-1]->centerX + cells_array[i-1]->sizeX/2)){
					i = i - range;
				}
				else {
					i = -1;
					j = 1;
				}
			}
			else if(x > cells_array[i]->centerX + cells_array[i]->sizeX/2){
				if((y == cells_array[i+1]->centerY) && (x > cells_array[i+1]->centerX - cells_array[i+1]->sizeX/2)){
					i = i + range;
				}
				else {
					i = -1;
					j = 1;
				}
			}
			else {
				j = 1;
			}
		}
		else if (y < cells_array[i]->centerY){
			i = i - range;
		}
		else if (y > cells_array[i]->centerY){
			i = i + range;
		}
		range = range/2;
		if(range == 0)
			range =1;
	}
	
	return i;
}

int cell_select(struct cell_t** cells_array, double range, int* idA, int* idB) {
	int i, j, accept;
	double x, y;
	time_t t;
	i = accept = 0;
	do {
		j = 0;
		do {
			*idA = (int)rand()/(RAND_MAX/(movablesPadCtr)+1);							//1
		}while(cells_array[*idA]->sizeX==0);
		do {

			do {
				*idB = (int)rand()/(RAND_MAX/(movablesPadCtr)+1);							//1
			}while(cells_array[*idB]->sizeX==0 || *idA==*idB);
		
			/*if(*idA == *idB) {
				if(*idB>1) *idB-=1;
				else *idB = 2;
			}*/
			if(cells_array[*idA]->sizeX - (cells_array[*idB+1]->centerX-cells_array[*idB+1]->sizeX/2) + (cells_array[*idB-1]->centerX + cells_array[*idB-1]->sizeX/2) < 0.000001){
				if(cells_array[*idB]->sizeX - (cells_array[*idA+1]->centerX-cells_array[*idA+1]->sizeX/2) + (cells_array[*idA-1]->centerX + cells_array[*idA-1]->sizeX/2)< 0.000001){
					accept = 1;
				}
			}
			if (*idB==*idA) {
				accept = 0;
			}
			j++;
			//printf("\ncell_select i = %d, j = %d, idA = %d, idB = %d", i, j, idA, idB);
		}while((j < 40 && accept == 0));	
		i++;		
	}while(i < 25 && accept == 0);	/*
	if(accept&&range<10) {
		printf("\n%d, %d, %d", i,j,accept);
		printf("\nidA = %d, centerX = %f, centerY = %f, sizeX = %f", *idA, cells_array[*idA]->centerX, cells_array[*idA]->centerY, cells_array[*idB]->sizeX);
		printf("\nidB = %d, centerX = %f, centerY = %f, sizeX = %f", *idB, cells_array[*idB]->centerX, cells_array[*idB]->centerY, cells_array[*idB]->sizeX);
	}*/
	return accept;
}
int random_accept(double c, double t){	
	return (rand()/(RAND_MAX/100000) < (exp(-c/t)*100000));
}

int sa_move(struct cell_t** cells_array, double t, int idA, int idB, double* dc){
	double cost1, cost2, tmpY;
	int accept = 0;
	time_t t1,t2;
	t1=clock();
	cost1 = node_cost(cells_array[idA]->index, cells_array[idA]->centerX, cells_array[idA]->centerY) + node_cost(cells_array[idB]->index, cells_array[idB]->centerX, cells_array[idB]->centerY);
	cost2 = node_cost(cells_array[idB]->index, cells_array[idA]->centerX, cells_array[idA]->centerY) + node_cost(cells_array[idA]->index, cells_array[idB]->centerX, cells_array[idB]->centerY);
	t2 = clock();
	//printf("%ld\t", t2-t1);
	//printf("1 = %f, 2 = %f\n", node_cost(idA, cells_array[idA]->centerX, cells_array[idA]->centerY), node_cost(idB, cells_array[idB]->centerX, cells_array[idB]->centerY));
	//printf("1 = %d, 2 = %d\n", idA, idB);
	if(cost2-cost1<0.0001){
		accept = 1;
	}
	else{
		accept = random_accept(cost2-cost1, t);
	}
	//printf("idA: %d, idB: %d cost diff:\t%f, accept: %d\n", idA, idB, cost2-cost1, accept);
	if(accept == 1) {
		t1 = clock();
		struct cell_t* tmpcell;
		//tmpX = cells_array[idA]->centerX;
		tmpY = cells_array[idA]->centerY;
		cells_array[idA]->centerX = cells_array[idB-1]->centerX/2 + cells_array[idB-1]->sizeX/4 + cells_array[idB+1]->centerX/2 - cells_array[idB+1]->sizeX/4;
		cells_array[idA]->centerY = cells_array[idB]->centerY;
		cells_array[idB]->centerX = cells_array[idA-1]->centerX/2 + cells_array[idA-1]->sizeX/4 + cells_array[idA+1]->centerX/2 - cells_array[idA+1]->sizeX/4;
		cells_array[idB]->centerY = tmpY;
		tmpcell = cells_array[idA];
		cells_array[idA] = cells_array[idB];
		cells_array[idB] = tmpcell;

		//cost3 = node_cost(cells_array[idA]->index, cells_array[idA]->centerX, cells_array[idA]->centerY) + node_cost(cells_array[idB]->index, cells_array[idB]->centerX, cells_array[idB]->centerY);
		//printf("cost3 = %f, cost2-1 = %f, cost2 = %f, cost1 = %f\n", cost3, cost2-cost1, cost2, cost1);
		*dc += cost2-cost1;
		t2 = clock();
		//printf("%ld\t", t2-t1);
	}
	//printf("\n");
	return accept;
}
	
void update_vars(int a, double* t, double* r, int iter) {
	
	double fraction_a = (double)a/iter;
	if (fraction_a > 0.90) {
		*t = *t*0.85;
		//*r = *r*0.8;
		//j = 0.5;
	}
	else if (fraction_a > 0.8) {
		*t = *t*0.85;
		//*r = *r*0.9;
		//j = 0.9;
	}
	else if (fraction_a > 0.15) {
		*t = *t*0.85;
		//*r = *r*0.95;
		//j = 0.95;
	}
	else{
		*t = *t*0.85;
		//*r = *r*0.8;
		//j = 0.8;
	}
	//return j;
}

void annealing (struct cell_t** cells_array, double hot, double range, int iter) {

	int a, i, idA, idB, total_accepted, selected = 0, reps = 0, selected_reps = 0;
	double t, r, dc, dc_total = 0;
	t = hot;
	r = range;
	total_accepted = 0;
	time_t t1, t2, t3, t4=0, t5=0;
	while (t >.5) {
		reps++;
		dc = 0;
		a = 0;
		for(i = 0; i<iter; i++) {
			t1 = clock();
			selected = cell_select(cells_array, r, &idA, &idB);
			t2 = clock();
			if(selected==1){
				a += sa_move(cells_array, t, idA, idB, &dc);
				t3 = clock();
				t5 += t3 - t2;
				selected_reps++;
			}
			t4 += t2-t1;
		}
		total_accepted+=a;
		update_vars(a, &t, &r, iter);
		dc_total +=dc;
		//printf("Temp = %f, Range = %f, Accepted = %d, dc = %f \n", t, r, a, dc);
	}
	printf("Total accepted: %d, Reps: %d Time for cell_select: %ld, Time for sa_move: %ld * %d times\n", total_accepted, reps, t4, t5, selected_reps);
	//printf("\nTotal selected = %d Total accepted moves = %d, Total dc = %f\n", selected, total_accepted, dc_total);
}
