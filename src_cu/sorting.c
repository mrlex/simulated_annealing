#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "gordian.h"

int quicksort_partition (struct cell_t **a, int l, int r, char coordinate);

int partition(int *table, int lo, int hi) {
	int tmp;
	int pivot = table[lo];
	int i = lo - 1;
	int j = hi + 1;
	do {
		do {
			i++;
		} while (table[i] < pivot);
		do {
			j--;
		} while (table[j] > pivot);
		if (i >= j)
			return j;
		tmp = table[i];
		table[i] = table[j];
		table[j] = tmp;
	} while(true);
}

void quicksort_simple (int *table, int lo, int hi) {
	if (lo < hi) {
		int p = partition(table, lo, hi);
		quicksort_simple(table, lo, p);
		quicksort_simple(table, p+1, hi);
	}
}

int partition_double (int* table1, int* table2, int l, int r)
{
	int pivot = table1[l+(r-l)/2];
	int i = l - 1;
	int j = r + 1;
	do {
		do{
			i++;
		}while (table1[i] < pivot);
		do{
			j--;
		}while (table1[j] > pivot);
		if (i>=j) {
			return j;
		}
		int tmp = table1[i];
		table1[i] = table1[j];
		table1[j] = tmp;
		tmp = table2[i];
		table2[i] = table2[j];
		table2[j] = tmp;
	}while(true);
}

void quicksort_double_array (int* table1, int* table2, int l, int r)
{
	if (l < r){
		int p = partition_double(table1, table2, l, r);
		quicksort_double_array(table1, table2, l, p);
		quicksort_double_array(table1, table2, p+1, r);
	}
}
int quicksort_by_distance(struct cell_t **a, int l, int r)
{
    int i, j;
	double pivot;
	struct cell_t *t;

	pivot = a[l]->distance;

	i = l;
	j = r+1;

	while(1){
        do {
            ++i; 
        } while(i <= r && a[i]->distance <= pivot);
        do {
            --j; 
        } while(a[j]->distance > pivot );
		
        if (i >= j) {
            break;
        }
		t = a[i]; a[i] = a[j]; a[j] = t;
	}
	t = a[l]; a[l] = a[j]; a[j] = t;
	return j;
}

void new_quicksort(struct cell_t **a, int l, int r)
{
	int j;

	if (l < r) {
		// divide and conquer
		j = quicksort_by_distance (a, l, r);
        new_quicksort (a, l, j-1);
		new_quicksort (a, j+1, r);
    }
}

void quicksort (struct cell_t **a, int l, int r, char coordinate){

	int j;

	if(l < r){
		// divide and conquer
		j = quicksort_partition (a, l, r, coordinate);
		quicksort (a, l, j-1, coordinate);
		quicksort (a, j+1, r, coordinate);
		}
	}


int quicksort_partition (struct cell_t **a, int l, int r, char coordinate){

	int i, j;
	double pivot;
	struct cell_t *t;

	if(coordinate=='X') pivot = a[l]->centerX;
	else if(coordinate=='Y') pivot = a[l]->centerY;
	else{
		printf("\nERROR: invalid argument in quicksort_partition\n");
		exit(1);
		}

	i = l;
	j = r+1;

	if(coordinate=='X'){
		while(1){
			do ++i; while(i <= r && a[i]->centerX <= pivot);
			do --j; while(a[j]->centerX > pivot );
			if(i >= j) break;
			t = a[i]; a[i] = a[j]; a[j] = t;
			}
		t = a[l]; a[l] = a[j]; a[j] = t;
		return j;
		}

	else if(coordinate=='Y'){
		while(1){
			do ++i; while(i <= r && a[i]->centerY <= pivot);
			do --j; while(a[j]->centerY > pivot );
			if(i >= j) break;
			t = a[i]; a[i] = a[j]; a[j] = t;
			}
		t = a[l]; a[l] = a[j]; a[j] = t;
		return j;
		}
	else{
		printf("\nERROR: invalid argument in quicksort_partition\n");
		exit(1);
		}
	}

int quicksort_float (double *tbl, int l, int r)
{
    int i, j;
	double pivot;
    double t =0;

	pivot = tbl[l];

	i = l;
	j = r+1;

	while(1){
        do {
            ++i; 
        } while(i <= r && tbl[i] <= pivot);
        do {
            --j; 
        } while(tbl[j] > pivot );
		
        if (i >= j) {
            break;
        }
		t = tbl[i]; tbl[i] = tbl[j]; tbl[j] = t;
	}
	t = tbl[l]; tbl[l] = tbl[j]; tbl[j] = t;
	return j;

}

void quicksort_table(double *tbl, int l, int r) 
{
    int j;
    
    if ( l < r) {
        j = quicksort_float (tbl, l, r);
        quicksort_table (tbl, l, j-1);
        quicksort_table (tbl, j+1, r);
    }
}

void selection_sort (struct cell_t **a, int n, char coordinate){

	int i, j;
	struct cell_t *t;

	if(coordinate == 'X'){
		for(i=0; i<n-1; i++){
			for(j=i; j<n; j++){
				if(a[j]->centerX < a[i]->centerX){
					t = a[i]; a[i] = a[j]; a[j] = t;
					}
				}
			}
		}

	else if(coordinate == 'Y'){
		for(i=0; i<n-1; i++){
			for(j=i; j<n; j++){
				if(a[j]->centerY < a[i]->centerY){
					t = a[i]; a[i] = a[j]; a[j] = t;
					}
				}
			}
		}

	else{
		printf("\nERROR: invalid argument in selection_sort\n");
		exit(1);
		}
	}

