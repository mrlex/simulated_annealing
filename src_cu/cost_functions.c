#include "gordian.h"
#include "parsers.h"
#include "simulated_annealing.h"

int find_first_occurence(int *table, int N, int x) {
	int mid, low = 0, high = N-1;
	int result = -1;
	while (low <= high) {
		mid = (low+high)/2;
		if (x == table[mid])
		{
			result = mid;
			high = mid-1;
		}
		else if (x < table[mid])
			high = mid-1;
		else
			low = mid+1;
	}
	return result;
}

double total_cost(){
	double total_x, total_y;
	double out_x, out_y;
	int i, j, net_degree;
	total_x = total_y = 0;
	for (i = 0; i < netsCtr; i++){
		net_degree = nets[i].connectionsCtr-1;
		out_x = nets[i].connections[net_degree]->centerX;
		out_y = nets[i].connections[net_degree]->centerY;
		for (j = net_degree-1; j > -1; j--) {
			total_x += fabs(out_x - nets[i].connections[j]->centerX);
			total_y += fabs(out_y - nets[i].connections[j]->centerY);
		}
	}
	return total_x + total_y;
}

double total_cost_table() {
	double total_x=0, total_y=0;
	int i;
	for (i = 0; i < tupCtr; i++){
		total_x += fabs((id_table[nets_table_col[i]]->centerX - id_table[nets_table_row[i]]->centerX)*nets_table_value[i]);
		total_y += fabs((id_table[nets_table_col[i]]->centerY - id_table[nets_table_row[i]]->centerY)*nets_table_value[i]);
	}
	return total_x + total_y; 
}
double node_cost(int node, double n_centerX, double n_centerY) {
	double total_x = 0, total_y=0;
	int temp = node;
	int i = find_first_occurence(nets_table_row, tupCtr, temp);
	if (i == -1){
		printf("Node not found");
		exit(1);
	}
	while (nets_table_row[i] == node) {
		total_x += fabs((id_table[nets_table_col[i]]->centerX - n_centerX)*nets_table_value[i]);
		total_y += fabs((id_table[nets_table_col[i]]->centerY - n_centerY)*nets_table_value[i]);
		i++;
		if (i==tupCtr) {
			break;
		}
	}
	return total_x + total_y;
}

void output_check(struct cell_t** cells_array){
	int oob;
	double overlap;
	oob = overlap = 0;
	for(int i = 0; i < movablesPadCtr-1; i++) {
		if(((cells_array[i]->centerX-cells_array[i]->sizeX/2)<0)||((cells_array[i]->centerX+cells_array[i]->sizeX/2)>coreAreaWidth)){
			//printf("OOB: %d, x = %f, y = %f, sizeX = %f\n", i, cells_array[i]->centerX, cells_array[i]->centerY, cells_array[i]->sizeX);
			oob++;
		}
		if((cells_array[i]->centerX+cells_array[i]->sizeX/2 - (cells_array[i+1]->centerX-cells_array[i+1]->sizeX/2)>0.0001)&&(cells_array[i]->centerY ==cells_array[i+1]->centerY)) {
			printf("Overlap: %d, x1 = %f, size1 = %f, x2 = %f, size2 = %f\n", i, cells_array[i]->centerX, cells_array[i]->sizeX, cells_array[i+1]->centerX, cells_array[i+1]->sizeX);
			overlap+=cells_array[i]->centerX+cells_array[i]->sizeX/2 - (cells_array[i+1]->centerX-cells_array[i+1]->sizeX/2);
		}
		if(cells_array[i]->sizeX == 0) {
			//printf("%d, centerX = %f \n", i, cells_array[i]->centerX);
		}
	}
	printf("OOB = %d, Overlap = %f\n\n", oob, overlap);
}

/*
double total_cost_table() {

	double total_x, total_y;
	int i, j;
	total_x = total_y = 0;
	for (i = 0; i < cellsCtr; i++) {
		for(j = i; j < cellsCtr; j++) {
			if(nets_table[i+j*cellsCtr] != 0) {
				total_x += fabs(id_table[i]->centerX - id_table[j]->centerX)*nets_table[i+j*cellsCtr];
				total_y += fabs(id_table[i]->centerY - id_table[j]->centerY)*nets_table[i+j*cellsCtr];
			}
		}
	}
	return total_x+total_y;
}

double node_cost(int node, double n_centerX, double n_centerY) {
	double total_x, total_y, output;
	total_x = total_y = 0;
	int line = node*cellsCtr;
	for(int i = 0; i < cellsCtr; i++) {
		if(nets_table[i+line] != 0) {
			total_x += fabs(id_table[i]->centerX - n_centerX)*nets_table[i+line];
			total_y += fabs(id_table[i]->centerY - n_centerY)*nets_table[i+line];
			//printf("%d * %s: centerX = %f, centerY = %f\n", nets_table[i+line], id_table[i]->name, id_table[i]->centerX, id_table[i]->centerY);
		}
	}	
	output = total_x+total_y;
	//printf("%f\n\n",output);
	return output;
}*/