#ifndef _MY_HEADER_FILE_
#define _MY_HEADER_FILE_

double total_cost();

double total_cost_table();

double node_cost(int node, double n_centerX, double n_centerY);

void annealing (struct cell_t** cells_array, double hot, double range, int iter);

void output_check(struct cell_t** cells_array);



/*
void allocate_and_copy();

void allocate_back_to_host();

void free_cuda();
*/
#endif