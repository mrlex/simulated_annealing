#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
//#include <sys/time.h>
#include <malloc.h>
#include "parsers.h"
//#include <stdbool.h>
#include "gordian.h"
#include <time.h>
#include "simulated_annealing.h"
#include <cuda_runtime.h>
#include <helper_cuda.h>
#include <helper_functions.h>


#define PADDING_PERCENTAGE	(0.05)
__device__ void swap_int(int *p, int i, int j) {
	int temp;
	temp = p[i];
	p[i] = p[j];
	p[j] = temp;
}

__device__ void swap_double(double *p, int i, int j) {
	double temp;
	temp = p[i];
	p[i] = p[j];
	p[j] = temp;
}
void swap_int_h(int *p, int i, int j) {
	int temp;
	temp = p[i];
	p[i] = p[j];
	p[j] = temp;
}

void swap_double_h(double *p, int i, int j) {
	double temp;
	temp = p[i];
	p[i] = p[j];
	p[j] = temp;
}
void fisher_yates(int *T, int *T1, int n){
	int j,i;
	for(i = n-1; i > 0; i--){
		j = rand()/(RAND_MAX/i+1);
		swap_int_h(T, i, j);
		swap_int_h(T1, i, j);
	}
}

void allocate_and_copy(){
	int i;
	checkCudaErrors(cudaMallocHost(&movables_cells_location_h, movablesPadCtr*sizeof(int)));
	checkCudaErrors(cudaMallocHost(&centerY_h, cellsCtr*sizeof(double)));
	checkCudaErrors(cudaMallocHost(&centerX_h, cellsCtr*sizeof(double)));
	checkCudaErrors(cudaMallocHost(&sizeX_h, cellsCtr*sizeof(double)));
	checkCudaErrors(cudaMallocHost(&nets_table_row_h, tupCtr*sizeof(int)));
	checkCudaErrors(cudaMallocHost(&nets_table_col_h, tupCtr*sizeof(int)));
	checkCudaErrors(cudaMallocHost(&nets_table_value_h, tupCtr*sizeof(int)));
	checkCudaErrors(cudaMallocHost(&nets_table_start_h, cellsCtr*sizeof(int)));
	checkCudaErrors(cudaMallocHost(&cells_to_shuffle_h, movablesCtr*sizeof(int)));
	checkCudaErrors(cudaMallocHost(&cells_to_location_h, movablesCtr*sizeof(int)));

	checkCudaErrors(cudaMalloc(&cells_to_location_d, movablesCtr*sizeof(int)));
	checkCudaErrors(cudaMalloc(&cells_to_shuffle_d, movablesCtr*sizeof(int)));
	checkCudaErrors(cudaMalloc(&movables_cells_location_d, movablesPadCtr*sizeof(int)));
	checkCudaErrors(cudaMalloc(&centerY_d, cellsCtr*sizeof(double)));
	checkCudaErrors(cudaMalloc(&centerX_d, cellsCtr*sizeof(double)));
	checkCudaErrors(cudaMalloc(&sizeX_d, cellsCtr*sizeof(double)));
	checkCudaErrors(cudaMalloc(&nets_table_row_d, tupCtr*sizeof(int)));
	checkCudaErrors(cudaMalloc(&nets_table_col_d, tupCtr*sizeof(int)));
	checkCudaErrors(cudaMalloc(&nets_table_value_d, tupCtr*sizeof(int)));
	checkCudaErrors(cudaMalloc(&nets_table_start_d, cellsCtr*sizeof(int)));
	checkCudaErrors(cudaMalloc(&devStates, movablesCtr*sizeof(curandState)));
	for(i = 0; i < movablesPadCtr; i++){
		movables_cells_location_h[i] = movable_cells_list[i]->index;
	}

	for(i = 0; i < cellsCtr; i++){
		centerX_h[i] = id_table[i]->centerX;
		centerY_h[i] = id_table[i]->centerY;
		sizeX_h[i] = id_table[i]->sizeX;
		nets_table_start_h[i] = nets_table_start[i];
	}
	for(i = 0; i < tupCtr; i++){
		nets_table_row_h[i] = nets_table_row[i];
		nets_table_col_h[i] = nets_table_col[i];
		nets_table_value_h[i] = nets_table_value[i];
	}
	checkCudaErrors(cudaMemcpy(movables_cells_location_d, movables_cells_location_h, movablesPadCtr*sizeof(int), cudaMemcpyHostToDevice));
	checkCudaErrors(cudaMemcpy(centerY_d, centerY_h, cellsCtr*sizeof(double), cudaMemcpyHostToDevice));
	checkCudaErrors(cudaMemcpy(centerX_d, centerX_h, cellsCtr*sizeof(double), cudaMemcpyHostToDevice));
	checkCudaErrors(cudaMemcpy(sizeX_d, sizeX_h, cellsCtr*sizeof(double), cudaMemcpyHostToDevice));
	checkCudaErrors(cudaMemcpy(nets_table_row_d, nets_table_row_h, tupCtr*sizeof(int), cudaMemcpyHostToDevice));
	checkCudaErrors(cudaMemcpy(nets_table_col_d, nets_table_col_h, tupCtr*sizeof(int), cudaMemcpyHostToDevice));
	checkCudaErrors(cudaMemcpy(nets_table_value_d, nets_table_value_h, tupCtr*sizeof(int), cudaMemcpyHostToDevice));
	checkCudaErrors(cudaMemcpy(nets_table_start_d, nets_table_start_h, cellsCtr*sizeof(int), cudaMemcpyHostToDevice));
}

void allocate_back_to_host(){
	int i, j;
	checkCudaErrors(cudaMemcpy(movables_cells_location_h, movables_cells_location_d, movablesCtr*sizeof(int), cudaMemcpyDeviceToHost));
	checkCudaErrors(cudaMemcpy(centerY_h, centerY_d, cellsCtr*sizeof(double), cudaMemcpyDeviceToHost));
	checkCudaErrors(cudaMemcpy(centerX_h, centerX_d, cellsCtr*sizeof(double), cudaMemcpyDeviceToHost));
	checkCudaErrors(cudaMemcpy(sizeX_h, sizeX_d, cellsCtr*sizeof(double), cudaMemcpyDeviceToHost));
	checkCudaErrors(cudaMemcpy(nets_table_row_h, nets_table_row_d, tupCtr*sizeof(int), cudaMemcpyDeviceToHost));
	checkCudaErrors(cudaMemcpy(nets_table_col_h, nets_table_col_d, tupCtr*sizeof(int), cudaMemcpyDeviceToHost));
	checkCudaErrors(cudaMemcpy(nets_table_value_h, nets_table_value_d, tupCtr*sizeof(int), cudaMemcpyDeviceToHost));
	checkCudaErrors(cudaMemcpy(nets_table_start_h, nets_table_start_d, cellsCtr*sizeof(int), cudaMemcpyDeviceToHost));

	for (i = 0; i < cellsCtr; i++) {
		id_table[i]->centerX = centerX_h[i];
		id_table[i]->centerY = centerY_h[i];
		if (id_table[i]->sizeX != sizeX_h[i])
			printf("%f:%f\n", id_table[i]->sizeX, sizeX_h[i]);
		id_table[i]->sizeX = sizeX_h[i];
	}
	// for(i = 0; i < movablesPadCtr; i++){
	// 	j = 0;
	// 	while ((movable_cells_list[j]->index != movables_cells_location_h[i]) && (movable_cells_list[j]->tpf != 0)){
	// 		j++;
	// 	}
	// 	struct cell_t* tmp = movable_cells_list[j];
	// 	movable_cells_list[j] = movable_cells_list[i];
	// 	movable_cells_list[i] = tmp;
	// }
}

void free_cuda() {
	checkCudaErrors(cudaFreeHost(&movables_cells_location_h));
	checkCudaErrors(cudaFreeHost(&centerY_h));
	checkCudaErrors(cudaFreeHost(&centerX_h));
	checkCudaErrors(cudaFreeHost(&sizeX_h));
	checkCudaErrors(cudaFreeHost(&nets_table_row_h));
	checkCudaErrors(cudaFreeHost(&nets_table_col_h));
	checkCudaErrors(cudaFreeHost(&nets_table_value_h));
	checkCudaErrors(cudaFreeHost(&nets_table_start_h));

	checkCudaErrors(cudaFree(&movables_cells_location_d));
	checkCudaErrors(cudaFree(&centerY_d));
	checkCudaErrors(cudaFree(&centerX_d));
	checkCudaErrors(cudaFree(&sizeX_d));
	checkCudaErrors(cudaFree(&nets_table_row_d));
	checkCudaErrors(cudaFree(&nets_table_col_d));
	checkCudaErrors(cudaFree(&nets_table_value_d));
	checkCudaErrors(cudaFree(&nets_table_start_d));
}
int quicksort_partition (struct cell_t **a, int l, int r, char coordinate);

int partition(int *table, int lo, int hi) {
	int tmp;
	int pivot = table[lo];
	int i = lo - 1;
	int j = hi + 1;
	do {
		do {
			i++;
		} while (table[i] < pivot);
		do {
			j--;
		} while (table[j] > pivot);
		if (i >= j)
			return j;
		tmp = table[i];
		table[i] = table[j];
		table[j] = tmp;
	} while(true);
}

void quicksort_simple (int *table, int lo, int hi) {
	if (lo < hi) {
		int p = partition(table, lo, hi);
		quicksort_simple(table, lo, p);
		quicksort_simple(table, p+1, hi);
	}
}

int partition_double (int* table1, int* table2, int l, int r)
{
	int pivot = table1[l+(r-l)/2];
	int i = l - 1;
	int j = r + 1;
	do {
		do{
			i++;
		}while (table1[i] < pivot);
		do{
			j--;
		}while (table1[j] > pivot);
		if (i>=j) {
			return j;
		}
		int tmp = table1[i];
		table1[i] = table1[j];
		table1[j] = tmp;
		tmp = table2[i];
		table2[i] = table2[j];
		table2[j] = tmp;
	}while(true);
}

void quicksort_double_array (int* table1, int* table2, int l, int r)
{
	if (l < r){
		int p = partition_double(table1, table2, l, r);
		quicksort_double_array(table1, table2, l, p);
		quicksort_double_array(table1, table2, p+1, r);
	}
}
int quicksort_by_distance(struct cell_t **a, int l, int r)
{
    int i, j;
	double pivot;
	struct cell_t *t;

	pivot = a[l]->distance;

	i = l;
	j = r+1;

	while(1){
        do {
            ++i; 
        } while(i <= r && a[i]->distance <= pivot);
        do {
            --j; 
        } while(a[j]->distance > pivot );
		
        if (i >= j) {
            break;
        }
		t = a[i]; a[i] = a[j]; a[j] = t;
	}
	t = a[l]; a[l] = a[j]; a[j] = t;
	return j;
}

void new_quicksort(struct cell_t **a, int l, int r)
{
	int j;

	if (l < r) {
		// divide and conquer
		j = quicksort_by_distance (a, l, r);
        new_quicksort (a, l, j-1);
		new_quicksort (a, j+1, r);
    }
}

void quicksort (struct cell_t **a, int l, int r, char coordinate){

	int j;

	if(l < r){
		// divide and conquer
		j = quicksort_partition (a, l, r, coordinate);
		quicksort (a, l, j-1, coordinate);
		quicksort (a, j+1, r, coordinate);
		}
	}


int quicksort_partition (struct cell_t **a, int l, int r, char coordinate){

	int i, j;
	double pivot;
	struct cell_t *t;

	if(coordinate=='X') pivot = a[l]->centerX;
	else if(coordinate=='Y') pivot = a[l]->centerY;
	else{
		printf("\nERROR: invalid argument in quicksort_partition\n");
		exit(1);
		}

	i = l;
	j = r+1;

	if(coordinate=='X'){
		while(1){
			do ++i; while(i <= r && a[i]->centerX <= pivot);
			do --j; while(a[j]->centerX > pivot );
			if(i >= j) break;
			t = a[i]; a[i] = a[j]; a[j] = t;
			}
		t = a[l]; a[l] = a[j]; a[j] = t;
		return j;
		}

	else if(coordinate=='Y'){
		while(1){
			do ++i; while(i <= r && a[i]->centerY <= pivot);
			do --j; while(a[j]->centerY > pivot );
			if(i >= j) break;
			t = a[i]; a[i] = a[j]; a[j] = t;
			}
		t = a[l]; a[l] = a[j]; a[j] = t;
		return j;
		}
	else{
		printf("\nERROR: invalid argument in quicksort_partition\n");
		exit(1);
		}
	}

int quicksort_float (double *tbl, int l, int r)
{
    int i, j;
	double pivot;
    double t =0;

	pivot = tbl[l];

	i = l;
	j = r+1;

	while(1){
        do {
            ++i; 
        } while(i <= r && tbl[i] <= pivot);
        do {
            --j; 
        } while(tbl[j] > pivot );
		
        if (i >= j) {
            break;
        }
		t = tbl[i]; tbl[i] = tbl[j]; tbl[j] = t;
	}
	t = tbl[l]; tbl[l] = tbl[j]; tbl[j] = t;
	return j;

}

void quicksort_table(double *tbl, int l, int r) 
{
    int j;
    
    if ( l < r) {
        j = quicksort_float (tbl, l, r);
        quicksort_table (tbl, l, j-1);
        quicksort_table (tbl, j+1, r);
    }
}

void selection_sort (struct cell_t **a, int n, char coordinate){

	int i, j;
	struct cell_t *t;

	if(coordinate == 'X'){
		for(i=0; i<n-1; i++){
			for(j=i; j<n; j++){
				if(a[j]->centerX < a[i]->centerX){
					t = a[i]; a[i] = a[j]; a[j] = t;
					}
				}
			}
		}

	else if(coordinate == 'Y'){
		for(i=0; i<n-1; i++){
			for(j=i; j<n; j++){
				if(a[j]->centerY < a[i]->centerY){
					t = a[i]; a[i] = a[j]; a[j] = t;
					}
				}
			}
		}

	else{
		printf("\nERROR: invalid argument in selection_sort\n");
		exit(1);
		}
	}


/*
1: red
2: green
3: blue
4: purple
5: light blue
6: yellow
7: black
8: blue
9: orange
10: dark green
12: brown


// plots the whole core area to a .png image file 
void plot_area (char *filename, short plot_partitions){

	char cmd[1024];
	double pin_dimX;
	double pin_dimY;
	struct cell_t *cur_cell;
	struct partition_t *p = partitions;
	gnuplot_ctrl *h;	//a plotting handler used by gnuplot

	pin_dimX = 2 * coreAreaWidth / (cellsCtr-movablesCtr); 
	pin_dimY = 2 * coreAreaHeight / (cellsCtr-movablesCtr);
	#if (PRINTING==COMPACT || PRINTING==FULL)
	int last_print=-1;
	int print_ctr=0;
	#endif

	h = gnuplot_init();
	gnuplot_resetplot(h);

	#if (PRINTING==FULL)
	printf("\rPlotting %s  0%%", filename); fflush(stdout);
	//printf("\tPlotting..."); fflush(stdout);
	#elif (PRINTING==COMPACT)
	printf("\rPlotting %s\t[ 0%%]", filename); fflush(stdout);
	#endif
	//plot the cells
	cur_cell = cells_list;
	while(cur_cell!=NULL){
			
		//print filled rectangle for a cell
		//if(!cur_cell->terminal) sprintf(cmd, "set object rect from %lf,%lf to %lf,%lf fc rgb 'green' fs solid 1 border -1;", cur_cell->centerX - cur_cell->sizeX / 2, cur_cell->centerY - cur_cell->sizeY / 2, cur_cell->centerX + cur_cell->sizeX / 2, cur_cell->centerY + cur_cell->sizeY / 2);

		//print empty rectangle for a cell
		if(cur_cell->tpf==0) sprintf(cmd, "set object rect from %lf,%lf to %lf,%lf fc rgb 'blue' fs solid;", cur_cell->centerX - cur_cell->sizeX / 2, cur_cell->centerY - cur_cell->sizeY / 2, cur_cell->centerX + cur_cell->sizeX / 2, cur_cell->centerY + cur_cell->sizeY / 2);
		//print filled rectangle for a pin
		else if(cur_cell->tpf==0x4)sprintf(cmd, "set object rect from %lf,%lf to %lf,%lf fc rgb 'grey' fs solid;", cur_cell->centerX - pin_dimX / 2, cur_cell->centerY - pin_dimY / 2, cur_cell->centerX + pin_dimX / 2, cur_cell->centerY + pin_dimY / 2);
		//else if(cur_cell->tpf==0x1)sprintf(cmd, "set object rect from %lf,%lf to %lf,%lf fc rgb 'green' fs solid;", cur_cell->centerX - pin_dimX / 2, cur_cell->centerY - pin_dimY / 2, cur_cell->centerX + pin_dimX / 2, cur_cell->centerY + pin_dimY / 2);

		gnuplot_cmd(h, cmd);
		cur_cell = cur_cell->next;

		//just for printing the percentage of the completed plotting proccess
		#if (PRINTING==FULL)
		if((int)(print_ctr*100/cellsCtr) > last_print){
			last_print = (int)(print_ctr*100/cellsCtr);
			printf("\b\b\b%2d%%", last_print); fflush(stdout);
			}
		print_ctr++;
		#elif (PRINTING==COMPACT)
		if((int)(print_ctr*100/cellsCtr) > last_print){
			last_print = (int)(print_ctr*100/cellsCtr);
			printf("\rPlotting\t[%2d%%]", last_print); fflush(stdout);
			}
		print_ctr++;	
		#endif
		}
    for(int i = 0; i < tupCtr; i++) {
        sprintf(cmd, "set arrow from %lf,%lf, 0 to %lf,%lf, nohead lc rgb 'red'",id_table[nets_table_row[i]]->centerX, id_table[nets_table_row[i]]->centerY, id_table[nets_table_col[i]]->centerX, id_table[nets_table_col[i]]->centerY); 
    }
	//plot the partitioning lines
	if (plot_partitions){
		while(p != NULL){
			sprintf(cmd, "set object rect from %lf,%lf to %lf,%lf lw 0.5 fillstyle empty border 3;", p->minX, p->minY, p->maxX, p->maxY);
			gnuplot_cmd(h, cmd);
			p = p->next;
			}
		}

	//plot the core area borders
	sprintf(cmd, "set object rect from 0,0 to %lf,%lf lw 2.0 fillstyle empty border 1;", coreAreaWidth, coreAreaHeight);
	gnuplot_cmd(h, cmd);

	//make the plot to a pop-up window
	//gnuplot_cmd(h, "set term wxt persist");
	//gnuplot_cmd(h, "set terminal wxt title 'GORDIAN Placement';");

	//make the plot to a png image file
	sprintf(cmd, "set output '%s';", filename);
	gnuplot_cmd(h, cmd);
	gnuplot_cmd(h,"set terminal png size 1600,1200;" );


	//make the plot visible
	gnuplot_cmd(h, "set key off;");		//disable the legend
	sprintf(cmd, "set xrange [%lf:%lf]; set yrange [%lf:%lf];", coreAreaWidth * (PADDING_PERCENTAGE) * (-1.0), coreAreaWidth * (PADDING_PERCENTAGE + 1), coreAreaHeight * (PADDING_PERCENTAGE) * (-1.0), coreAreaHeight * (PADDING_PERCENTAGE + 1));
	gnuplot_cmd(h, cmd);
	gnuplot_cmd(h, "plot -100000;");

	#if (PRINTING==FULL)
	printf("\b\b\b\b... \t[OK]\n");
	#elif (PRINTING==COMPACT)
	printf("\rPlotting\t[OK] "); fflush(stdout);
	#endif

	gnuplot_close(h);
	}
*/






/* Legalizes the cells. First moves the cells so they all fit inside rows. Then moves the cells so they do not overlap. */
void legalize (struct cell_t **cells_array){

    int i, j, k;
    double pos_y, pos_x;        //used in final placement
    int numrows;            //the count of rows in the core area
	//    int *row_count;            //the count of cells inside every row
    double *row_len;        //the total length of cells inside every row
    int sweeps=0;            //number of sweeps needed in final placement
    int done;            //indicates the completion of the final placement
    int offset;            //distance in rows, where the extra cells are attempted to be moved to
    #if (PRINTING==COMPACT)
    //int last_printed_percentage=0;    //used for printing
    #endif

    #if (PRINTING==FULL)
    printf("\nLegalizing..."); fflush(stdout);
    #elif (PRINTING==COMPACT)
    printf("\rLegalizing\t[ 0%%]"); fflush(stdout);
    #endif

    //sort the cells (ascending y-coordinate order)
    quicksort (cells_array, 0, movablesCtr-1, 'Y');

    numrows = (int)(coreAreaHeight/stdCellHeight);
    pos_y = stdCellHeight / 2;
    pos_x = 0;

    //allocate memory for the row_count and row_len vectors
    row_count = (int*) malloc (numrows * sizeof(int));
    if (row_count == NULL) {
        printf("Cannot allocate memory: row_count %ld\n", numrows * sizeof(int));
        exit(1);
    }
    row_len = (double*) malloc (numrows * sizeof(double));
    if (row_len == NULL) {
        printf("Cannot allocate memory: row_len %ld\n", numrows * sizeof(double));
        exit(1);
    }

    for (i=0; i<numrows; i++){
        row_count[i] = 0;
        row_len[i] = 0.0;
        }

    i=j=0;
	//int *empty_order;
	//empty_order = (int *)malloc(numrows*sizeof(int));
    //calculate the cell count and row length of every row    
    do{    
        if (cells_array[j]->centerY <= pos_y + stdCellHeight/2 && ( cells_array[j]->centerY > pos_y - stdCellHeight/2 || cells_array[j]->centerY == 0)){
            row_count[i]++;
            /*if (row_len[i] += cells_array[j]->sizeX > coreAreaWidth){
				cells_array[j]->centerY += stdCellHeight;
			}*/
            row_len[i] += cells_array[j]->sizeX;
            j++;
            }
        else {
            pos_y += stdCellHeight;
            i++;
            }
    } while (j<movablesCtr);
    done = 1;

	    
    do {
    offset=1;
    do {
        sweeps++;
        j=0;
        done=1;

        for (i=0; i<numrows; i++){

            if (row_len[i] > coreAreaWidth){
                int move_cell;
                int move_direction=0;                

                //see if the extra cell can be moved 'offset' rows above or below
                if ((i>=offset && i<numrows-offset && row_len[i-offset] >= row_len[i+offset] && row_len[i] > row_len[i+offset] + cells_array[j+row_count[i]-1]->sizeX) ){ //|| i<offset){
                    move_cell = j+row_count[i]-1;
                    move_direction = 1;
                    }
                else if ((i>=offset && i<numrows-offset && row_len[i-offset] < row_len[i+offset] && row_len[i] > row_len[i-offset] + cells_array[j]->sizeX) ){ //|| i>=numrows-offset){
                    move_cell = j;
                    move_direction = -1;
                    }
                else if ((i < offset && row_len[i] > row_len[i+offset] + cells_array[j+row_count[i]-1]->sizeX)){
					move_cell = j+row_count[i]-1;
					move_direction = 1;
				}
				else if (i>=numrows-offset && row_len[i] > row_len[i-offset] + cells_array[j]->sizeX) {
                    move_cell = j;
                    move_direction = -1;
                    }
                    
                //move the cell
                if (move_direction != 0){
                    int move_to=j;

                    if (offset==1) cells_array[move_cell]->centerY = cells_array[move_cell+move_direction]->centerY;
                    else {
                        if (move_direction > 0) for (k=0; k<offset; k++) move_to += row_count[i+k];
                        else {
                            move_to--;
                            for (k=1; k<offset; k++) move_to -= row_count[i-k];
                            }

                        cells_array[move_cell]->centerY = cells_array[move_to]->centerY;
                        }

                    row_count[i]--;
                    row_count[i+move_direction*offset]++;
                    row_len[i] -= cells_array[move_cell]->sizeX;
                    row_len[i+move_direction*offset] += cells_array[move_cell]->sizeX;
                    done=0;

                    //restore the cells' sorting
                    if (offset>1) {
                        if (move_direction > 0) quicksort (cells_array, move_cell, move_to, 'Y');
                        else quicksort (cells_array, move_to, move_cell, 'Y');
                        }
                    }

                if (move_direction < 0) j++;

                //if (move_direction != 0 && offset > 1) break;
                }
            j += row_count[i];
            }

        if (done) offset++;
        else offset=1;

        } while (!(done && offset==numrows));
		done = 0;
		for(i =0; i< numrows; i++) {
			if (row_len[i] > coreAreaWidth) {
				done = 1;
				printf("\n *********BOOM********* \n");
			}
		}
	} while (done == 1);
		
    #if (PRINTING==COMPACT)
    printf("\rLegalizing\t[50%%]"); fflush(stdout);
    #endif

    j=0;
    pos_y = stdCellHeight / 2;

    //legalize and spread cells inside every row
    for (i=0; i<numrows; i++){

        /*#if (PRINTING==COMPACT)
        if((int)(j*100/movablesCtr) > last_printed_percentage){
            last_printed_percentage = (int) (j*100/movablesCtr);
            printf("\rLegalizing\t[%2d%%]", last_printed_percentage); fflush(stdout);
            }
        #endif*/

        //sort the cells in the i-th row (ascending x-coordinate order)
        quicksort (cells_array, j, j+row_count[i]-1, 'X');

        //pos_x = (coreAreaWidth - row_len[i]) / 2;
		pos_x = 0;
		double spacing;
        for (k=j; k<j+row_count[i]; k++) {
			spacing = (coreAreaWidth - row_len[i])/(row_count[i]-1);
            cells_array[k]->centerY = pos_y;
            cells_array[k]->centerX = pos_x + cells_array[k]->sizeX / 2;
            pos_x += cells_array[k]->sizeX + spacing;
            }
            
        pos_y += stdCellHeight;
        j += row_count[i];
        }

    #if (PRINTING==FULL)
    printf("\b\b\b\b\bin %d sweeps\n", sweeps);
    #elif (PRINTING==COMPACT)
    printf("\rLegalizing\t[OK] \n"); fflush(stdout);
    #endif
}




////////////////

#include <stdio.h>
#include <stdlib.h>
#include "gordian.h"
#include <stdbool.h>

//////////////////////////
typedef struct sorted
{
    struct cell_t *next_cell;
    struct sorted *next;
}sort;

typedef struct rows
{
    double row_max;
    double filled;
}row;

sort *curr_sort, *pre_sort, *temp_sort,*curr2_sort, *pre2_sort, *root_sort;

int find_first_occurence(int *table, int N, int x) {
	int mid, low = 0, high = N-1;
	int result = -1;
	while (low <= high) {
		mid = (low+high)/2;
		if (x == table[mid])
		{
			result = mid;
			high = mid-1;
		}
		else if (x < table[mid])
			high = mid-1;
		else
			low = mid+1;
	}
	return result;
}

double total_cost(){
	double total_x, total_y;
	double out_x, out_y;
	int i, j, net_degree;
	total_x = total_y = 0;
	for (i = 0; i < netsCtr; i++){
		net_degree = nets[i].connectionsCtr-1;
		out_x = nets[i].connections[net_degree]->centerX;
		out_y = nets[i].connections[net_degree]->centerY;
		for (j = net_degree-1; j > -1; j--) {
			total_x += fabs(out_x - nets[i].connections[j]->centerX);
			total_y += fabs(out_y - nets[i].connections[j]->centerY);
		}
	}
	return total_x + total_y;
}

double total_cost_table() {
	double total_x=0, total_y=0;
	int i;
	for (i = 0; i < tupCtr; i++){
		total_x += fabs((id_table[nets_table_col[i]]->centerX - id_table[nets_table_row[i]]->centerX)*nets_table_value[i]);
		total_y += fabs((id_table[nets_table_col[i]]->centerY - id_table[nets_table_row[i]]->centerY)*nets_table_value[i]);
	}
	return total_x + total_y; 
}
double node_cost(int node, double n_centerX, double n_centerY) {
	double total_x = 0, total_y=0;
	int temp = node;
	int i = find_first_occurence(nets_table_row, tupCtr, temp);
	if (i == -1){
		printf("Node not found");
		exit(1);
	}
	while (nets_table_row[i] == node) {
		total_x += fabs((id_table[nets_table_col[i]]->centerX - n_centerX)*nets_table_value[i]);
		total_y += fabs((id_table[nets_table_col[i]]->centerY - n_centerY)*nets_table_value[i]);
		i++;
		if (i==tupCtr) {
			break;
		}
	}
	return total_x + total_y;
}

void output_check(struct cell_t** cells_array){
	int oob, olap;
	double overlap;
	olap = oob = overlap = 0;
	int j =0;
	for(int i = 0; i < movablesPadCtr-1; i++) {
		if(((cells_array[i]->centerX-cells_array[i]->sizeX/2)<0)||((cells_array[i]->centerX+cells_array[i]->sizeX/2)>coreAreaWidth)){
			//printf("OOB: %d, x = %f, y = %f, sizeX = %f\n", i, cells_array[i]->centerX, cells_array[i]->centerY, cells_array[i]->sizeX);
			oob++;
		}
		if((cells_array[i]->centerY != cells_array[i+1]->centerY) && (cells_array[i]->sizeX != 0)){
			j++;
		}
		if((cells_array[i]->centerX+cells_array[i]->sizeX/2 - (cells_array[i+1]->centerX-cells_array[i+1]->sizeX/2)>0.0001)&&(cells_array[i]->centerY ==cells_array[i+1]->centerY)) {
			printf("Overlap:%d %d:%d, %f:%f x1 = %f, size1 = %f, x2 = %f, size2 = %f\n", i, cells_array[i]->index, cells_array[i+1]->index, cells_array[i]->centerY, cells_array[i+1]->centerY, cells_array[i]->centerX, cells_array[i]->sizeX, cells_array[i+1]->centerX, cells_array[i+1]->sizeX);
			overlap+=cells_array[i]->centerX+cells_array[i]->sizeX/2 - (cells_array[i+1]->centerX-cells_array[i+1]->sizeX/2);
			olap ++;
		}
		
		// if(cells_array[i]->sizeX == 0) {
		// 	//printf("%d, centerX = %f \n", i, cells_array[i]->centerX);
		// }
	}
	printf("OOB = %d, Overlap = %d, %f\n\n", oob, olap, overlap);
	printf("%d\n", j);
}

int divide(struct cell_t** cells_array, double x, double y) {
	int j, range, i = movablesPadCtr/2;
	range = movablesPadCtr/4;
	j = 0;
	y = y - fmod(y,stdCellHeight) + stdCellHeight/2;
	while (j == 0) {
		/*if (i == 0){
			if ((x > cells_array[i]->centerX - cells_array[i]->sizeX/2) && (x< cells_array[i]->centerX + cells_array[i]->sizeX/2)) {
				j = 1;
			}
			else {
				i = -1;
				j = 1;
			}
		}
		else if (i == movablesPadCtr-1){
			if (x > cells_array[i]->centerX - cells_array[i]->sizeX/2 && x< cells_array[i]->centerX + cells_array[i]->sizeX/2)
				j = 1;
			else {
				i = -1;
				j = 1;
			}
		}*/
		if(i == 0||i == movablesPadCtr-1){
			i =-1;
			break;
		}
		if (fabs(cells_array[i]->centerY - y)<0.001) {
			if(x < cells_array[i]->centerX - cells_array[i]->sizeX/2) {
				if((y == cells_array[i-1]->centerY) && (x < cells_array[i-1]->centerX + cells_array[i-1]->sizeX/2)){
					i = i - range;
				}
				else {
					i = -1;
					j = 1;
				}
			}
			else if(x > cells_array[i]->centerX + cells_array[i]->sizeX/2){
				if((y == cells_array[i+1]->centerY) && (x > cells_array[i+1]->centerX - cells_array[i+1]->sizeX/2)){
					i = i + range;
				}
				else {
					i = -1;
					j = 1;
				}
			}
			else {
				j = 1;
			}
		}
		else if (y < cells_array[i]->centerY){
			i = i - range;
		}
		else if (y > cells_array[i]->centerY){
			i = i + range;
		}
		range = range/2;
		if(range == 0)
			range =1;
	}
	
	return i;
}

int cell_select(struct cell_t** cells_array, double range, int* idA, int* idB) {
	int i, j, accept;
	//double x, y;
	//time_t t;
	i = accept = 0;
	do {
		j = 0;
		do {
			*idA = (int)rand()/(RAND_MAX/(movablesPadCtr)+1);							//1
		}while(cells_array[*idA]->sizeX==0);
		do {

			do {
				*idB = (int)rand()/(RAND_MAX/(movablesPadCtr)+1);							//1
			}while(cells_array[*idB]->sizeX==0 || *idA==*idB);
		
			/*if(*idA == *idB) {
				if(*idB>1) *idB-=1;
				else *idB = 2;
			}*/
			if(cells_array[*idA]->sizeX - (cells_array[*idB+1]->centerX-cells_array[*idB+1]->sizeX/2) + (cells_array[*idB-1]->centerX + cells_array[*idB-1]->sizeX/2) < 0.000001){
				if(cells_array[*idB]->sizeX - (cells_array[*idA+1]->centerX-cells_array[*idA+1]->sizeX/2) + (cells_array[*idA-1]->centerX + cells_array[*idA-1]->sizeX/2)< 0.000001){
					accept = 1;
				}
			}
			if (*idB==*idA) {
				accept = 0;
			}
			j++;
			//printf("\ncell_select i = %d, j = %d, idA = %d, idB = %d", i, j, idA, idB);
		}while((j < 40 && accept == 0));	
		i++;		
	}while(i < 25 && accept == 0);	/*
	if(accept&&range<10) {
		printf("\n%d, %d, %d", i,j,accept);
		printf("\nidA = %d, centerX = %f, centerY = %f, sizeX = %f", *idA, cells_array[*idA]->centerX, cells_array[*idA]->centerY, cells_array[*idB]->sizeX);
		printf("\nidB = %d, centerX = %f, centerY = %f, sizeX = %f", *idB, cells_array[*idB]->centerX, cells_array[*idB]->centerY, cells_array[*idB]->sizeX);
	}*/
	return accept;
}
int random_accept(double c, double t){	
	return (rand()/(RAND_MAX/100000) < (exp(-c/t)*100000));
}

int sa_move(struct cell_t** cells_array, double t, int idA, int idB, double* dc){
	double cost1, cost2, tmpY;
	int accept = 0;
	//time_t t1,t2;
	//t1=clock();
	cost1 = node_cost(cells_array[idA]->index, cells_array[idA]->centerX, cells_array[idA]->centerY) + node_cost(cells_array[idB]->index, cells_array[idB]->centerX, cells_array[idB]->centerY);
	cost2 = node_cost(cells_array[idB]->index, cells_array[idA]->centerX, cells_array[idA]->centerY) + node_cost(cells_array[idA]->index, cells_array[idB]->centerX, cells_array[idB]->centerY);
	//t2 = clock();
	//printf("%ld\t", t2-t1);
	//printf("1 = %f, 2 = %f\n", node_cost(idA, cells_array[idA]->centerX, cells_array[idA]->centerY), node_cost(idB, cells_array[idB]->centerX, cells_array[idB]->centerY));
	//printf("1 = %d, 2 = %d\n", idA, idB);
	if(cost2-cost1<0.0001){
		accept = 1;
	}
	else{
		accept = random_accept(cost2-cost1, t);
	}
	//printf("idA: %d, idB: %d cost diff:\t%f, accept: %d\n", idA, idB, cost2-cost1, accept);
	if(accept == 1) {
		//t1 = clock();
		struct cell_t* tmpcell;
		//tmpX = cells_array[idA]->centerX;
		tmpY = cells_array[idA]->centerY;
		cells_array[idA]->centerX = cells_array[idB-1]->centerX/2 + cells_array[idB-1]->sizeX/4 + cells_array[idB+1]->centerX/2 - cells_array[idB+1]->sizeX/4;
		cells_array[idA]->centerY = cells_array[idB]->centerY;
		cells_array[idB]->centerX = cells_array[idA-1]->centerX/2 + cells_array[idA-1]->sizeX/4 + cells_array[idA+1]->centerX/2 - cells_array[idA+1]->sizeX/4;
		cells_array[idB]->centerY = tmpY;
		tmpcell = cells_array[idA];
		cells_array[idA] = cells_array[idB];
		cells_array[idB] = tmpcell;

		//cost3 = node_cost(cells_array[idA]->index, cells_array[idA]->centerX, cells_array[idA]->centerY) + node_cost(cells_array[idB]->index, cells_array[idB]->centerX, cells_array[idB]->centerY);
		//printf("cost3 = %f, cost2-1 = %f, cost2 = %f, cost1 = %f\n", cost3, cost2-cost1, cost2, cost1);
		*dc += cost2-cost1;
		//t2 = clock();
		//printf("%ld\t", t2-t1);
	}
	//printf("\n");
	return accept;
}
	
void update_vars(int a, double* t, double* r, int iter) {
	
	double fraction_a = (double)a/iter;
	if (fraction_a > 0.90) {
		*t = *t*0.5;
		//*r = *r*0.8;
		//j = 0.5;
	}
	else if (fraction_a > 0.8) {
		*t = *t*0.9;
		//*r = *r*0.9;
		//j = 0.9;
	}
	else if (fraction_a > 0.15) {
		*t = *t*0.9;
		//*r = *r*0.95;
		//j = 0.95;
	}
	else{
		*t = *t*0.7;
		//*r = *r*0.8;
		//j = 0.8;
	}
	//return j;
}

void annealing (struct cell_t** cells_array, double hot, double range, int iter) {

	int a, i, idA, idB, selected = 0;
	//int total_accepted, reps = 0,  selected_reps = 0
	double t, r, dc, dc_total = 0;
	t = hot;
	r = range;
	//total_accepted = 0;
	time_t t1, t2, t4;
	//time_t t3, t4=0, t5=0;
	t1=clock();
	while (t >.5) {
		//reps++;
		dc = 0;
		a = 0;
		for(i = 0; i<iter; i++) {
			//t1 = clock();
			selected = cell_select(cells_array, r, &idA, &idB);
			//t2 = clock();
			if(selected==1){
				a += sa_move(cells_array, t, idA, idB, &dc);
				//t3 = clock();
				//t5 += t3 - t2;
				//selected_reps++;
			}
			//t4 += t2-t1;
		}
		//total_accepted+=a;
		update_vars(a, &t, &r, iter);
		dc_total +=dc;
		//printf("Temp = %f, Range = %f, Accepted = %d, dc = %f \n", t, r, a, dc);
	}
	t2=clock();
	t4 = t2-t1;
	//printf("Total accepted: %d, Reps:  %d, Time for cell_select: %ld, Time for sa_move: %ld * %d times\n", total_accepted, reps, t4, t5, selected_reps);
	//printf("\nTotal selected = %d Total accepted moves = %d, Total dc = %f\n", selected, total_accepted, dc_total);
	printf("Time: %ld \n", t4*1000/ CLOCKS_PER_SEC);
}


#ifndef PRINTING
#error "Please define macro PRINTING and recompile"
#endif

double grid_min;
double avg_area;


int cell_hash_function (char *name)
{
    int i, sum;

    sum = 0;
    for(i=0; i<strlen(name) && i<MAX_STRING_LENGTH; i++) {
        sum = (sum*7 + name[i]) % CELLS_HASH_MODULO;
    }
    return sum;
}


/* adds a cell to the hash table */
struct cell_t* cell_add (char *name)
{
    int pos;
    struct cell_t *new_c;

    pos = cell_hash_function (name);

    new_c = (struct cell_t*) malloc (sizeof(struct cell_t));
    if (new_c == NULL) {
        printf("Cannot allocate memory: cell_add %ld\n", sizeof(struct cell_t));
        exit(1);
    }

    strcpy(new_c->name, name);
    new_c->next = cells[pos];
    cells[pos] = new_c;
    cellsCtr++;

    return new_c;
}


/* find a cell inside the hash table and returns pointer to cell */
struct cell_t* cell_find (char *name)
{
    struct cell_t *i;

    i = cells[cell_hash_function(name)];

    while (i!=NULL) {
        if(!strcmp(i->name, name)) {
            return i;
        }
        i = i->next;
    }

    return NULL;
}


/* converts the hash table containing the cells, to a linked list */
void hash_to_list ()
{
    int i;
    struct cell_t *cur_cell, *last_cell=NULL; 

    //find the first cell in the hash table
    cells_list = cells[0];
    i=0;
    
    while(cells_list==NULL && i<CELLS_HASH_MODULO) {
        i++;
        cells_list = cells[i];
    }

    //link the last cell of every hash table row with the first cell of the next row
    cur_cell = cells[i];
    while(cur_cell!=NULL) {
        last_cell = cur_cell;
        cur_cell = cur_cell->next;
    }

    for(i=i+1; i<CELLS_HASH_MODULO; i++){
        cur_cell = cells[i];
        //make the link
        if(cur_cell==NULL) 
            continue;
        else 
            last_cell->next = cur_cell;

        //find the last cell of the hash table row
        while(cur_cell!=NULL) {
            last_cell = cur_cell;
            cur_cell = cur_cell->next;
        }
    }
}

char* random_name(char *name) {
	int key;
    static char charset[] = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";      
	name[0] = 'e';
	for(int i = 1; i<9; i++){
		key = rand()%(int)(sizeof(charset)-1);
		name[i] = charset[key];
	}
	name[9] = '\0';
	return name;
}
void padding(struct cell_t** cells_array) {
	
	int i;
	char *name;
	  
	//time_t t;
	name = (char *)malloc(10*sizeof(char));
    if (name == NULL) {
        printf("Cannot allocate memory: padding %ld\n", 10*sizeof(char));
        exit(1);
    }
	struct cell_t *empty;
	int empty_rem = 2*coreAreaHeight/stdCellHeight-1;
	movablesPadCtr = movablesCtr+empty_rem+1;
	empty = cell_add(random_name(name));
	cellsCtr--;
	NULL_CHECK(empty);
	empty->centerX = coreAreaWidth;
	empty->centerY = coreAreaHeight - stdCellHeight/2;
	empty->sizeX = 0;
	empty->sizeY = stdCellHeight;
	empty->tpf = 1;
	empty->index = -1;
	
	//cells_tmp = (struct cell_t**) malloc ((movablesCtr + empty_rem + 1)*sizeof(struct cell_t*));
	//cells_array = (struct cell_t**) realloc(cells_array, (movablesCtr + empty_rem + 1)*sizeof(struct cell_t*));
	cells_array[movablesCtr+empty_rem] = empty;
	cells_array[movablesCtr+empty_rem-1] = cells_array[movablesCtr-1];
	for(i = movablesCtr-2; i >= 0; i--) {
		if(cells_array[i+empty_rem+1]->centerY>cells_array[i]->centerY) {
			empty = cell_add(random_name(name));
			NULL_CHECK(empty);
			empty->centerX = 0;
			empty->centerY = cells_array[i+empty_rem+1]->centerY;
			empty->sizeX = 0;
			empty->sizeY = stdCellHeight;
			empty->tpf = 1;
			empty->index = -1;
			cells_array[i+empty_rem] = empty;
			empty = cell_add(random_name(name));
			NULL_CHECK(empty);
			empty->centerX = coreAreaWidth;
			empty->centerY = cells_array[i]->centerY;
			empty->sizeX = 0;
			empty->sizeY = stdCellHeight;
			empty->tpf = 1;
			empty->index = -1;
			cells_array[i+empty_rem-1] = empty;
			empty_rem -=2;
			cellsCtr-=2;
		}
		cells_array[i+empty_rem] = cells_array[i];		
		//printf("%d:(%d) %s\n", i, empty_rem, cells_array[i+empty_rem]->name);
	}
	//printf("empty_rem = %d cells_array->name = %s\n", empty_rem, cells_array[0]->name);
	empty = cell_add(random_name(name));
	cellsCtr--;
	NULL_CHECK(empty);
	empty->centerX = 0;
	empty->centerY = 0;
	empty->sizeX = 0;
	empty->sizeY = stdCellHeight;
	empty->tpf = 1;
	empty->index = -1;
	cells_array[0] = empty;
//	free(cells_array);
//	cells_array = cells_tmp;
}


/* parses the necessary files in bookshelf format */
int parse_bookshelf (char *path, char *aux){

    int i, nodes_count, terminals_count, nets_count, netpins_count, rows_count, res;
    char filename[MAX_FILENAME_SIZE], tmp[16], line[1024];
    char file_aux[MAX_FILENAME_SIZE];
    char file_nets[MAX_FILENAME_SIZE];
    char file_nodes[MAX_FILENAME_SIZE];
    char file_pl[MAX_FILENAME_SIZE];
    char file_scl[MAX_FILENAME_SIZE];
    FILE *f;
    struct cell_t *cur_cell;

    #if (PRINTING==COMPACT)
    int last_print=-1;
    #endif

    #if (PRINTING==FULL)
    printf("Parsing...     "); fflush(stdout);
    #elif (PRINTING==COMPACT)
    printf("Parsing \t[ 0%%]"); fflush(stdout);
    #endif
    
    
    //initialize the hash table
    for(i=0; i<CELLS_HASH_MODULO; i++) 
        cells[i] = NULL;
	
    strcpy(file_aux, path);
    strcat(file_aux, "/");
    strcat(file_aux, aux);

    //open the auxiliary file
    f = fopen(file_aux, "r");
    NULL_CHECK(f);

    fscanf(f, "%*[^:]%*[:]");

    //read the filenames from the auxiliary file
    while (fscanf(f, "%s", filename) == 1) {

        if (sscanf(filename, "%*[^.]%*[.]%s", tmp)==1) {

            if(!strcmp(tmp, "nodes")) {
                strcpy(file_nodes, path);
                                strcat(file_nodes, "/");
                strcat(file_nodes, filename);
            }

            else if(!strcmp(tmp, "nets")) {
                strcpy(file_nets, path);
                                strcat(file_nets, "/");
                strcat(file_nets, filename);
            }

            else if(!strcmp(tmp, "pl")) {
                strcpy(file_pl, path);
                                strcat(file_pl, "/");
                strcat(file_pl, filename);
            }
            else if(!strcmp(tmp, "scl")) {
                strcpy(file_scl, path);
                                strcat(file_scl, "/");
                strcat(file_scl, filename);
            }
        }
    }

    fclose(f);


    //open the nodes file
    f = fopen(file_nodes, "r");
    NULL_CHECK(f);

    //ignore the first line containing the format name and revision
    fgets(line, sizeof(line), f);

    //ignore the commented lines with info about the origin of the file
    do {
        fgets(line, sizeof(line), f);
    } while(line[0]=='#');

    //parse the number of nodes and number of terminals
    if(!fscanf(f, "%*[^:]%*[:]%d", &nodes_count)){
        printf("\nERROR: nodes file does not contain number of nodes\n");
        exit(1);
        }

    if(!fscanf(f, "%*[^:]%*[:]%d", &terminals_count)){
        printf("\nERROR: nodes file does not contain number of terminals\n");
        exit(1);
        }
    
	id_table = (struct cell_t**) malloc ((nodes_count) * sizeof(struct cell_t*));
    if (id_table == NULL) {
        printf("Cannot allocate memory: id_table %ld\n", (nodes_count) * sizeof(struct cell_t*));
        exit(1);
    }
    movablesCtr = cellsCtr = i = 0;
    int terminalStart = nodes_count-terminals_count;
    double min_x = INFINITY;
    double total_cell_area =0;
    while (cellsCtr < nodes_count) {
        double sizeX, sizeY;
        char name[MAX_STRING_LENGTH];
        tmp[0]='\0';
        res = fscanf(f, "%s%lf%lf%*[ \t]%[^ \t\n]%*[ \t\n]", name, &sizeX, &sizeY, tmp);

        if (res < 3 || res > 4 || (res == 4 && strncasecmp(tmp, "terminal", 8))) {
            printf("\nERROR: in nodes file while parsing cell number: %d\n", cellsCtr);
            exit(1);
        }

        cur_cell = cell_add (name);
        NULL_CHECK(cur_cell);

        cur_cell->sizeX = sizeX;
        cur_cell->sizeY = sizeY;
        //cur_cell->area = sizeX * sizeY;
        //cur_cell->clique_weight = 0.0;

        if (res == 3) {
            cur_cell->tpf = 0;
            //cur_cell->level = -1;
            cur_cell->index = movablesCtr;
            if (cur_cell->sizeX < min_x) {
                min_x = cur_cell->sizeX;
            }    
            id_table[movablesCtr] = cur_cell;
            movablesCtr++;
        }
        else if (res == 4) {
            cur_cell->tpf = 0x4;
            //cur_cell->level = 0;
            cur_cell->index = i;
            id_table[terminalStart+i] = cur_cell;
            i++;    //counts the terminals
        }

        #if (PRINTING==COMPACT)
        if ((int)(cellsCtr*33/nodes_count) > last_print) {
            last_print = (int)(cellsCtr*33/nodes_count);
            printf("\b\b\b\b\b[%2d%%]", last_print); fflush(stdout);
        }
        #endif
        total_cell_area += cur_cell->sizeX * sizeY;
    }
    avg_area = total_cell_area/cellsCtr;
    grid_min = min_x / 10.0;
    fclose(f);

    if (i != terminals_count) {
        printf("\nERROR: Terminal count mismatch\n");
        exit(1);
    }

    if (terminals_count<=0) {
        printf("\nERROR: No fixed terminals found\n");
        exit(1);
    }

    //open the pl file
    f = fopen(file_pl, "r");
    NULL_CHECK(f);

    //ignore the first line containing the format name and revision
    fgets(line, sizeof(line), f);

    //ignore the commented lines with info about the origin of the file
    do {
        fgets(line, sizeof(line), f);
    } while(line[0]=='#');

    i = 0;
    while (i < cellsCtr) {
        double centerX, centerY;
        char name[MAX_STRING_LENGTH];

        tmp[0] = line[0] = '\0';
        res = fscanf(f, "%s%lf%lf%*[ \t]%*[:]%*[ \t]%[^ \t\n]%*[ \t]%[^ \t\n]%*[ \t\n]", name, &centerX, &centerY, tmp, line);

        cur_cell = cell_find (name);
        if (cur_cell == NULL) {
            printf("\nERROR: Cell %s in pl file was not found in nodes file\n", name);
            exit(1);
        }

        cur_cell->centerX = centerX;
        cur_cell->centerY = centerY;
        if (res>=4) {
            //strcpy(cur_cell->orientation, tmp);
        }
        else { 
            //strcpy(cur_cell->orientation, "-");
        }
        if(!strncasecmp(line, "/fixed", 6)){
            cur_cell->tpf = cur_cell->tpf|0x1;
            if(!(cur_cell->tpf&0x4)){
                printf("\nWarning: Cell %s in pl file is fixed.\n", name);
                //exit(1);
                }
            }/*
        else{
            cur_cell->tpf = cur_cell->tpf;
            if(cur_cell->terminal){
                printf("\nERROR: Cell %s in pl file is a terminal but is not declared fixed\n", name);
                exit(1);
                }
            }*/

        #if (PRINTING==COMPACT)
        if((int)(i*33/cellsCtr)+33 > last_print){
            last_print = (int)(i*33/cellsCtr)+33;
            printf("\b\b\b\b\b[%2d%%]", last_print); fflush(stdout);
            }
        #endif

        i++;
        }

    fclose(f);
    //int non_zero, *nets_table_z;

    //open the nets file
    f = fopen(file_nets, "r");
    NULL_CHECK(f);

    //ignore the first line containing the format name and revision
    fgets(line, sizeof(line), f);

    //ignore the commented lines with info about the origin of the file
    do{
        fgets(line, sizeof(line), f);
    }while(line[0]=='#');

    //parse the number of nets and number of pins
    if(!fscanf(f, "%*[^:]%*[:]%d", &nets_count)){
        printf("\nERROR: nets file does not contain number of nets\n");
        exit(1);
        }

    if(!fscanf(f, "%*[^:]%*[:]%d", &netpins_count)){
        printf("\nERROR: nets file does not contain number of netpins\n");
        exit(1);
        }

    nets = (struct net_t*) malloc (nets_count * sizeof(struct net_t));
    if (nets == NULL) {
        printf("Cannot allocate memory: nets %ld\n", nets_count * sizeof(struct net_t));
        exit(1);
    }

    int* nets_table_temp0 = (int *)calloc(2*netpins_count,sizeof(int));
    int* nets_table_temp1 = (int *)calloc(2*netpins_count,sizeof(int));
    int nets_tableCtr = 0;
    netsCtr = i = 0;
    while(netsCtr<nets_count){
        int net_degree, net_deg0, out_id, in_id;
        char name[MAX_STRING_LENGTH];

        res = fscanf(f, "%*[^:]%*[:]%*[ \t\n]%d%*[^\n]%*[\n]", &net_degree);
        if(res<1){
            printf("\nERROR: net degree could not be parsed from nets file\n");
            exit(1);
            }

        i+=net_degree;

        nets[netsCtr].connectionsCtr = net_degree;
		net_deg0 = net_degree;
        nets[netsCtr].connections = (struct cell_t**) malloc (net_degree * sizeof(struct cell_t*));
        if (nets[netsCtr].connections == NULL) {
            printf("Cannot allocate memory: connections %ld\n", net_degree * sizeof(struct cell_t*));
            exit(1);
        }
		//non_zero = 0;
        while(net_degree>0){
            fscanf(f, "%s%*[ \t]%*[^\n]%*[\n]", name);
            cur_cell = cell_find (name);
            if(cur_cell == NULL){
                printf("\nERROR: Cell %s in nets file was not found in nodes file\n", name);
                exit(1);
                }
            if (net_deg0 == net_degree) {
				if(cur_cell->tpf == 0x4)
					out_id = cur_cell->index + movablesCtr;
				else
					out_id = cur_cell->index;
				}
			else {
				if(cur_cell->tpf == 0x4){
					in_id = cur_cell->index + movablesCtr;
                    nets_table_temp0[nets_tableCtr] = out_id;
                    nets_table_temp1[nets_tableCtr] = in_id;
                    nets_table_temp0[nets_tableCtr+1] = in_id;
                    nets_table_temp1[nets_tableCtr+1] = out_id;
                    nets_tableCtr+=2;
					//non_zero += 2;
				}
				else{
					in_id = cur_cell->index;
                    nets_table_temp0[nets_tableCtr] = out_id;
                    nets_table_temp1[nets_tableCtr] = in_id;
                    nets_table_temp0[nets_tableCtr+1] = in_id;
                    nets_table_temp1[nets_tableCtr+1] = out_id;
                    nets_tableCtr+=2;
					//non_zero += 2;
				}
			}

            nets[netsCtr].connections[net_degree-1]    = cur_cell;
            net_degree--;
        }

        #if (PRINTING==COMPACT)
        if((int)(netsCtr*34/nets_count)+66 > last_print){
            last_print = (int)(netsCtr*34/nets_count)+66;
            printf("\b\b\b\b\b[%2d%%]", last_print); fflush(stdout);
        }
        #endif    
        netsCtr++;
    }
    fclose(f);
    if(i!=netpins_count){
        printf("\nERROR: Netpins count mismatch\n");
        exit(1);
        }

	//Sorting + creating COO nets table
    quicksort_double_array (nets_table_temp0, nets_table_temp1, 0, nets_tableCtr-1);

    int start = 0;
    int length = 0;
    for (i = 0; i < cellsCtr; i++){
        while (nets_table_temp0[start+length] == i) {
            length++;
        }
        if (length>1){
            quicksort_simple(nets_table_temp1, start, start+length-1);
        }
        if (start+length>=nets_tableCtr)
            break;
        start += length;
        length = 0;
    }
    printf("netpins_count: %d, nets_tableCtr: %d, start: %d\n", netpins_count, nets_tableCtr, start);
    nets_table_row = (int *)malloc(nets_tableCtr*sizeof(int));
    if (nets_table_row == NULL) {
        printf("Cannot allocate memory: nets_table_row %ld\n", nets_tableCtr * sizeof(int));
        exit(1);
    }

    nets_table_col = (int *)malloc(nets_tableCtr*sizeof(int));
    if (nets_table_col == NULL) {
        printf("Cannot allocate memory: nets_table_col %ld\n", nets_tableCtr * sizeof(int));
        exit(1);
    }

    nets_table_value = (int *)malloc(nets_tableCtr*sizeof(int));
    if (nets_table_value == NULL) {
        printf("Cannot allocate memory: nets_table_value %ld\n", nets_tableCtr * sizeof(int));
        exit(1);
    }

    nets_table_start = (int *)malloc(cellsCtr*sizeof(int));
    if (nets_table_start == NULL) {
        printf("Cannot allocate memory: nets_table_start %ld\n", cellsCtr * sizeof(int));
        exit(1);
    }

    nets_table_row[0] = nets_table_temp0[0];
    nets_table_col[0] = nets_table_temp1[0];
    nets_table_value[0] = 1;
    nets_table_start[nets_table_temp0[0]] = 0;
    tupCtr = 0;
    for (i = 1; i < nets_tableCtr; i++){
        if (nets_table_temp0[i] == nets_table_temp0[i-1]){
            if (nets_table_temp1[i] == nets_table_temp1[i-1]){
                nets_table_value[tupCtr] ++;
            }
        }
        else 
            nets_table_start[nets_table_temp0[i]] = i;
        tupCtr++;
        nets_table_row[tupCtr] = nets_table_temp0[i];
        nets_table_col[tupCtr] = nets_table_temp1[i];
        nets_table_value[tupCtr] = 1;
    }


    int* tmp_array;
    tmp_array = (int *)realloc(nets_table_row, tupCtr*sizeof(int));
    if (tmp_array == NULL) {
        printf("Cannot allocate memory: realloc nets_table_row %ld\n", tupCtr*sizeof(int));
        exit(1);
    }
    nets_table_row = tmp_array;

    tmp_array = (int *)realloc(nets_table_col, tupCtr*sizeof(int));
    if (tmp_array == NULL) {
        printf("Cannot allocate memory: realloc  nets_table_col %ld\n", tupCtr*sizeof(int));
        exit(1);
    }
    nets_table_col = tmp_array;

    tmp_array = (int *)realloc(nets_table_value, tupCtr*sizeof(int));
    if (tmp_array == NULL) {
        printf("Cannot allocate memory: realloc nets_table_value %ld\n", tupCtr*sizeof(int));
        exit(1);
    }
    nets_table_value = tmp_array;


    //open the scl file
    f = fopen(file_scl, "r");
    NULL_CHECK(f);

    //ignore the first line containing the format name and revision
    fgets(line, sizeof(line), f);

    //ignore the commented lines with info about the origin of the file
    do{
        fgets(line, sizeof(line), f);
    }while(line[0]=='#');

    //parse the number of rows
    if(!fscanf(f, "%*[^:]%*[:]%d", &rows_count)){
        printf("\nERROR: scl file does not contain number of rows\n");
        exit(1);
        }

    //ignore all the rows till the last one
    for(i=0; i<rows_count-1; i++){

        do{
            fgets(line, sizeof(line), f);
            sscanf(line, "%s", tmp);
            }while(strncasecmp(tmp, "end", 3));
        }

    //parse the coordinate of the last row
    do{
        fscanf(f, "%s", line);
        }while(strncasecmp(line, "coordinate", 10));

    fscanf(f, "%*[^:]%*[:]%lf", &coreAreaHeight);
    

    //parse the height of the last row
    do{
        fscanf(f, "%s", line);
        }while(strncasecmp(line, "height", 6));

    fscanf(f, "%*[^:]%*[:]%lf", &stdCellHeight);
    coreAreaHeight += stdCellHeight;

    //parse the numsites of the last row
    do{
        fscanf(f, "%s", line);
        }while(strncasecmp(line, "numsites", 8));

    fscanf(f, "%*[^:]%*[:]%lf", &coreAreaWidth);

    fclose(f);

    hash_to_list ();

    #if (PRINTING==FULL)
    printf("\b\b\b\b\b\b\b\b...     \t[OK]");
    #elif (PRINTING==COMPACT)
    printf("\b\b\b\b\b[OK] \n"); fflush(stdout);
    #endif
    return 1;
    }


/* prints information about the parsed elements */
void print_parsed_info (){
    printf("\n------------------------------------------ PARSED INFO --------------------------------------------------------\n\n");
    printf("\t      cellsCtr: %7d\n", cellsCtr);
    printf("\t   movablesCtr: %7d\n", movablesCtr);
    printf("\t       netsCtr: %7d\n", netsCtr);
    printf("\t coreAreaWidth: %7.0lf\n", coreAreaWidth);
    printf("\tcoreAreaHeight: %7.0lf\n", coreAreaHeight);
    printf("\t stdCellHeight: %7.0lf\n", stdCellHeight);
    printf("\n----------------------------------------------------------------------------------------------------------------\n\n");
    }



double hpwl (){

	int i, j;
	double sum = 0.0;

	for(i=0; i<netsCtr; i++){
		double minX=coreAreaWidth, minY=coreAreaHeight, maxX=0.0, maxY=0.0;
 
		for(j=0; j<nets[i].connectionsCtr; j++){
			if(nets[i].connections[j]->centerX < minX) minX = nets[i].connections[j]->centerX;
			if(nets[i].connections[j]->centerY < minY) minY = nets[i].connections[j]->centerY;
			if(nets[i].connections[j]->centerX > maxX) maxX = nets[i].connections[j]->centerX;
			if(nets[i].connections[j]->centerY > maxY) maxY = nets[i].connections[j]->centerY;
			}

			sum += ((maxX-minX) + (maxY-minY));
		}
	return sum;
	}

void create_movables_cells_lists()
{
    movable_cells_list = (struct cell_t**) malloc (movablesCtr * sizeof(struct cell_t*));
    if (movable_cells_list == NULL) {
		printf("Cannot allocate memory: create_movables_cells_lists %ld\n", movablesCtr * sizeof(struct cell_t*));
		exit(1);
	}
    struct cell_t *cur_cell = cells_list;
    while (cur_cell!=NULL) { 
        if (cur_cell->tpf==0) {
            movable_cells_list[cur_cell->index] = cur_cell;
        }
        cur_cell = cur_cell->next;
    }
}

	
//initial placement strategy
void init_placement (struct cell_t** cells) {	
	int i;
		
	//random placement

	time_t t;
	int rows = coreAreaHeight / stdCellHeight;
	srand((unsigned) time(&t));
	for (i = 0; i<movablesCtr; i++){
		cells[i]->centerY = (rand()/(RAND_MAX/rows+1)) * stdCellHeight+ stdCellHeight/2;
		cells[i]->centerX = rand()/(RAND_MAX/(coreAreaWidth+1)+1);
		if ((cells[i]->centerX - cells[i]->sizeX/2) < 0)
			cells[i]->centerX = cells[i]->sizeX/2;
		else if ((cells[i]->centerX + cells[i]->sizeX/2) > coreAreaWidth)
			cells[i]->centerX = coreAreaWidth - cells[i]->sizeX/2;
	}
}
		
void free_everything() {
	free(cells_list);
	free(movable_cells_list);
	free(id_table);
	free(nets_table_col);
	free(nets_table_row);
	free(nets_table_value);
	free(row_count);
}

#define threads 32

__global__ void setup_kernel ( curandState * state, unsigned long seed) {
	int id = blockIdx.x * blockDim.x + threadIdx.x;
    curand_init ( seed, id, 0, &state[id] );
} 


//total_x_next - total_x_prev
__device__ double change_cost_d(int cell_1, int cell_2,
	double* centerY_d, double* centerX_d, double * sizeX_d, double* sizeY_d,
	int* nets_table_row_d, int* nets_table_col_d, int* nets_table_value_d, 
	int* nets_table_start_d, int* cells_to_shuffle_d, int *movables_cells_location_d) {
	int i = nets_table_start_d[cell_1];
	int value, col, col_X, col_Y;
	double total_x_prev = 0, total_x_next = 0, total_y_prev = 0, total_y_next = 0;
	double centerY_1, centerY_2, centerX_1, centerX_2;
	centerY_1 = centerY_d[cell_1];
	centerX_1 = centerX_d[cell_1];
	centerY_2 = centerY_d[cell_2];
	centerX_2 = centerX_d[cell_2];
	while(nets_table_row_d[i] == cell_1) {
		value = nets_table_value_d[i];
		col = nets_table_col_d[i];
		col_Y = centerY_d[col];
		col_X = centerX_d[col];
		total_x_prev += fabs((col_X - centerX_1)*value);
		total_y_prev += fabs((col_Y - centerY_1)*value);
		total_x_next += fabs((col_X - centerX_2)*value);
		total_y_next += fabs((col_Y - centerY_2)*value);
		i++;
	}
	i = nets_table_start_d[cell_2];
	while(nets_table_row_d[i] == cell_2) {
		value = nets_table_value_d[i];
		col = nets_table_col_d[i];
		col_Y = centerY_d[col];
		col_X = centerX_d[col];
		total_x_prev += fabs((col_X - centerX_1)*value);
		total_y_prev += fabs((col_Y - centerY_1)*value);
		total_x_next += fabs((col_X - centerX_2)*value);
		total_y_next += fabs((col_Y - centerY_2)*value);
		i++;
	}
	return total_y_next - total_y_prev + total_x_next - total_x_prev;
}

__device__ int random_accept_d(curandState* globalState, double c, double t, int id) {
	return curand_uniform(&globalState[id])*100000 < (__expf(-c/t)/1000000);
}

void restore(){
	double min = 10000000000, max = 0;
	int x = 0;
	// for (int i = 0; i < movablesPadCtr; ++i)
	// {
	// 	printf("%d, %f, %f\n", movable_cells_list[i]->index, movable_cells_list[i]->centerY, movable_cells_list[i]->centerX);
	// }
	for (int i = 0; i < movablesPadCtr; ++i)
	{
		if (movable_cells_list[i]->index!=-1){
			for (int j = 0; j < movablesPadCtr; ++j)
			{
				if (id_table[j]->centerY == movable_cells_list[i-1]->centerY){
					if ((id_table[j]->centerX<min) && (id_table[j]->centerX > movable_cells_list[i-1]->centerX)){
						min = id_table[j]->centerX;
						x = j;
					}
				}
			}
			if (x>-1){
				movable_cells_list[i] = id_table[x];
			}
			min = 100000000000;
		}
		else
			x = -1;
		//	max = 0;
		//max = id_table[x]->centerX;
	}
}
__global__ void kernel(int T, curandState* globalState, int *movables_cells_location_d,
	double* centerY_d, double* centerX_d, double * sizeX_d, double* sizeY_d,
	int* nets_table_row_d, int* nets_table_col_d, int* nets_table_value_d, 
	int* nets_table_start_d, int* cells_to_shuffle_d, int* cells_to_location_d) {
	int id = blockIdx.x * blockDim.x + threadIdx.x;
	int accept, idx = 2*threadIdx.x;
	double delta;
	__shared__ int cell[2*threads];
	__shared__ int prev[2*threads];
	__shared__ int next[2*threads];
	__shared__ int prevX[2*threads];
	__shared__ int nextX[2*threads];
	__shared__ int ctol[2*threads];

	ctol[idx] = cells_to_location_d[2*id];
	ctol[idx+1] = cells_to_location_d[2*id+1];

	cell[idx] = cells_to_shuffle_d[2 * id];
	cell[idx+1] = cells_to_shuffle_d[2 * id + 1];

	prev[idx] = movables_cells_location_d[ctol[idx]-1];
	prev[idx+1] = movables_cells_location_d[ctol[idx+1]-1];

	next[idx] = movables_cells_location_d[ctol[idx]+1];
	next[idx+1] = movables_cells_location_d[ctol[idx+1]+1];

	prevX[idx] = centerX_d[prev[idx]];
	prevX[idx+1] = centerX_d[prev[idx+1]];

	nextX[idx] = centerX_d[next[idx]];
	nextX[idx+1] = centerX_d[next[idx+1]];

	if (centerY_d[prev[idx]] == centerY_d[cell[idx]] == centerY_d[next[idx]]) {
		if (centerY_d[prev[idx+1]] == centerY_d[cell[idx+1]] == centerY_d[next[idx+1]]) {
			if ((sizeX_d[cell[idx]]- (centerX_d[next[idx+1]]-sizeX_d[next[idx+1]]/2) + centerX_d[prev[idx+1]] + sizeX_d[prev[idx+1]]/2) < 0.000001){
				if ((sizeX_d[cell[idx+1]]- (centerX_d[next[idx]]-sizeX_d[next[idx]]/2) + centerX_d[prev[idx]] + sizeX_d[prev[idx]]/2) < 0.000001){
					accept = 1;
				}
			}
		}
	}
    __syncthreads();
    if (accept == 1){
	    delta = change_cost_d(cell[idx], cell[idx+1],
		centerY_d, centerX_d,  sizeX_d,  sizeY_d,
		nets_table_row_d, nets_table_col_d, nets_table_value_d, 
		nets_table_start_d, cells_to_shuffle_d, movables_cells_location_d);
	    if (delta < 0.001) {
	    	accept = 1;
	    }
	    else 
	    	accept = 0;
	    // 	accept = random_accept_d(globalState, delta, T, id);
	    if (accept == 1) {
	    	centerX_d[idx] = (- prevX[idx+1] - sizeX_d[prev[idx+1]]/2 + nextX[idx+1] - sizeX_d[nextX[idx+1]]/2)/2;
	    	centerX_d[idx+1] = (- prevX[idx] - sizeX_d[prev[idx]]/2 + nextX[idx] - sizeX_d[nextX[idx]]/2)/2;
	    	// centerX_d[idx] = prevX[idx+1]/2 + sizeX_d[prev[idx+1]]/4 + nextX[idx+1]/2 - sizeX_d[nextX[idx+1]]/4;
	    	// centerX_d[idx+1] = prevX[idx]/2 + sizeX_d[prev[idx]]/4 + nextX[idx]/2 - sizeX_d[nextX[idx]]/4;
	    	swap_double(centerY_d, cell[idx], cell[idx+1]);
	    	//swap_double(centerX_d, cell[idx], cell[idx+1]);
	    	swap_int(movables_cells_location_d, ctol[idx], ctol[idx+1]);
 	    	swap_int(cells_to_location_d, cell[idx], cell[idx+1]);
	    	//swap_int(cell, idx, idx+1);
	    }
	}
    __syncthreads();
}

int main (int argc, char *argv[])
{
	//int i, j;//, z, h, count;
	time_t current_time;//, total_divide, tmp;
	clock_t start, end;
	double starting_cost, starting_cost_hpwl, final_cost, final_cost_hpwl;
	int j;
	current_time = time(NULL);
	//const int blockSize = 256, nStreams = 2;
	//const int n = 4 * 1024 * blockSize;
	//const int streamSize = n / nStreams;
	cudaEvent_t startEvent, stopEvent;
	//cudaStream_t stream[nStreams];
	checkCudaErrors( cudaEventCreate(&startEvent) );
	checkCudaErrors( cudaEventCreate(&stopEvent) );
	float ms;

	parse_bookshelf (argv[1], argv[2]);	// 1. parsing
	print_parsed_info();
	create_movables_cells_lists();
	
	init_placement(movable_cells_list);
	legalize(movable_cells_list);

	dim3 blocks(movablesCtr/(2*threads));


	int empty_rem = 2*coreAreaHeight/stdCellHeight-1;
	movablesPadCtr = movablesCtr+empty_rem+1;
	struct cell_t** tmp_array;
	tmp_array = (struct cell_t**) realloc(movable_cells_list, (movablesCtr + empty_rem + 1)*sizeof(struct cell_t*));
	if (tmp_array == NULL) {
		printf("Cannot allocate memory: movable_cells_list %ld\n", (movablesCtr + empty_rem + 1)*sizeof(struct cell_t*));
		exit(1);
	}
	movable_cells_list = tmp_array;
	padding(movable_cells_list);
	allocate_and_copy();
    setup_kernel <<< blocks, threads >>> ( devStates, time(NULL));
	int T = 10000000*(argv[4][0]-'0');

	starting_cost = total_cost_table()/2;
	starting_cost_hpwl = hpwl();
	j=0;
	for (int i = 0; i < movablesPadCtr; ++i)//Move the shit around
	{
		if (movable_cells_list[i]->tpf==0 && movable_cells_list[i]->sizeX!=0){
			cells_to_shuffle_h[j] = movable_cells_list[i]->index;
			cells_to_location_h[j] = i; //shuffle id -> location -> id
			j++;
		}
	}
	printf("%d, %d\n", j, movablesCtr);

	checkCudaErrors( cudaEventRecord(startEvent,0) );
	int i = 0;
	while (T>.5) {
		fisher_yates(cells_to_shuffle_h, cells_to_location_h, movablesCtr);
		checkCudaErrors(cudaMemcpy(cells_to_shuffle_d, cells_to_shuffle_h, movablesCtr*sizeof(int), cudaMemcpyHostToDevice)); //CudaMemcpyAsyncss
		checkCudaErrors(cudaMemcpy(cells_to_location_d, cells_to_location_h, movablesCtr*sizeof(int), cudaMemcpyHostToDevice));
		kernel <<< 1, 1 >>>  (T, devStates, movables_cells_location_d, centerY_d, centerX_d, sizeX_d, sizeY_d,
		nets_table_row_d, nets_table_col_d, nets_table_value_d, 
		nets_table_start_d, cells_to_shuffle_d, cells_to_location_d);
		  cudaError_t error = cudaGetLastError();
		  if(error != cudaSuccess)
		  {
		    // print the CUDA error message and exit
		    printf("CUDA error: %s\n", cudaGetErrorString(error));
		    exit(-1);
		  }
		//allocate_back_to_host();
		//output_check(movable_cells_list);
		checkCudaErrors(cudaMemcpy(cells_to_location_h, cells_to_location_d, movablesCtr*sizeof(int), cudaMemcpyDeviceToHost));
		T = 0;
		// T = T*.99;
		i++;
	}
	printf("HASDLBAKJSDLKAJSBD %d\n", i);
	checkCudaErrors( cudaEventRecord(stopEvent, 0) );
	checkCudaErrors( cudaEventSynchronize(stopEvent) );
	checkCudaErrors( cudaEventElapsedTime(&ms, startEvent, stopEvent) );
	printf("Time for sequential transfer and execute (ms): %f\n", ms);

	char s1[40], s2[40];
	sprintf(s1, "%ld", time(&current_time));
	//plot_area(s1,0);
	int iter = 100000*(argv[3][0]-'0');
	T = 10000000*(argv[4][0]-'0');
	printf("\n%d %d \n", iter, T);
	start = clock();
	//annealing(movable_cells_list, T, MIN(coreAreaHeight, coreAreaWidth), movablesCtr);
	end = clock();
	//output_check(movable_cells_list);
	sprintf(s2, "%ld", time(&current_time)+1);


	allocate_back_to_host();
	restore();
	output_check(movable_cells_list);

	final_cost = total_cost_table()/2;
	final_cost_hpwl = hpwl();
	//printf("Output1: %s Output2: %s\nStarting total wire length: \t\t%f\nFinal total wire length: \t\t%f\n", s1, s2, starting_cost, final_cost);
	//printf("Starting total wire length(hpwl): \t%f\nFinal total wire length(hpwl): \t\t%f\n", starting_cost_hpwl, final_cost_hpwl);
	
	double final_cost1 = total_cost_table()/2;
	double final_cost_hpwl1 = hpwl();
	FILE *f;
	char costs[10];
	sprintf(costs, "../%c %c", argv[3][0], argv[4][0]);
	f = fopen(costs, "a");

	fprintf(f, "%f \t %f \t %f \t %f \t %f \t %f \t cells/time: \t%d\t%ld\n", starting_cost, final_cost, starting_cost_hpwl, final_cost_hpwl, final_cost1, final_cost_hpwl1, cellsCtr, end - start);
	fclose(f);
	//plot_area(s2,0);
	free_everything();
	//free_cuda();
	return 0;
 }
/*	
	for(int i = 0; i<cellsCtr; i++){
		printf("%s: ", id_table[i]->name);
		for(int j = 0; j<cellsCtr; j++) {
			if(nets_table[j+i*cellsCtr]!=0){
				printf("%d: %s ", nets_table[j+i*cellsCtr], id_table[j]->name);
			}
		}
		printf("\n");
	}
	int idA;
	for(int i = 0; i<100; i++) {
		do {
			idA = (int)rand()/(RAND_MAX/(movablesPadCtr)+1);							//1
		}while(movable_cells_list[idA]->sizeX==0);
		printf("%s: centerX = %f, centerY = %f\n", id_table[idA]->name, id_table[idA]->centerX, id_table[idA]->centerY);
		node_cost(idA, id_table[idA]->centerX, id_table[idA]->centerY);
	}
	
	for(int i = 0; i<cellsCtr; i++) {
		printf("%s:", id_table[i]->name);
		for(int j = 0; j<cellsCtr; j++) {
			printf("\t%d", nets_table[j+i*cellsCtr]);
		}
		printf("\n");
	}*/
	//printf("mcl0: center = %f, size = %f, mcl1: center = %f, size = %f\n", movable_cells_list[0]->centerX, movable_cells_list[0]->sizeX, movable_cells_list[1]->centerX, movable_cells_list[1]->sizeX);
	//output_check(movable_cells_list);
	//printf("Total wire length(table): %f\n", total_cost_table());
	//printf("coreAreaWidth= %f, coreAreaHight = %f\n" ,coreAreaWidth, coreAreaHeight);
/*	i = j = 0;
	int k;
	current_time = clock();
	int accept = 0;
	for(k = 0; k<100000;k++) {
		accept += cell_select(movable_cells_list, MIN(coreAreaHeight, coreAreaWidth), i,j);
	}
	printf("\ncell_select median = %ld\n, reps =%d, accept = %d\n", (clock()-current_time), k, accept);

	*/


