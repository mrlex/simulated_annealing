#include "gordian.h"
#include "parsers.h"
#include "simulated_annealing.h"
#include <stdio.h>
#include <cuda_runtime.h>
#include <helper_cuda.h>
#include <helper_functions.h>
//Before padding
void allocate_and_copy(){
	int i;

	checkCudaErrors(cudaMallocHost(&movables_cells_location_h, movablesCtr*sizeof(int)));
	checkCudaErrors(cudaMallocHost(&centerY_h, cellsCtr*sizeof(double)));
	checkCudaErrors(cudaMallocHost(&centerX_h, cellsCtr*sizeof(double)));
	checkCudaErrors(cudaMallocHost(&sizeX_h, cellsCtr*sizeof(double)));
	checkCudaErrors(cudaMallocHost(&nets_table_row_h, tupCtr*sizeof(int)));
	checkCudaErrors(cudaMallocHost(&nets_table_col_h, tupCtr*sizeof(int)));
	checkCudaErrors(cudaMallocHost(&nets_table_value_h, tupCtr*sizeof(int)));
	checkCudaErrors(cudaMallocHost(&nets_table_start_h, cellsCtr*sizeof(int)));

	checkCudaErrors(cudaMalloc(&movables_cells_location_d, movablesCtr*sizeof(int)));
	checkCudaErrors(cudaMalloc(&centerY_d, cellsCtr*sizeof(double)));
	checkCudaErrors(cudaMalloc(&centerX_d, cellsCtr*sizeof(double)));
	checkCudaErrors(cudaMalloc(&sizeX_d, cellsCtr*sizeof(double)));
	checkCudaErrors(cudaMalloc(&nets_table_row_d, cellsCtr*sizeof(int)));
	checkCudaErrors(cudaMalloc(&nets_table_col_d, cellsCtr*sizeof(int)));
	checkCudaErrors(cudaMalloc(&nets_table_value_d, cellsCtr*sizeof(int)));
	checkCudaErrors(cudaMalloc(&nets_table_start_d, cellsCtr*sizeof(int)));

	for(i = 0; i < movablesCtr; i++){
		movables_cells_location_h[i] = movable_cells_list[i]->index;
	}
	for(i = 0; i < cellsCtr; i++){
		centerX_h[i] = id_table[i]->centerX;
		centerY_h[i] = id_table[i]->centerY;
		sizeX_h[i] = id_table[i]->sizeX;
		nets_table_start_h[i] = nets_table_start[i];
	}
	for(i = 0; i < tupCtr; i++){
		nets_table_row_h[i] = nets_table_row[i];
		nets_table_col_h[i] = nets_table_col[i];
		nets_table_value_h[i] = nets_table_value[i];
	}
	checkCudaErrors(cudaMemcpy(movables_cells_location_d, movables_cells_location_h, movablesCtr*sizeof(int), cudaMemcpyHostToDevice));
	checkCudaErrors(cudaMemcpy(centerY_d, centerY_h, cellsCtr*sizeof(double), cudaMemcpyHostToDevice));
	checkCudaErrors(cudaMemcpy(centerX_d, centerX_h, cellsCtr*sizeof(double), cudaMemcpyHostToDevice));
	checkCudaErrors(cudaMemcpy(sizeX_d, sizeX_h, cellsCtr*sizeof(double), cudaMemcpyHostToDevice));
	checkCudaErrors(cudaMemcpy(nets_table_row_d, nets_table_row_h, tupCtr*sizeof(int), cudaMemcpyHostToDevice));
	checkCudaErrors(cudaMemcpy(nets_table_col_d, nets_table_col_h, tupCtr*sizeof(int), cudaMemcpyHostToDevice));
	checkCudaErrors(cudaMemcpy(nets_table_value_d, nets_table_value_h, tupCtr*sizeof(int), cudaMemcpyHostToDevice));
	checkCudaErrors(cudaMemcpy(nets_table_start_d, nets_table_start_h, cellsCtr*sizeof(int), cudaMemcpyHostToDevice));
}

void allocate_back_to_host(){
	int i;
	checkCudaErrors(cudaMemcpy(movables_cells_location_h, movables_cells_location_d, movablesCtr*sizeof(int), cudaMemcpyDeviceToHost));
	checkCudaErrors(cudaMemcpy(centerY_h, centerY_d, cellsCtr*sizeof(double), cudaMemcpyDeviceToHost));
	checkCudaErrors(cudaMemcpy(centerX_h, centerX_d, cellsCtr*sizeof(double), cudaMemcpyDeviceToHost));
	checkCudaErrors(cudaMemcpy(sizeX_h, sizeX_d, cellsCtr*sizeof(double), cudaMemcpyDeviceToHost));
	checkCudaErrors(cudaMemcpy(nets_table_row_h, nets_table_row_d, tupCtr*sizeof(int), cudaMemcpyDeviceToHost));
	checkCudaErrors(cudaMemcpy(nets_table_col_h, nets_table_col_d, tupCtr*sizeof(int), cudaMemcpyDeviceToHost));
	checkCudaErrors(cudaMemcpy(nets_table_value_h, nets_table_value_d, tupCtr*sizeof(int), cudaMemcpyDeviceToHost));
	checkCudaErrors(cudaMemcpy(nets_table_start_h, nets_table_start_d, cellsCtr*sizeof(int), cudaMemcpyDeviceToHost));

	for (i = 0; i < cellsCtr; i++) {
		id_table[i]->centerX = centerX_h[i];
		id_table[i]->centerY = centerY_h[i];
		id_table[i]->sizeX = sizeX_h[i];		
	}

}

void free_cuda() {
	checkCudaErrors(cudaFreeHost(&movables_cells_location_h));
	checkCudaErrors(cudaFreeHost(&centerY_h));
	checkCudaErrors(cudaFreeHost(&centerX_h));
	checkCudaErrors(cudaFreeHost(&sizeX_h));
	checkCudaErrors(cudaFreeHost(&nets_table_row_h));
	checkCudaErrors(cudaFreeHost(&nets_table_col_h));
	checkCudaErrors(cudaFreeHost(&nets_table_value_h));
	checkCudaErrors(cudaFreeHost(&nets_table_start_h));

	checkCudaErrors(cudaFree(&movables_cells_location_d));
	checkCudaErrors(cudaFree(&centerY_d));
	checkCudaErrors(cudaFree(&centerX_d));
	checkCudaErrors(cudaFree(&sizeX_d));
	checkCudaErrors(cudaFree(&nets_table_row_d));
	checkCudaErrors(cudaFree(&nets_table_col_d));
	checkCudaErrors(cudaFree(&nets_table_value_d));
	checkCudaErrors(cudaFree(&nets_table_start_d));
}